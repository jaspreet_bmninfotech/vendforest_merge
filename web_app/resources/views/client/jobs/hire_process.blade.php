@extends('layouts/base')
    @section('addstyle')
    <link rel="stylesheet" media="screen" href="{{asset('css/client.css')}}" type="text/css" />
    @endsection
    @section('content')
@if(isset($data['user_details']))
    <?php
    $user_name = $data['user_details']->userName;
    if($data['user_details']->firstName != "")
    {
        $user_name = $data['user_details']->firstName . " " . $data['user_details']->lastName;
    }
    ?>
    <!-- here out errors -->

    <div class="container client-content hire-content">
        <div class="col-xs-12">
            @if (Session::has('hire_flash_success_msg'))
                <div class="alert alert-success text-center">
                    <strong>Great!</strong> {{hire_flash_success_msg}}.
                </div>
            @endif

            @if (Session::has('hire_flash_err_msg'))
                <div class="alert alert-warning text-center">
                    <strong>Great!</strong> {{Session::get('hire_flash_err_msg')}}.
                </div>
            @endif
            <div class="col-xs-6 hire_user_div">
                <div class="row">
                    <div class="col-xs-2" id="client_img">
                        <img src="/<?php echo ($data['attach']->path != null) ?  $data['attach']->path:'images/no-user.png' ?>" class="img-responsive img-circle">
                    </div>
                    
                    <div class="col-xs-10">
                        <h2 class="au_name">{{$user_name}}</h2>
                    </div>
                </div>
            </div>

            <div class="job-detail-wrapper">
                <div id="view_job_post_div" class="tab-pane fade in active">
                    <h3>Contract Details</h3>
                    <div class="col-xs-12">
                        <form action="{{url('client/hire-confirm')}}/{{$data['job_details']->id}}/{{$data['user_details']->id}}" method="POST">
                            <div class="form-group">
                                <p class="margin_defaullt"><a href="<?php echo 'job/details/' . $data['job_details']->id; ?>">{{$data['job_details']->name}}&nbsp;(#{{$data['job_details']->id}})</a></p>
                            </div>
                            <div class="form-group">
                                <b>Contract Title</b>
                                <p class="margin_bottom2"><a href="<?php echo 'job/details/' . $data['job_details']->id; ?>">{{$data['job_details']->name}}</a></p>
                            </div>

                            <?php
                            if ($data['job_details']->isHourly != '0')
                            {
                            ?>
                                <div class="form-group">
                                    <p><b>Hourly Rate :</b>
                                    <span>${{$data['job_details']->minRate}}/hr</span></p>
                                </div>
                            <?php
                            }
                            else
                            {
                            ?>
                                <div class="form-group">
                                    <p><b>Rate :</b>
                                    <span>${{$data['job_details']->budget}}</span></p>
                                </div>
                            <?php
                            }
                            ?>

                            <div class="form-group">
                                <label><b>Work Description</b></label>
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="bid_id" value="{{$data['bid_id']}}">
                                <textarea name="work_desc" class="form-control" rows="10">{{$data['job_details']->description}}</textarea>
                            </div>
                            
                            <div class="form-group">
                                <input type="checkbox" name="hire_tnc" id="hire_tnc">&nbsp;<span>Yes, I understand and agree to the <a href="javascript:void(0);">Vendor Forest Terms of Service</a>, including the <a href="javascript:void(0);">User Agreement</a> and <a href="javascript:void(0);">Privacy Policy</a>.</span>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="button solid">Hire {{$user_name}}</button>
                                <a href="{{ route('client.job') }}" class="button default2 btn_padd">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @elseif(isset($data['team_details']))
    <?php
    $team_name = $data['team_details']->name;
    $tId = $data['team_details']->id;
    ?>
    <!-- here out errors -->

    <div class="container client-content hire-content">
        <div class="col-xs-12">
            @if (Session::has('hire_flash_success_msg'))
                <div class="alert alert-success text-center">
                    <strong>Great!</strong> {{hire_flash_success_msg}}.
                </div>
            @endif

            @if (Session::has('hire_flash_err_msg'))
                <div class="alert alert-warning text-center">
                    <strong>Great!</strong> {{hire_flash_err_msg}}.
                </div>
            @endif
            <div class="col-xs-6 hire_user_div">
                <div class="row">
                    <div class="col-xs-2">
                        <img src="/<?php echo ($data['attach']->path != null) ?  $data['attach']->path:'images/no-user.png' ?>" class="img-responsive img-circle">
                    </div>
                    
                    <div class="col-xs-10">
                        <h2>{{$team_name}}</h2>
                    </div>
                </div>
            </div>

            <div class="job-detail-wrapper">
                <div id="view_job_post_div" class="tab-pane fade in active">
                    <h3>Contract Details</h3>
                    <div class="col-xs-12">
                        <form action="{{url('client/team/hire-confirm')}}/{{$data['job_details']->id}}/{{$data['team_details']->id}}" method="POST">
                            <div class="form-group">
                              
                                <p><a href="<?php echo 'job/details/' . $data['job_details']->id; ?>">{{$data['job_details']->name}}&nbsp;(#{{$data['job_details']->id}})</a></p>
                            </div>

                            <div class="form-group">
                                <p><label>Contract Title</label></p>
                                <p><a href="<?php echo 'job/details/' . $data['job_details']->id; ?>">{{$data['job_details']->name}}</a></p>
                            </div>
                            <?php
                            if ($data['job_details']->isHourly!= '0')
                            {
                            ?>
                                <div class="form-group">
                                    <p><label>Hourly Rate</label></p>
                                    <p>{{$data['job_details']->minRate}}/hr</p>
                                </div>
                            <?php
                            }
                            else
                            {
                            ?>
                                <div class="form-group">
                                    <p><label>Rate</label></p>
                                    <p>{{$data['job_details']->budget}}</p>
                                </div>
                            <?php
                            }
                            ?>

                            <div class="form-group">
                                <p><label>Work Description</label></p>
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <input type="hidden" name="bid_id" value="{{$data['bid_id']}}">
                                <textarea name="work_desc" class="form-control" rows="10">{{$data['job_details']->description}}</textarea>
                            </div>
                            <div class="form-group">
                                <input type="checkbox" name="hire_tnc" id="hire_tnc">&nbsp;<span>Yes, I understand and agree to the <a href="javascript:void(0);">Vendor Forest Terms of Service</a>, including the <a href="javascript:void(0);">User Agreement</a> and <a href="javascript:void(0);">Privacy Policy</a>.</span>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="button solid" data-id="{{$tId}}">Hire {{$team_name}}</button>
                                <a href="{{ route('client.job') }}" class="btn-default button solid cancel_btn">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
    @endsection

    @section('addjavascript')
        <script src="{{asset('js/client.js')}}"></script>
    @endsection