@extends('../layouts/base')
  @section('content')
		<div class="container job-deatils-page">
			<div class="row">
				<div class="col-md-8">
					<div class="job-detail-panel">
						<div class="vf-wrap job-deatils">
							<div class="job-heading">
								<h2 class="job_name">{{$data->title}}</h2>
								<span>Posted at : @if( \Carbon\Carbon::parse($data->created_at)->diffInMonths(\Carbon\Carbon::now()) <= 10 )
                                    {{ \Carbon\Carbon::parse($data->created_at)->diffForHumans() }}
                                @else
                                    {{ \Carbon\Carbon::parse($data->created_at)->format('m/d/Y') }}
                                @endif</span><br/>
							</div>
							<div class="job-description">
								<div class="col-md-8 border-right">
									<div class="job-text">
										<div>From Date :</div>
										<span>{{date("m/d/Y", strtotime($data->fromdatetime))}}</span><br/><br>
										<div>To Date :</div>
										<span>{{date("m/d/Y", strtotime($data->todatetime))}}</span>
									</div>
									<div class="job-info">	
										<div>						
										</div>
									</div>
								</div>
								<div class="col-md-4 job-requirement">
									<div>									
										<h3>Status </h3>
										<?php
                                        if ($data->status == '1') {
                                              ?>
                                        <p>Open</p>
                                        <?php
                                        } elseif ($data->status == '0') {
                                            ?>
                                        <p>Close</p>
                                            <?php
                                        } elseif ($data->status == '2') {
                                            ?>
                                        <p>Expired</p>

                                            <?php
                                        }
                                        ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 client-details">
					<div class="row">
						<div class="job-detail-panel client-panel">
							<div class="text-center submit-proposal-buttons">
									<?php
                                        if ($data->invitestatus == '2') {	
                                              ?>
									<a href="javascript:;" class="button solid button-margin" onclick="accept({{$data->id}})" >Accept</a><br>
									<!-- <a href="/vendor/job/"><button class="button default vf-btn"> Decline </button></a> -->
									<?php
                                        } else {
                                            ?>
								 	<a href="/vendor/proposal/"><button class="button solid vf-btn">View Proposal</button></a>
							<?php
							}
							?>
							</div>
							<div>
								<label>About the client  </label>
							</div>
							<div>
								<h5> Jobs Posted</h5>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
 @section('addjavascript')
    <script src="{{asset('js/tourGuide.js')}}"></script>
    @endsection