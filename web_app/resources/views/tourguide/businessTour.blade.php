@extends('layouts/base') 
@section('content')
<div class="job-posting-form container">
    @if(Session::has('bussinessRedirected'))
    <div class="alert alert-danger">
        <p style="">{{Session::get('bussinessRedirected')}}</p>
    </div>
    @endif @if(Session::has('success'))
    <div class="alert alert-success">
        <p style="color:green;">{{Session::get('success')}}</p>
        @php Session::forget('success'); @endphp
    </div>
    @endif
    <div class="col-md-3">
        @include('tourguide/sidenav')
    </div>
    <div class="form-fields col-md-9">
        <form method="POST" autocomplete="off" action="{{route('tourguide.business.add')}}" enctype="multipart/form-data">
            <div class="vfform">
                <div class="title">
                    <h3>Business Information</h3>
                </div>
                <div class="feildcont">
                    {!! csrf_field() !!}
                    <div class="row">
                        <div class="col-md-6 dropdown">
                            <label>Business Category <em>*</em></label>
                            <select name="businesscategory" id="category" onchange="getSubcategories(this)">
                                <option value="">- Select -</option>
                                @foreach($category as $key=>$val)
                                <option value="{{$val->id}}_{{$val->shortCode}}" data-id="{{$val->id}}">{{$val->name}}</option>
                                @endforeach
                            </select>
                            <div class="clearfix"></div>
                            @if($errors->has('businesscategory'))
                            <span class="text-danger">{{ $errors->first('businesscategory') }}</span> @endif
                        </div>
                        <div class="col-md-6 last">
                            <label>Can you travel to work to on event? <em>*</em></label>
                            <select name="cantravel">
                                <option>- Select -</option>
                                <option value="yes">YES</option>
                                <option value="no">NO</option>
                            </select>
                            <div class="clearfix"></div>
                            @if($errors->has('cantravel'))
                            <span class="text-danger">{{ $errors->first('cantravel') }}</span> @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Business Name<em>*</em></label>
                            <input type="text" name="businessname" id="name"> @if($errors->has('businessname'))
                            <span class="text-danger">{{ $errors->first('businessname') }}</span> @endif
                        </div>
                    </div>
                </div>
            </div>
            <!-- Business Profile end-->
            <!-- Bussinss Info else part-->
            <div class="vfform bussiness" id="other">
                <div class="title">
                    <h3>Information</h3>
                </div>
                <div class="feildcont">
                    <div class="row">
                        <div class="col-md-12 last">

                            <label>Specific Category <em>*</em></label>
                            <select id="subcategory" name="subcategory">
                                <option value="">select your category</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label>How Would like to describe your bussiness?<em>*</em></label>
                            <textarea id="description" name="description"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label>How Did You hear about us? <em>*</em></label>
                            <select name="hearaboutus">
                                <option value="">select</option>
                                <option value="internet">Internet</option>
                            	<option value="friends">Friends</option>
                            </select>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- Bussiness Info End else part-->
            <button type="submit" class="button solid">Submit</button>
            <br>
            <br>
        </form>
    </div>
</div>
@endsection @section('addjavascript')
<script type="text/javascript" src="{{asset('js/bussiness.js')}}"></script>

@endsection