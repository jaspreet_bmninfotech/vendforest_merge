@extends('layouts.base')
@section('content')

<div class="job-posting-form container">

	<a href="{{route('dispute.create')}}"> Apply New  Dispute</a> 
	<a href="{{route('dispute.index', ['type'=>'aganist']) }}"> Aganist Dispute</a> 

	<table>
		<thead>
			<tr>
				<th>Job</th>
				<th>Subject </th>
				<th> Status </th>
				<th> Apply at </th>
				<th>  View Detial </th>
				
			</tr>
		</thead>
		<tbody>

			@foreach($dispute as $key => $val)
			<tr>
				<td>
					{{$val['job']['name']}}
				</td>
				<td> {{$val->subject}}</td>
				<td>{{$val['status']['name']}}</td>
				<td> {{$val->created_at}} </td>
				<td><a href="{{route('dispute.detail', ['id'=> $val->id])}}" > View Detail</a>  </td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>

@endsection