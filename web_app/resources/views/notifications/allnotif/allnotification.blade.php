@extends('../layouts/base')
  	@section('content')
	<div class="job-posting-form container">
		<div class="row">
			<div class="vendor-job-list form-fields panel-group">
				<div class="vfform margin-zero" id="">
					<div class="heading">
						<h3>Notification</h3>
					</div>
				@if($data==NULL)
					<div>
						<div class="col-md-12">
							<div class="row">
								<div class="vf-bids">
									<div class="col-md-12 text-center ">
										<h3>Currently you have no Notification</h3>
									</div>
								</div>
							</div>
						</div>
					</div>
				@else
				@foreach ($data as $notif)
					<div>
						<div class="col-md-12">
							<div class="row">
								<div class="vf-bids">
									<div class="col-md-6 ">
										<div class="bid-detail">
											<b>{{$notif['name']}}</b>&nbsp;<span>{{$notif['message']}}</span> 
										</div>
									</div>
									<div class="col-md-3 ">
										<div class="bid-detail">
											
										</div>
									

										<div class="bid-detail">
										</div>
										
									</div>
									<div class="col-md-3 ">
										<div class="bid-detail showdate">
										{{date('F j, Y g:i a',strtotime($notif['date']))}} 
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				@endforeach
				@endif
				</div>
			</div>
		</div>
	</div>
  @endsection