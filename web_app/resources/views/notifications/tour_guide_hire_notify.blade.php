<li class="dropdown">
   <a href="javascript:;" data-href="/tourguide/hires/{{$notification->data['content']['id']}}" data-notif-id="{{$notification->id}}" data-operation="{{ $notification->data['notif'] }}">
    <div class="row">
    	<div class="col-md-6 col_black notifybarssetting">
        	<span> {{ ucwords($notification->data['senduser'])}}</span> <span >{{ucwords($notification->data['notif']) }} 
         	{{$notification->data['content']['tourguidejob']['title']}}</span> <br>
         	{{date('F j,Y',strtotime($notification->created_at))}} 
     	</div>
     	<div class="clearfix"></div>
    </div>
</a>
</li>
