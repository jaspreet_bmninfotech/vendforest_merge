<li class="dropdown">
	<?php if(Auth::user()->type=='cn') 
		{?>
   <a href="javascript:;" data-href="/client/tourguide/{{$notification->data['content']['tourguidejob']['id']}}/detail" data-notif-id="{{$notification->id}}" data-operation="{{ $notification->data['notif'] }}">
    <div class="row">
    	<div class="col-md-6 col_black notifybarssetting">

    		<span>{{ ucwords($notification->data['senduser'])}}</span> <span>{{ucwords($notification->data['notif']) }} 
         	{{$notification->data['content']['tourguidejob']['title']}} <br>
         	{{date('F j,Y',strtotime($notification->created_at))}} </span>
     	</div>
     	<div class="clearfix"></div>
    </div>
	</a>
	<?php }else
	{ ?>
	<a href="javascript:;" data-href="/vendor/tourguide/{{$notification->data['content']['tourguidejob']['id']}}/detail" data-notif-id="{{$notification->id}}" data-operation="{{ $notification->data['notif'] }}">
    <div class="row">
    	<div class="col-md-6 col_black notifybarssetting">
    		<span>{{ ucwords($notification->data['senduser'])}}</span> <span>{{ucwords($notification->data['notif']) }} 
         	{{$notification->data['content']['tourguidejob']['title']}}</br>
         	{{date('F j,Y',strtotime($notification->created_at))}} </span> 
     	</div>
     	<div class="clearfix"></div>
    </div>
</a>
	<?php } ?>
</li>
