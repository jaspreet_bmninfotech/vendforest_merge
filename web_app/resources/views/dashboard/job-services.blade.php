@extends('../layouts/base')
  @section('content')
<div class="featured_section116">
<div class="container">
    <div class="stcode_title6">
       <h3>Choose the service in which in which you're posting the job under</h3>
    </div>
    <div class="clearfix "></div>
    <div class="col-md-4" data-anim-type="fadeInLeft" data-anim-delay="100">
        <div class="box">
        
            <img src="images/icon-img51.png" alt="" />
            <br /><br />
            <h3>Tour Guide</h3>
            <br />
            <a href="#" class="button twentynine">Read More</a>
            
        </div>
    </div><!-- end section -->
    
    <div class="col-md-4 " data-anim-type="fadeInUp" data-anim-delay="100">
        <div class="box">
        
            <img src="images/icon-img52.png" alt="" />
            <br /><br />
            <h3>Vendor</h3>
            <br />
            <a href="#" class="button twentynine">Read More</a>
            
        </div>
    </div><!-- end section -->
    
    <div class="col-md-4 last  " data-anim-type="fadeInRight" data-anim-delay="100">
        <div class="box">
        
            <img src="images/icon-img53.png" alt="" />
            <br /><br />
            <h3>Pro Vendor</h3>
            <br />
            <a href="#" class="button twentynine">Read More</a>
            
        </div>
    </div><!-- end section -->
</div>
</div><!-- end featured section 116 -->

@endsection