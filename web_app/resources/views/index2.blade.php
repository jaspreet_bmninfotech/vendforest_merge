@extends('layouts/base')
  @section('content')
  <section class="vf-hd">
    <div class="vf-standar-div container">
      <div class="vf-left-title">
        <h1>Find and hire the best vendor from all over the world!</h1>
        <p>Small paragraph hopefuly explaining what you just said in the first place.</p>
        <form class="vf-hd-search">
          <input type="search" class="vf-q" placeholder="What do you need?" />
          <button class="vf-q-btn">Get started</button>
        </form>
      </div>
    </div>
  </section>
  <!-- End Banner -->
  <section class="vf-hw-works">
    <div class="vf-standar-div container">
      <h1>How it works</h1>
      <div class="hw-bx-wp">
        <div class="hw-bx-ct-1">
          <div class="hw-bx-im" style="background-color: #f6f9fa; border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px;">
            <img src="http://res.cloudinary.com/lyruntpzo/image/upload/v1508335203/find_xhnvjt.svg" width="290" height="250" alt="Find"
            />
          </div>
          <div class="hw-bx-dt">
            <h3>Find</h3>
            <p>
              Learn from over 1000 videos created by expert teachers on web design, coding, business, and much more. Our library is continually
              refreshed with the latest on web technology so you will never fall behind.
            </p>
          </div>
        </div>
        <div class="hw-bx-ct-2">
          <div class="hw-bx-im" style="background-color: #f6f9fa; border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px;">
            <img src="http://res.cloudinary.com/lyruntpzo/image/upload/v1508335203/bid_ouydfk.svg" alt="Bid" width="290" height="250"
            />
          </div>
          <div class="hw-bx-dt">
            <p>
              Learn from over 1000 videos created by expert teachers on web design, coding, business, and much more. Our library is continually
              refreshed with the latest on web technology so you will never fall behind.
            </p>
          </div>
        </div>
      </div>
      <div class="hw-bx-wp">
        <div class="hw-bx-ct-1">
          <div class="hw-bx-im" style="background-color: #f6f9fa; border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px;">
            <img src="http://res.cloudinary.com/lyruntpzo/image/upload/v1508335203/hire_zeet0v.svg" width="290" height="250" alt="Find"
            />
          </div>
          <div class="hw-bx-dt">
            <h3>Hire</h3>
            <p>
              Learn from over 1000 videos created by expert teachers on web design, coding, business, and much more. Our library is continually
              refreshed with the latest on web technology so you will never fall behind.
            </p>
          </div>
        </div>
        <div class="hw-bx-ct-2">
          <div class="hw-bx-im" style="background-color: #f6f9fa; border-radius: 3px; -webkit-border-radius: 3px; -moz-border-radius: 3px;">
            <img src="http://res.cloudinary.com/lyruntpzo/image/upload/v1508335203/pay_vp3agt.svg" alt="Bid" width="290" height="250"
            />
          </div>
          <div class="hw-bx-dt">
            <h3>Pay</h3>
            <p>
              Learn from over 1000 videos created by expert teachers on web design, coding, business, and much more. Our library is continually
              refreshed with the latest on web technology so you will never fall behind.
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>
    <!-- END How it works -->
  <section class="vf-hire container">
    <div class="vf-standar-div">
      <h1>Hire the right vendor for you</h1>
      <div id="grid-display">
        <a class="one-fourth">
          <h3>Model
          </h3>
        </a>
        <a class="one-fourth">
          <h3>
            Venue
          </h3>
        </a>
        <a class="one-fourth">
          <h3>
            Hair-Stylist
          </h3>
        </a>
        <a class="one-fourth">
          <h3>
            Band
          </h3>
        </a>
        <a class="one-fourth">
          <h3>
            Photographer
          </h3>
        </a>
        <a class="one-fourth">
          <h3>
            Videographer
          </h3>
        </a>
        <a class="one-fourth">
          <h3>
            Transportation
          </h3>
        </a>
        <a class="one-fourth">
          <h3>
            Florist
          </h3>
        </a>
      </div>
      <div class="hw-bx-dt">
        <br><br>
        <p>
          Learn from over 1000 videos created by expert teachers on web design, coding, business, and much more. Our library is continually
          refreshed with the latest on web technology so you will never fall behind.
        </p>
      </div>
    </div>
    <div class="view-all">
      <a href="" class="button default">See All Categories</a>
    </div>
  </section>
  <!-- End Hire Section -->
  <section class="client-testimony">
    <div class="vf-standar-div container">
      <div class="testimony-content">
        <h3>
          <img class="quote-mark" src="https://www.upwork.com/static/marketing/adquiro-webpack/images/icon-quote.2fa88ae581e5.svg"
            alt="">
          We accomplish more, for a lot less.
        </h3>
        <p>Tyson Quick</p>
        <p>CEO &amp; Founder, Instapage</p>
      </div>
      <div class="testimony-media">
        <iframe src="https://player.vimeo.com/video/188020320?color=6fda44&amp;title=0&amp;byline=0&amp;portrait=0" width="489" height="270"
          frameborder="0"></iframe>
      </div>
    </div>
  </section>
  <!-- End Testimony Section -->
  <section class="job-bidding container">
    <div class="vf-standar-div">
      <h1>Browse Jobs Type</h1>
      <ul>
        <li>
          <a href="#">Award Ceremony</a>
        </li>
        <li>
          <a href="#">Birthday</a>
        </li>
        <li>
          <a href="#">Board Meeting</a>
        </li>
        <li>
          <a href="#">Business Dinner</a>
        </li>
        <li>
          <a href="#">Conference</a>
        </li>
        <li>
          <a href="#">Convention</a>
        </li>
        <li>
          <a href="#">Corporate Enetertainment</a>
        </li>
        <li>
          <a href="#">Executive Retreat</a>
        </li>
        <li>
          <a href="#">Family Event</a>
        </li>
        <li>
          <a href="#">Golf Event</a>
        </li>
        <li>
          <a href="#">Incentive Event</a>
        </li>
        <li>
          <a href="#">Incentive Travel</a>
        </li>
        <li>
          <a href="#">Meeting</a>
        </li>
        <li>
          <a href="#">Seminar</a>
        </li>
        <li>
          <a href="#">Team Building Event</a>
        </li>
        <li>
          <a href="#">Trade Show</a>
        </li>
        <li>
          <a href="#">Press Conference</a>
        </li>
        <li>
          <a href="#">Networking Event</a>
        </li>
        <li>
          <a href="#">Opening Ceremony</a>
        </li>
        <li>
          <a href="#">Product Launches</a>
        </li>
        <li>
          <a href="#">Theme Party</a>
        </li>
        <li>
          <a href="#">VIP Event</a>
        </li>
        <li>
          <a href="#">Trade Fair</a>
        </li>
        <li>
          <a href="#">Shareholder Meeting</a>
        </li>
        <li>
          <a href="#">Wedding</a>
        </li>
        <li>
          <a href="#">Wedding Anniversary</a>
        </li>
      </ul>
    </div>
  </section>
  <!-- End Browse Job Type -->
  <section class="level-vendor">
    <div class="vf-standar-div container">
      <h1>Find the service that works for you</h1>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th class="bg-cloud-1 vf-th-vg text-center">
              <h3 class="font-gotham-light m-t-40">Vendor</h3>
              <p class="text-short txt-gray font-gotham-medium text-granite">Professional freelancers and the essentials to find them.</p>
              <div class="sp-dv">
                <p>
                  <a href="" class="button default">Sign Up</a>
                </p>
              </div>
            </th>
            <th class="bg-cloud-2 vf-th-vg text-center">
              <h3 class="font-gotham-light m-t-40">Pro Vendor</h3>
              <p class="text-short txt-gray font-gotham-medium text-granite">Premium talent, pre-vetted and handpicked for you.</p>
              <div class="sp-dv">
                <p>
                  <a href="" class="button default">Learn More</a>
                </p>
              </div>
            </th>
            <th class="bg-cloud-3 vf-th-vg text-center">
              <h3 class="font-gotham-light m-t-40">Tour Guide</h3>
              <p class="text-short txt-gray font-gotham-medium text-granite">Top notch end-to-end Freelancer Management System.</p>
              <div class="sp-dv">
                <p>
                  <a href="" class="button default">Learn More</a>
                </p>
              </div>
            </th>
          </tr>
        </thead>
      </table>
    </div>
  </section>
  <div class="site__cash" id="site__cash"></div>
  @endsection
