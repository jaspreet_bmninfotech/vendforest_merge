@extends('layouts/base')
@section('content')
<div class="container client-content">
        <div class="row">
            <br><br>           
            <div class="col-md-4 col-xs-4">
               @if(Session::has('buyplanRedirected'))
                <div class="alert alert-danger">
                <p style="">{{Session::get('buyplanRedirected')}}</p>
                </div>
                @endif
                @if(Session::has('success'))
                <div class="alert alert-success">
                <p style="color:green;">{{Session::get('success')}}</p>
                @php Session::forget('success'); @endphp
                </div>
                @endif  
            </div>
        </div>
    <div class="form-fields col-md-7">
         <div class="vfform"> 
            <div class="title">
                <h3>Payment Detail</h3>
            </div>  
            <div class="feildcont">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                    <form accept-charset="UTF-8" action="{{route('plan.buy',$plan->id)}}" class="require-validation"
                        data-cc-on-file="false"
                        data-stripe-publishable-key="pk_test_GAvXjMzFP2JIj0LnsM42l1i4"
                        id="payment-form" method="post">
                        {{ csrf_field() }}
                        <div class='form-row'>
                            <div class='col-xs-12 form-group required'>
                                <label class='control-label'>Name on Card</label> 
                                @php if (isset($customer)) { 
                                @endphp 
                                <input class='' size='4' type='text' name="cardBrand" value="{{ $customer->sources->data[0]->brand }}">
                                @php   
                                }else{
                                @endphp
                                <input class='' size='4' type='text' name="cardBrand" value="">
                                @php
                                }
                               
                                 @endphp   
                            </div>
                        </div>
                        <div class='form-row'>
                            <div class='col-xs-12 form-group card required'>
                                <label class='control-label'>Card Number</label> 

                                @php if (isset($customer)){ 
                                    @endphp 
                                    <input autocomplete='off' class='card-number' size='20'
                                    type='text' name="cardNumber" value="{{ $customer->sources->data[0]->last4 }}">
                                    <input type ="hidden" id="hasPrevCard" name ="hasPrevCard" value="1">
                                @php   
                                }else{
                                @endphp
                                      <input autocomplete='off' class='card-number' size='20'
                                    type='text' name="cardNumber" value="">
                                    <input type ="hidden" id="hasPrevCard" name ="hasPrevCard" value="0">
                                    @php
                                }
                                 @endphp
                            </div>
                        </div>
                        <div class='form-row'>
                            <div class='col-xs-4 form-group cvc required'>
                                <label class='control-label'>CVC</label> <input autocomplete='off'
                                    class='card-cvc' placeholder='ex. 311' size='4'
                                    type='text'>
                            </div>
                            <div class='col-xs-4 form-group expiration required'>
                                <label class='control-label'>Expiration</label> <input
                                    class='card-expiry-month' placeholder='MM' size='2'
                                    type='text'>
                            </div>
                            <div class='col-xs-4 form-group expiration required'>
                                <label class='control-label'> </label><br> <input
                                    class='card-expiry-year' placeholder='YYYY' size='4'
                                    type='text'>
                            </div>
                        </div>
                        <div class='form-row text-center'>
                            <div class='form-group'>
                                <div class="col-md-12">
                                    <button class='col-md-12 col-xs-12  button solid submit-button'
                                    type='submit' style="margin-top: 10px;">Pay »</button>

                                    <p><a href ="{{route('plan.buy',$plan->id) }}" name="newCard" id="newCard">use another Card</a></p>

                                </div>
                            </div>
                        </div>
                        <div class='form-row'>
                            <div class='col-md-12 error form-group hide'>
                                <div class='alert-danger alert'>Please correct the errors and try
                                    again.</div>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>              
        </div>
    </div>
     <div class="col-md-5">
        <div class="vfform panel panel-default">          
                <div class="panel-heading">
                    <h3>Customer Detail</h3>
                </div> 
            <table class="table">
            <thead>
                <tr>
                    <th>Customer Name</th>
                    <th>Plan ID</th>
                    <th>Plan Price</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{$user}}</td>
                    <td>{{$plan->id}}</td>
                    <td>{{$plan->price}}</td>
                </tr>
            </tbody>
            </table>
     </div>   
     <div class="clearfix"></div>
</div>
@endsection
@section('addjavascript')
<script type="text/javascript" src="{{asset('js/paymentuser.js')}}"></script>
 <script src='https://js.stripe.com/v2/' type='text/javascript'></script>
@endsection