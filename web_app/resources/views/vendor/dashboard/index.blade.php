@extends('../layouts/base')
  @section('content')
<div class="job-posting-form container">
	<div class="row">
		<div class="col-md-3">
			<div class="sidebar_widget">
				<div class="sidebar_title"><h4>Jobs</h4>
				</div>
				<ul class="arrows_list1">
				 <li><a href="{{route('vendor.dashboard')}}"><i class="fa fa-angle-right"></i> Find jobs</a></li>
					<li><a href="{{route('vendor.bids')}}"><i class="fa fa-angle-right"></i> Saved jobs</a></li>		
					<!-- <li><a href="profile"><i class="fa fa-angle-right"></i> Find jobs</a></li>
					<li><a href="business"><i class="fa fa-angle-right"></i> Saved jobs</a></li>
					<li><a href=""><i class="fa fa-angle-right"></i> Jobs invite</a></li>
					<li><a href="payment"><i class="fa fa-angle-right"></i> My status</a></li>
					<li><a href="payment"><i class="fa fa-angle-right"></i> Transanction</a></li> -->
				</ul>	
			</div>
		</div>
		<div class="vendor-job-list form-fields panel-group col-md-9">
			<div class="vfform" id="save-job">
				<div class="title">
					<h3>Jobs</h3>
				</div>
				<form role="form"  >
					<table class="table vf-saved-job" id="jobList">
						<thead>
							<tr class="">
								<th width="40%">JOB TITLE</th>
								<th width="20%">RATE</th>
								<th width="20%">ORGANIZER</th>
								<th width="20%">ACTIONS</th>
							</tr>
						</thead>
						<tbody class="">
						</tbody>
					</table>		
				</form>
			</div>
			<span class="text-center job-show-more"><h3 class="bottom-border" id="show-more">Show More Jobs</h3></span>
		</div>
	</div>
	<div id="myModal" class="modal fade" role="dialog">
	  <div class="modal-dialog modal-lg">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <div class="modal-header">
	        <a href="" class="pull-left" data-dismiss="modal">Back to Jobs</a>
	        <a target="_blank" href="{{route('vendor.job-detail',['id'=>1])}}" class="pull-right">Open in new window</a>
	      </div>
	      <div class="modal-body" id="quickjobdetailsdata">
	      
	      </div>
	    </div>

	  </div>
	</div>
</div>
@endsection
@section('addjavascript')
<script type="text/javascript" src="/js/vendor-job.js"></script>
<script type="text/javascript" src="/js/quick-view-jobs.js"></script>
<script type="text/javascript" id="jobsApplieds">
	var jobsApplied = {{($jobsApplied)}};
</script>
<script type="text/template" id="jobListTmpl">     
    <% _.each(data,function(v,i,l){ %> 
	    <tr class="bottom-border">
		    <td>
		        <h3><%= v.name %></h3>
		        <span>Posted at <%= v.created_at %></span>
		    </td>
		    <% if(v.isHourly == 1){ %>
		    <td>Hourly&nbsp; <%= v.minRate %>k - <%= v.maxRate %>K/Hr</td>
		    <% }else{ %>
		    <td>Fixed&nbsp; <%= v.budget %>K</td>
		    <% } %> 
		    <td><%= v.username %></td>
		    <td>
		    	<% if(!_.contains(jobsApplied,parseInt(v.id))) {	%>
			        <a href="/vendor/job/<%= v.id %>/details" id="applyToJob" class="button solid">Apply To Job</a>
			        <button type="button" class="button solid" onclick="quickJobDetails(<%= v.id %>)" data-toggle="modal" data-target="#myModal" >Quick View</button>
			        <% }else{ %>
			        <a href="/vendor/proposal/<%= v.id %>/details" id="" class="button solid">View Proposal</a>
			        <% } %>
		    </td>
		</tr>
    <% }) %>

</script>
<script type="text/template" id="quickjobdetailsTmpl">
	<div class="vf-wrap modal-job-deatils" >
		<div class="job-heading" >
			<h2><%= data.name %></h2>
			<label>Editing & proofreading</label>
			<span>&nbsp;<%= data.isHourly %> ago</span>
		</div>
		<div class="job-description">
			<div class="col-md-8 border-right">
				<div class="job-text">
					<p><%= data.description %></p><br/>
					<p>Payment:about 10 $(Depends on your capabilities If you have any suggestions. do not be afraid to offer them!)</p><br/>
					<p>Thank you!</p><br/>
				</div>
				<div class="job-info">
					<label>Attachment</label>
					<h3>@Raw food.doc(9.3mb)</h3>
					<h4>Project Type:One-time project</h4>
					<p>You will be asked to answer the following questions when submitting a proposal:</p>
						<li>1. Do you have any suggestion for my job?</li>
				</div>
			</div>
			<div class="col-md-4 jo-requirement">
				<div>
					<h4>$<%= data.maxRate %></h4>
					<p>Fixed Price</p>
				</div>
				<div>
					<h4>Intermediate Level</h4>
					<p>I am looking for a mix of experience and value</p>
				</div>
				<div>									
					<h4><%= data.created_at %></h4>
					<p>Start Date</p>
				</div>
			</div>
		</div>
		
	</div>
	<div class="text-center">
			<a href="/vendor/job/<%=data.id %>/proposal"><button class="button solid button-margin">Place Bid</button></a>
		</div>					
</script>
@endsection