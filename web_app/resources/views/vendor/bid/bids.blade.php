@extends('../layouts/base')
  @section('content')
<div class="job-posting-form container">
	<div class="row">
		<div class="vendor-job-list form-fields panel-group">
			<div class="vfform margin-zero" id="">
				<div class="title">
					<h3>PROPOSALS</h3>
				</div>
				<div class="">
					<div class="col-md-4">
						<h4>SENT</h4>
					</div>
					<div class="col-md-6">
						<h4>JOBS</h4>
					</div>
					<div class="col-md-2">
						<h4>ACTION</h4>
					</div>
				</div>
				<?php
				if($data == "") {
					?>
				 <div>
					<div class="col-md-12">
						<div class="row">
							<div class="vf-bids">
								<div class="col-md-12 text-center ">
									<h3>Currently you have no bid</h3>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php
				 }else{ ?>
			@foreach ($data as $bid)
				<div>
					<div class="col-md-12">
						<div class="row">
							<div class="vf-bids">
								<div class="col-md-4 ">
									<div class="bid-detail">
										{{date('m/d/Y', strtotime($bid['created_at']))}}
									</div>
								</div>
								<div class="col-md-6 ">
									<div class="bid-detail">
										{{$bid['name']}}
									</div>
								</div>
								<div class="col-md-2 ">
									<div class="bid-detail">
										<a href="/vendor/job/{{$bid['id']}}/details"><button class="button solid">View Job Detail	</button></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			@endforeach
			<?php }?>
			</div>
		</div>
	</div>
	
</div>
		

  @endsection