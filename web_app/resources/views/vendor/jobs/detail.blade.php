@extends('../layouts/base')
  @section('content')

		<div class="container job-deatils-page">
			<div class="row">

				<?php
				
			if (isset($data))
			{
			?>
				<div class="col-md-8">
					<div class="job-detail-panel">
						<div class="vf-wrap job-deatils">
							<div class="job-heading">
								<h2>{{$data['name']}}</h2>
								
								<span><b>Posted at</b> : @if( \Carbon\Carbon::parse($data['created_at'])->diffInMonths(\Carbon\Carbon::now()) <= 10 )
                                    {{ \Carbon\Carbon::parse($data['created_at'])->diffForHumans() }}
                                @else
                                    {{ \Carbon\Carbon::parse($data['created_at'])->format('m/d/Y') }}
                                @endif</span><br/><br/>
								<div>
									<b>Category</b>:-
									<span>{{$category['name']}}</span>
								</div>
								<div>
								<b>Sub Category:-</b>
								<span>{{$subcategory['name']}}</span>
							</div>
							</div>
							<div class="job-description">
								<div class="col-md-8 border-right">
									<div class="job-text">
										<p class="margin_bottom1">{{$data['description']}} </p>
										
										<p class="margin_bottom1">Thank you!</p>
									</div>
									<div class="job-info">
										<div>
											<?php
								if($data['isOneTime']===0) {
									?>
									<p><b>Project Type</b> : One-time project</p>
									<?php
								}else{
									?>
									<p><b>Project Type</b> : Not One-time project</p>
								<?php
								}	
								?>
										</div>
									</div>
								</div>
								<div class="col-md-4 job-requirement">
									<div>
										<?php
								if ($data['isHourly']==0) {
									?>
										<h2 class="job_budget">${{$data['budget']}}</h2>
										<p>Fixed Price</p>
										<?php
								}else{
									?>
										<h2 class="job_budget">${{$data['maxRate']}}</h3>
										<p>Hourly</p>
									<?php
									}	
										?>
									</div>
									<!-- <div>
										<h3>Intermediate Level</h3>
										<p>I am looking for a mix of experience and value</p>
									</div> -->
									<div>									
										<h3>Start Date</h3>
										<p>{{ date("m/d/Y", strtotime($data['startDateTime'])) }}</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 client-details">
					<div class="row">
						<div class="job-detail-panel client-panel">
							<div class="text-center submit-proposal-buttons">
							<?php
								$job_id = $data['id'];
								if ($hasdata['user_id'] != $user_id) {
									if ($hasdata['job_id'] != $job_id) {
							?>
									<?php if(@$tid==NULL) { ?>
									
									<a href="/vendor/job/{{$data['id']}}/proposal"><button class="button solid button-margin">Submit a Proposal</button></a><br>
									<?php }
									else{ ?>
									<a href="/vendor/job/{{$data['id']}}/{{$tid}}/proposal"><button class="button solid button-margin">Submit a Proposal</button></a><br>
									<?php } ?>
							<?php
								}
							}else{

								?>
								 	<a href="/vendor/proposal/{{$data['id']}}/details"><button class="button solid vf-btn">View Proposal</button></a>

								<?php
							}
							?>
							</div>
							<div>
								<label>About the client  </label>
							</div>
							<div class="five-star">
								<!-- <img src="/images/5-star-img.png">(0.00)0
								<span>reviews</span> -->
							</div>
						
						
							
							<div>
								<h5>{{$totalJobs}} Jobs Posted</h5>
								<!-- <p>50% Hire Rate,1 Open Job</p> -->
							</div>
						</div>
					</div>
				</div>
			<?php
			}
			else
			{
			?>
				<div class="text-center alert alert-info">
					<br>
					<h4>{{$mes}}</h4>
					<br>
				</div>
			<?php
			}
			?>
			</div>
		</div>
@endsection