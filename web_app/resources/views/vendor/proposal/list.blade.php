@extends('../layouts/base')
  @section('content')

		<div class="container job-deatils-page">
			<div class="row">
				<div class="col-md-8">
					<div class=" job-deatils">
							<div class="job-heading">
								<h1>{{$data['name']}}</h1>
								<p class="margin_bottom2">
								<span>Posted at : @if( \Carbon\Carbon::parse($data['created_at'])->diffInMonths(\Carbon\Carbon::now()) <= 10 )
                                    {{ \Carbon\Carbon::parse($data['created_at'])->diffForHumans() }}
                                @else
                                    {{ \Carbon\Carbon::parse($data['created_at'])->format('m/d/Y') }}
                                @endif </span></p>
							</div>
						</div>
					<div class="job-detail-panel">
						<div class="vf-wrap job-deatils detail-box">
							<div class="job-heading">
								<h2>Details</h2>
							</div>
							<div class="job-description">
								<div class="col-md-8">
									<div class="job-text">
										<p>{{$data['description']}} </p><br/>
									</div>
								</div>
							</div>
						</div>
					</div><br/>
					<div class="job-detail-panel">
						<div class="vf-wrap job-deatils detail-box">
							<div class="job-heading">
								<h2>Cover Letter</h2>
							</div>
							<div class="job-description">
								<div class="col-md-8">
									<div class="job-text">
										<p>{{$data['cover']}} </p><br/>
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
				<div class="col-md-3 client-details">
					<div class="row">
						<div class="job-detail-panel client-panel proposal-detail">
							<div>
								<h5>About the client  </h5>
							</div>
							<div class="text-center submit-proposal-buttons">
								<a href="/vendor/job/{{$data['id']}}/bid"><button class="button solid button-margin">Propose Diffrent Terms</button></a><br>
								<p>Withdraw Proposal</p>
							</div>
							<div class="five-star">
								<img src="/images/5-star-img.png">(0.00)0
								<span>reviews</span>
							</div>
							<div>
								<h5>{{$data['country_name']}}</h5>
								<span class="margin_bottom1">{{$data['state_name']}}  {{ date('g:i a', strtotime($data['created_at'])) }}</span>
							</div>
							<div>
								<h5>{{$totaljobs}} jobs posted</h5>

								<span>{{count($openedjob) >0 ? count($openedjob) : '0'}} Open job</span>
							</div>
	
							<div>
								<h5>60 Hours billed</h5>
							</div>
							
							<div>
								@if($totalspend!="")
								 @if(strlen((string)$totalspend) > 3)
									<h5>${{ substr($totalspend , 0 , -3) }}K + Total Spent</h5>
								@else
									<h5>${{$totalspend}} + Total Spent</h5>
								@endif
								@endif
								<span>{{count($data['hires']) > 0 ? count($data['hires']) : 0}} Hires</span>	<span>,{{ count($activevendor) ? count($activevendor) : 0 }} Active User</span>
							</div>
							<div>
								@if ($data['isHourly'] == '0')
								<h5>${{$data['budget']}}.00/hr Fixed Rate</h5>      
									@else
                                        <h5>${{$data['isHourly']}}.00/hr Avg Hourly Rate</h5>
                                    @endif
							</div>
							<div>
								<h5>Activity for this job </h5>	
								<span>20 to 50 proposals</span>
							</div>
							<div>
								<p>0 interviews</p>
								<span>Members since {{ date('M d, Y', strtotime($data['udate'])) }}</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
@section('addjavascript')
	<script type="text/javascript" src="{{asset('js/vendorProposal.js')}}"></script>
	

@endsection