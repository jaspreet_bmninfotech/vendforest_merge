@extends('layouts/base')
@section('content')
<div class="job-posting-form container">	
	@if(Session::has('success'))
	    <div class="alert alert-success">
	      <p style="color:green;">{{Session::get('success')}}</p>
	      @php Session::forget('success'); @endphp
	    </div>
	@endif
	<div class="form-fields col-md-12">
		<form role="form" method="POST" action="{{route('vendor.team.edit',$team['id'])}}" autocomplete="off">
			{!! csrf_field() !!}
			<div class="vfform">
				<div class="title">
					<h3>Edit {{$team['name']}}</h3>
				</div>
				<div class="feildcont">
						<div class="row">
							<div class="col-md-12">
								<table class="table vf-saved-job" id="teammember">
									<thead>
										<tr class="">
											<th width="10%">Sno</th>
											<th width="40%">Name</th>
											<th width="10%">Status</th>
											<th width="20%">joined At</th>
											<th width="20%">ACTIONS</th>
										</tr>
									</thead>
									<tbody class="">	
									@foreach($member as $key=>$value)
										<tr class="bottom-border" id="team{{$value->id}}">
											<td>{{ $loop->iteration }}</td>
											<td>
												<h3>{{$value->username}}</h3>
											</td>
											 <?php
                                        if ($value->status == '0'){
                                            ?>
                                            <td><p>Declined</p></td>
                                        <?php
                                        } elseif ($value->status == '1'){
                                        ?>
                                       <td> <p>Accepted</p></td>
                                        <?php
                                        }elseif ($value->status == '2'){
                                            ?>
											<td><p>Pending</p></td>
										<?php
                                        }
                                        ?>
											<td>{{date('F j,Y, h:i ',strtotime($value->created_at))}} </td>
											<td><button class="btn btn-success" onclick="deleteteammember({{$value->id}})"><span class="fa fa-times"></span></button></td>	
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
				</div>
			</div>
		@if($member->count() < 5 || $member->count() > 0 )
			<div class="vfform">
				<div class="feildcont">
					<div class="row">
						<div class="col-md-12">

							<h3 class="bottom-margin">Invite Member</h3>
							@php
								$countDiv = (5 - $member->count());
							@endphp
							@if($countDiv <= 5)
								@for ($i = 0; $i < $countDiv; $i++)
								<div class="col-md-6">
									<input type="text" placeholder=""  id="typeahead" onSelect= "function(item)"  name="member[]" >
								</div>
								<div class="col-md-6">
									<textarea name="description"></textarea>
								</div>
								<br>
								<br>
								@endfor
							@endif
						</div>
					</div>
				</div>
				
				<div class="text-center">
					<button type="submit" id="editteam" class="button solid" > Edit Team</button>
				</div>
				
				<br>
				<br>
			</div>
		@endif
		</form>
	</div>
</div>
@endsection
@section('addjavascript')
<script src="{{asset('js/team.js')}}"></script>
@endsection