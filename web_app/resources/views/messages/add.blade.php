@extends('../layouts/base')
@section('content')
<div class="job-posting-form container">
	<form role="form" method="post" >
		{!! csrf_field() !!}
		<div class="vendor-job-list form-fields panel-group ">
			<div class="vfform" id="save-job">
				<div class="title">
					<h3>Job Details</h3>
				</div>
				<div class="vf-wrap addproposal-margin " >
					<div class="col-md-12">
						<div class="row">
							<div class="job-heading" >
								<h3>{{$data['name']}}</h3>
							</div>
						</div>
					</div>
					<div class="job-description">
						<div class="col-md-8 border-right">
								<div class="row">
							<div class="job-text bottom-margin">
									<p>{{$data['description']}}</p><br/>
								</div>
							</div>
						</div>
						<div class="col-md-4 jo-requirement bottom-margin">
							<div>
								<h4></h4>
								<p>Fixed Price</p>
							</div>
							<div>
								<h4>Intermediate Level</h4>
								<p>I am looking for a mix of experience and value</p>
							</div>
							<div>									
								<h4></h4>
								<p>Start Date</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="vendor-job-list form-fields panel-group ">
			<div class="vfform" id="">
				<div class="title">
				<h3>Terms</h3>
				</div>
				<div class="row">
					<div class="feildcont">
						<div class=" vf-saved-job vf-terms" >
							<div class="bottom-border">
								<div class="col-md-8">
									<h3>Bid</h3>
									<span>Total Amount the client will see on your proposal. </span>
								</div>
								<div class="col-md-4">
									<div class="col-md-2">
										<span class="vf-dollor-sign">$</span> 
									</div>
									<div class="col-md-10">
										<input type="text" class="" name="bidPrice" id="bidPrice" value="">
									@if($errors->has('bidPrice'))
											<span class="text-danger">{{ $errors->first('bidPrice') }}</span><br/>
									@endif
										<div class="print-error-msg">
											<ul>
											</ul>
											
										</div>
									</div>
								</div>
							</div>
							<div class="bottom-border ">
								<div class="col-md-8">
									<h3>VendorForest Service Fee &nbsp;&nbsp;<span class="default-color">Explain this</span></h3>
								</div>
								<div class="col-md-4">
									<div class="col-md-2">
										<span class=" vf-dollor-sign">$</span> 
									</div>
									<div class="col-md-8">
										<input type="text" name="serviceFee" id="serviceFee" class="" value="20" readonly/>
										
									@if($errors->has('serviceFee'))
										<span class="text-danger">{{ $errors->first('serviceFee') }}</span>
									@endif
									</div>
									<div class="col-md-2">
										<span class="vf-percentage-sign">%</span> 
									</div>
								</div>
							</div>
							<div class="bottom-border">
								<div class="col-md-8">
									<h3>You'll be paid</h3>
									<span>The estimated amount you'll receive after service fees. </span>
								</div>
								<div class="col-md-4">
									<div class="col-md-2">
										<span class=" vf-dollor-sign">$</span> 
									</div>
									<div class="col-md-10">
										<input type="text" name="payment" id="payment" class="" value="" >
									@if($errors->has('payment'))
										<span class="text-danger">{{ $errors->first('payment') }}</span>
									@endif
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
		
		<div class="vendor-job-list form-fields panel-group ">
			<div class="vfform">
				<div class="title">
					<h3>Additional Information</h3>
				</div>
				<div class="">
					<div class="addproposal-margin vf-textarea-halfsize">
						<label>Cover Letter</label>
						<textarea name="cover"></textarea><br/>
					@if($errors->has('cover'))
						<span class="text-danger">{{ $errors->first('cover') }}</span><br/>
					@endif
						<label>Attachment(optional)</label>
						<input type="file" name="">
					</div>
				</div>
				<div class="text-center bottom-margin">
					<button class="button solid vf-btn" >Submit A Proposal</button> 
					<button class="button default vf-btn ">Cancel</button>
				</div>
			</div>
		</div>
	</form>
</div>
@endsection
@section('addjavascript')

<script type="text/javascript">
	$(document).ready(function() {
		var bidPrice=$("#bidPrice");
    		bidPrice.keyup(function(){
    			
	        var payment=isNaN(parseInt(bidPrice.val() * $("#serviceFee").val()/100)) ? 0 :(bidPrice.val()  * $("#serviceFee").val()/100)
	        var totalpayment=isNaN(parseInt(bidPrice.val() - payment)) ? 0 :(bidPrice.val() - payment)
	        $("#payment").val(totalpayment);
	    });

	    $(".btn-submit").click(function(e){
	    	e.preventDefault();

	    	var bidPrice = $("input[name='bidPrice']").val();
	    	var serviceFee = $("input[name='serviceFee']").val();
	    	var payment = $("input[name='payment']").val();

	        $.ajax({
	            url: "/vendor/job/bid",
	            type:'POST',
	            data: {bidPrice:bidPrice, serviceFee:serviceFee, payment:payment},
	            success: function(data) {
	                if($.isEmptyObject(data.error)){
	                	alert(data.success);
	                }else{
	                	printErrorMsg(data.error);
	                }
	            }
	        });
	    }); 

	    function printErrorMsg (msg) {
			$(".print-error-msg").find("ul").html('');
			$(".print-error-msg").css('display','block');
			$.each( msg, function( key, value ) {
				$(".print-error-msg").find("ul").append('<li>'+value+'</li>');
			});
		}
	});
</script>
@endsection