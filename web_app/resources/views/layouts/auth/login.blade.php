@extends ('layouts/base')
  @section('content')
    
    <div class="container">
      <section class="vf-login">
        <div class="vf-standar-div">
          <div class="loginbox">
            @if(Session::has('success'))
            <div class="alert alert-success">
              <p style="color:green;">{{Session::get('success')}}</p>
              @php Session::forget('success'); @endphp
            </div>
            @endif @if ($errors->any())
            <div class="alert alert-danger user-errors">
              <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
            @endif
            <h2>Login now and let the party begins</h2>
            <form class="form-input" autocomplete="off" method="POST" action="{{ route('login')}}">
              {{ csrf_field() }}
              <table class="form-table">
                <tr>
                  <td>
                    <input type="text" class="input-field" name="email" id="email" placeholder="Email or User Name" autocomplete="off">
                  </td>
                </tr>
                <tr>
                  <td>
                    <input type="password" class="input-field" name="password" id="password" placeholder="Password..." autocomplete="off">
                  </td>
                </tr>
                <tr>
                  <td>
                    <input type="checkbox" value="on">Remember me</td>
                </tr>
                  <td  class="text-center">
                    <button class="login-btn button solid" type="submit">Login Now</button>
                    <br><br>
                  </td>
                <tr>
                <tr>
                  <td class="text-center">
                    <a href="/auth/forgot">Trouble accesing your account?</a>
                  </td>
                </tr>
                </tr>
              </table>
            </form>
            <div class="text-center signupbox">
              <div class="line-heading txt-gray">I need a new account</div>
              <a class="button txt-gray default width-sm hidden-xs" href="/signup/create-account">
                  Sign up
              </a>
            </div>
          </div>
        </div>
      </section>
    </div>
  @endsection