<?php

use Illuminate\Database\Seeder;


class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

                $category = DB::table('category')->insertGetId(array('name'=>'Venues','shortCode'=>'venues','description'=>'PHP Ninga','parent_id'=>null));
 
                $subCategory = DB::table('category')->insert(array(
                 	array('name'=>'House of worship','shortCode'=>'houseofworship','description'=>'','parent_id'=>$category),
                 	array('name'=>'Hotel','shortCode'=>'hotel','description'=>'','parent_id'=>$category),
                 	array('name'=>'Banquet Hall','shortCode'=>'banquethall','description'=>'','parent_id'=>$category),
                 	array('name'=>'Country Club','shortCode'=>'countryclub','description'=>'','parent_id'=>$category),
                 	array('name'=>'Farm or Barn','shortCode'=>'farmofbarn','description'=>'','parent_id'=>$category),
                 	array('name'=>'Historic Home ','shortCode'=>'historichome','description'=>'','parent_id'=>$category),
                 	array('name'=>'Museum','shortCode'=>'museum','description'=>'','parent_id'=>$category),
                 	array('name'=>'Theater','shortCode'=>'theter','description'=>'','parent_id'=>$category),
                 	array('name'=>'Restaurant','shortCode'=>'resturant','description'=>'','parent_id'=>$category),
                 	array('name'=>'Vineyard or Winery','shortCode'=>'vineyardorwinery','description'=>'','parent_id'=>$category),
                 	array('name'=>'Modern Space or Loft ','shortCode'=>'modernspace','description'=>'','parent_id'=>$category),
                 	array('name'=>'Stadium','shortCode'=>'stadium','description'=>'','parent_id'=>$category),
                 	array('name'=>'Beach','shortCode'=>'beach','description'=>'','parent_id'=>$category),
                 	array('name'=>'Boat','shortCode'=>'boat','description'=>'','parent_id'=>$category),
                 	array('name'=>'Government Building','shortCode'=>'governmentbuilding','description'=>'','parent_id'=>$category),
                 	array('name'=>'Garden','shortCode'=>'garden','description'=>'','parent_id'=>$category)
            ));
             	$category = DB::table('category')->insertGetId( array('name'=>'Photography','shortCode'=>'photography','description'=>'','parent_id'=>null));
         		 $subCategory = DB::table('category')->insert(array(
                 	array('name'=>'Aerial photography','shortCode'=>'aerialphotography','description'=>'','parent_id'=>$category),
                 	array('name'=>'Architectural photography','shortCode'=>'architecturalphotography','description'=>'','parent_id'=>$category),
                 	array('name'=>'Candid photography','shortCode'=>'candidphotography','description'=>'','parent_id'=>$category),
                 	array('name'=>'Documentary photography','shortCode'=>'documentaryphotography','description'=>'','parent_id'=>$category),
                 	array('name'=>'Fashion photography','shortCode'=>'fashionphotography','description'=>'','parent_id'=>$category),
                 	array('name'=>'Food photography','shortCode'=>'foodphotography','description'=>'','parent_id'=>$category),
                 	array('name'=>'Landscape photography','shortCode'=>'landscapephotography','description'=>'','parent_id'=>$category),
                 	array('name'=>'Modeling photography','shortCode'=>'modelingphotography','description'=>'','parent_id'=>$category),
                 	array('name'=>'Night-long exposure photography','shortCode'=>'nightlongexposurephotography','description'=>'','parent_id'=>$category),
                 	array('name'=>'Photojournalism','shortCode'=>'photojournalism','description'=>'','parent_id'=>$category),
                 	array('name'=>'Conceptual / Fine art photography','shortCode'=>'conceptual','description'=>'','parent_id'=>$category),
                 	array('name'=>'Portraiture','shortCode'=>'portraiture','description'=>'','parent_id'=>$category),
                 	array('name'=>'Sport photography','shortCode'=>'sportphotography','description'=>'','parent_id'=>$category),
                 	array('name'=>'Street photography','shortCode'=>'streetphotography','description'=>'','parent_id'=>$category),
                 	array('name'=>'War photography','shortCode'=>'warphotography','description'=>'','parent_id'=>$category),
                 	array('name'=>'Wildlife photography','shortCode'=>'wildlife','description'=>'','parent_id'=>$category),
                 	array('name'=>'Wedding photography','shortCode'=>'weddingphotography','description'=>'','parent_id'=>$category),
                 	array('name'=>'Birthday photography','shortCode'=>'birthdayphotography','description'=>'','parent_id'=>$category),
                 	array('name'=>'Event photography','shortCode'=>'eventphotography','description'=>'','parent_id'=>$category)
             	));
         		$category = DB::table('category')->insertGetId(array('name'=>'Djs','shortCode'=>'djs','description'=>'','parent_id'=>null));
         		$subCategory = DB::table('category')->insert(array(
                 	array('name'=>'MC','shortCode'=>'mc','description'=>'','parent_id'=>$category),
                 	array('name'=>'Bilingual MC','shortCode'=>'bilingualmc','description'=>'','parent_id'=>$category),
                 	array('name'=>'Consultation','shortCode'=>'consultation','description'=>'','parent_id'=>$category),
                 	array('name'=>'Do-Not-Play List','shortCode'=>'donotplaylist','description'=>'','parent_id'=>$category),
                 	array('name'=>'Video Projection','shortCode'=>'videoprojection','description'=>'','parent_id'=>$category),
                 	array('name'=>'Club','shortCode'=>'club','description'=>'','parent_id'=>$category),
                 	array('name'=>'Mobile','shortCode'=>'mobile','description'=>'','parent_id'=>$category),
                 	array('name'=>'Music producer','shortCode'=>'musicproducer','description'=>'','parent_id'=>$category),
                 	array('name'=>'Radio','shortCode'=>'radio','description'=>'','parent_id'=>$category),
                 	array('name'=>'Turntablist','shortCode'=>'turntablist','description'=>'','parent_id'=>$category)
             	));
         		$category = DB::table('category')->insertGetId(array('name'=>'Registry','shortCode'=>'registry','description'=>'','parent_id'=>null));
         		$category = DB::table('category')->insertGetId(array('name'=>'Planners','shortCode'=>'planner','description'=>'','parent_id'=>null));
         		$subCategory = DB::table('category')->insert(array(
                 	array('name'=>'Full service or all-inclusive','shortCode'=>'fullservice','description'=>'','parent_id'=>$category),
                 	array('name'=>'Event designer','shortCode'=>'eventdesigner','description'=>'','parent_id'=>$category),
                 	array('name'=>'Partial Planning','shortCode'=>'partialplanning','description'=>'','parent_id'=>$category),
                 	array('name'=>'Timelines','shortCode'=>'timelines','description'=>'','parent_id'=>$category),
                 	array('name'=>'Vendor referrals','shortCode'=>'vendorreferrals','description'=>'','parent_id'=>$category),
                 	array('name'=>'To Do-List','shortCode'=>'todo-list','description'=>'','parent_id'=>$category),
                 	array('name'=>'A la carte planner','shortCode'=>'alacarteplanner','description'=>'','parent_id'=>$category),
                 	array('name'=>'Day of coordinator','shortCode'=>'dayofcoordinator','description'=>'','parent_id'=>$category)
             	));
         		$category = DB::table('category')->insertGetId(array('name'=>'Hair','shortCode'=>'hair','description'=>'','parent_id'=>null));
                $subCategory = DB::table('category')->insert(array(
                 	array('name'=>'Hair Trial','shortCode'=>'hairtrial','description'=>'','parent_id'=>$category),
                 	array('name'=>'On-Site Hair','shortCode'=>'onsitehair','description'=>'','parent_id'=>$category),
                 	array('name'=>'Updos/Styling','shortCode'=>'updos','description'=>'','parent_id'=>$category),
                 	array('name'=>'Extensions','shortCode'=>'extensions','description'=>'','parent_id'=>$category),
                 	array('name'=>'Coloring','shortCode'=>'coloring','description'=>'','parent_id'=>$category),
                 	array('name'=>'Haircuts','shortCode'=>'haircuts','description'=>'','parent_id'=>$category),
                 	array('name'=>'Textured Hair Styling','shortCode'=>'texturedhairstyling','description'=>'','parent_id'=>$category),
                 	array('name'=>'Keratin','shortCode'=>'keratin','description'=>'','parent_id'=>$category),
                 	array('name'=>'Blowouts','shortCode'=>'blowouts','description'=>'','parent_id'=>$category),
                 	array('name'=>'Relaxer','shortCode'=>'relaxer','description'=>'','parent_id'=>$category),
                 	array('name'=>'Perm','shortCode'=>'perm','description'=>'','parent_id'=>$category)
             	));
                $category = DB::table('category')->insertGetId(array('name'=>'Beauty','shortCode'=>'beauty','description'=>'','parent_id'=>null));
                $subCategory = DB::table('category')->insert(array(
                 	array('name'=>'Manicure / Pedicure','shortCode'=>'manicure','description'=>'','parent_id'=>$category),
                 	array('name'=>'Consultations','shortCode'=>'consultations','description'=>'','parent_id'=>$category),
                 	array('name'=>'Facials','shortCode'=>'facials','description'=>'','parent_id'=>$category),
                 	array('name'=>'Skincare','shortCode'=>'skincare','description'=>'','parent_id'=>$category),
                 	array('name'=>'Massages','shortCode'=>'massages','description'=>'','parent_id'=>$category),
                 	array('name'=>'Spa','shortCode'=>'spa','description'=>'','parent_id'=>$category),
                 	array('name'=>'Teeth Whitening','shortCode'=>'teethwhitening','description'=>'','parent_id'=>$category),
                 	array('name'=>'Men’s Grooming','shortCode'=>'mengrooming','description'=>'','parent_id'=>$category),
                 	array('name'=>'Body Art','shortCode'=>'bodyart','description'=>'','parent_id'=>$category),
             	));
                $category = DB::table('category')->insertGetId(array('name'=>'Makeup','shortCode'=>'makeup','description'=>'','parent_id'=>null));
                $subCategory = DB::table('category')->insert(array(
                 	array('name'=>'Contour Makeup','shortCode'=>'contourmakeup','description'=>'','parent_id'=>$category),
                 	array('name'=>'Fake Lashes','shortCode'=>'fakelashes','description'=>'','parent_id'=>$category),
                 	array('name'=>'Makeup Trial','shortCode'=>'makeuptrial','description'=>'','parent_id'=>$category),
                 	array('name'=>'Party Makeup','shortCode'=>'partymakeup','description'=>'','parent_id'=>$category),
                 	array('name'=>'On-Site Makeup','shortCode'=>'onsitemakeup','description'=>'','parent_id'=>$category),
                 	array('name'=>'Eye Makeup','shortCode'=>'eyemakeup','description'=>'','parent_id'=>$category),
                 	array('name'=>'Natural Makeup','shortCode'=>'naturalmakeup','description'=>'','parent_id'=>$category),
                 	array('name'=>'Airbrush Makeup','shortCode'=>'airbrushmakeup','description'=>'','parent_id'=>$category),
                 	array('name'=>'Tattoo Coverage','shortCode'=>'tattoocoverage','description'=>'','parent_id'=>$category),
                 	array('name'=>'Brown / Lash Tinting','shortCode'=>'brown','description'=>'','parent_id'=>$category)
                 ));
                $category = DB::table('category')->insertGetId(array('name'=>'Health','shortCode'=>'health','description'=>'','parent_id'=>null));
                $subCategory = DB::table('category')->insert(array(
                 	array('name'=>'Classes / Bootcamps','shortCode'=>'classes','description'=>'','parent_id'=>$category),
                 	array('name'=>'Fitness Plans','shortCode'=>'fitnessplans','description'=>'','parent_id'=>$category),
                 	array('name'=>'Meal Plans','shortCode'=>'mealplans','description'=>'','parent_id'=>$category),
                 	array('name'=>'Personal Training','shortCode'=>'personaltraining','description'=>'','parent_id'=>$category)
                 ));

                $category = DB::table('category')->insertGetId(array('name'=>'Videographer','shortCode'=>'videographer','description'=>'','parent_id'=>null));
                $subCategory = DB::table('category')->insert(array(
                 	array('name'=>'Cinematic','shortCode'=>'cinematic','description'=>'','parent_id'=>$category),
                 	array('name'=>'Documentary','shortCode'=>'documentary','description'=>'','parent_id'=>$category),
                 	array('name'=>'Short Form','shortCode'=>'shortform','description'=>'','parent_id'=>$category),
                 	array('name'=>'Storytelling','shortCode'=>'storytelling','description'=>'','parent_id'=>$category),
                 	array('name'=>'Traditional','shortCode'=>'traditional','description'=>'','parent_id'=>$category),
                 	array('name'=>'Full Feature Video','shortCode'=>'fullfeaturevideo','description'=>'','parent_id'=>$category),
                 	array('name'=>'Raw Footage','shortCode'=>'rawfootage','description'=>'','parent_id'=>$category),
                 	array('name'=>'Multiple Cameras','shortCode'=>'multiplecameras','description'=>'','parent_id'=>$category),
                 	array('name'=>'Drone Footage','shortCode'=>'dronefootage','description'=>'','parent_id'=>$category),
                 	array('name'=>'Highlight Video','shortCode'=>'highlightvideo','description'=>'','parent_id'=>$category),
                 	array('name'=>'Multiple Locations','shortCode'=>'multiplelocations','description'=>'','parent_id'=>$category),
                 	array('name'=>'One Event/Day','shortCode'=>'oneevent','description'=>'','parent_id'=>$category)
             	));
                $category = DB::table('category')->insertGetId(array('name'=>'Florists','shortCode'=>'florists','description'=>'','parent_id'=>null));	
                $subCategory = DB::table('category')->insert(array(
                 	array('name'=>'Set Up','shortCode'=>'setup','description'=>'','parent_id'=>$category),
                 	array('name'=>'Delivery','shortCode'=>'delivery','description'=>'','parent_id'=>$category),
                 	array('name'=>'Clean Up','shortCode'=>'cleanup','description'=>'','parent_id'=>$category),
                 	array('name'=>'Wholesale/Bulk','shortCode'=>'bulk','description'=>'','parent_id'=>$category),
                 	array('name'=>'Consultations','shortCode'=>'consultation','description'=>'','parent_id'=>$category),
                 	array('name'=>'Venue Visit','shortCode'=>'venuevisit','description'=>'','parent_id'=>$category)
             	));
                $category = DB::table('category')->insertGetId(array('name'=>'Caterers','shortCode'=>'caterers','description'=>'','parent_id'=>null));	
                $subCategory = DB::table('category')->insert(array(
                 	array('name'=>'Tastings','shortCode'=>'tastings','description'=>'','parent_id'=>$category),
                 	array('name'=>'Server(s)','shortCode'=>'server','description'=>'','parent_id'=>$category),
                 	array('name'=>'Buffet','shortCode'=>'buffet','description'=>'','parent_id'=>$category),
                 	array('name'=>'Cocktail Reception','shortCode'=>'cocktailreception','description'=>'','parent_id'=>$category),
                 	array('name'=>'Hors d’oeuvre','shortCode'=>'horsd’oeuvre','description'=>'','parent_id'=>$category),
                 	array('name'=>'Cake Cutting','shortCode'=>'cakecutting','description'=>'','parent_id'=>$category),
                 	array('name'=>'Plated','shortCode'=>'plated','description'=>'','parent_id'=>$category),
                 	array('name'=>'Breakfast / Brunch','shortCode'=>'breakfast','description'=>'','parent_id'=>$category),
                 	array('name'=>'Stations','shortCode'=>'stations','description'=>'','parent_id'=>$category)
                 ));
                $category = DB::table('category')->insertGetId(array('name'=>'Officiants','shortCode'=>'officiants','description'=>'','parent_id'=>null));	
                $subCategory = DB::table('category')->insert(array(
                 	array('name'=>'Non-Religious','shortCode'=>'nonreligious','description'=>'','parent_id'=>$category),
                 	array('name'=>'Interfaith','shortCode'=>'interfaith','description'=>'','parent_id'=>$category),
                 	array('name'=>'Ceremony Rehearsal','shortCode'=>'ceremonyrehearsal','description'=>'','parent_id'=>$category),
                 	array('name'=>'Vow Renewal','shortCode'=>'Vowrenewal','description'=>'','parent_id'=>$category),
                 	array('name'=>'Civil Union','shortCode'=>'civilunion','description'=>'','parent_id'=>$category)
                 ));
                $category = DB::table('category')->insertGetId(array('name'=>'Rentals','shortCode'=>'rentals','description'=>'','parent_id'=>null));	
                $subCategory = DB::table('category')->insert(array(
                 	array('name'=>'Chairs','shortCode'=>'chairs','description'=>'','parent_id'=>$category),
                 	array('name'=>'Dance Floor','shortCode'=>'dancefloor','description'=>'','parent_id'=>$category),
                 	array('name'=>'DinnerWare','shortCode'=>'dinnerware','description'=>'','parent_id'=>$category),
                 	array('name'=>'Draping','shortCode'=>'draping','description'=>'','parent_id'=>$category),
                 	array('name'=>'Equipment','shortCode'=>'equipment','description'=>'','parent_id'=>$category),
                 	array('name'=>'FlatWare','shortCode'=>'flatware','description'=>'','parent_id'=>$category),
                 	array('name'=>'GlassWare','shortCode'=>'glassware','description'=>'','parent_id'=>$category),
                 	array('name'=>'Lightning','shortCode'=>'lightning','description'=>'','parent_id'=>$category),
                 	array('name'=>'Linens','shortCode'=>'linens','description'=>'','parent_id'=>$category),
                 	array('name'=>'Lounge Furniture','shortCode'=>'loungefurniture','description'=>'','parent_id'=>
                 	$category),
                 	array('name'=>'On-Site Delivery + Setup','shortCode'=>'on-sitedelivery','description'=>'',
                 	'parent_id'=>$category),
                 	array('name'=>'Stages','shortCode'=>'stages','description'=>'','parent_id'=>$category),
                 	array('name'=>'Tables','shortCode'=>'tables','description'=>'','parent_id'=>$category),
                 	array('name'=>'Tear Down','shortCode'=>'teardown','description'=>'','parent_id'=>$category),
                 	array('name'=>'Tents','shortCode'=>'tents','description'=>'','parent_id'=>$category),
                 	array('name'=>'Instruments','shortCode'=>'instruments','description'=>'','parent_id'=>$category)
                 ));
                $category = DB::table('category')->insertGetId(array('name'=>'Dresses','shortCode'=>'dresses','description'=>'','parent_id'=>null));	
                $subCategory = DB::table('category')->insert(array(
                 	array('name'=>'Bride','shortCode'=>'bride','description'=>'','parent_id'=>$category),
                 	array('name'=>'Bridesmaids','shortCode'=>'bridesmaids','description'=>'','parent_id'=>$category),
                 	array('name'=>'Father of the Brides','shortCode'=>'fatherofthebrides','description'=>'','parent_id'=>$category),
                 	array('name'=>'Father of the Groom','shortCode'=>'fatherofthefgroom','description'=>'','parent_id'=>$category),
                 	array('name'=>'Flowers Girls','shortCode'=>'flowersgirls','description'=>'','parent_id'=>$category),
                 	array('name'=>'Groom','shortCode'=>'groom','description'=>'','parent_id'=>$category),
                 	array('name'=>'Groomsmen','shortCode'=>'groomsmen','description'=>'','parent_id'=>$category),
                 	array('name'=>'Mother of the Bride','shortCode'=>'motherofthebride','description'=>'','parent_id'=>$category),
                 	array('name'=>'Mother of the Groom','shortCode'=>'motherofthegroom','description'=>'','parent_id'=>$category),
                 	array('name'=>'Ring Bearers','shortCode'=>'ringbearers','description'=>'','parent_id'=>$category)
             	));			
                $category = DB::table('category')->insertGetId(array('name'=>'Bands','shortCode'=>'bands','description'=>'','parent_id'=>null));	
                $subCategory = DB::table('category')->insert(array(
                 	array('name'=>'Acoustic','shortCode'=>'acoustic','description'=>'','parent_id'=>$category),	
                 	array('name'=>'Big Band','shortCode'=>'bigband','description'=>'','parent_id'=>$category),	
                 	array('name'=>'Blues','shortCode'=>'blues','description'=>'','parent_id'=>$category),	
                 	array('name'=>'Caribbean','shortCode'=>'caribbean','description'=>'','parent_id'=>$category),	
                 	array('name'=>'Classic Rock','shortCode'=>'classicrock','description'=>'','parent_id'=>$category),	
                 	array('name'=>'Classical','shortCode'=>'classicrock','description'=>'','parent_id'=>$category),	
                 	array('name'=>'Country','shortCode'=>'country','description'=>'','parent_id'=>$category),	
                 	array('name'=>'Dance','shortCode'=>'dance','description'=>'','parent_id'=>$category),	
                 	array('name'=>'Folk','shortCode'=>'folk','description'=>'','parent_id'=>$category),	
                 	array('name'=>'Funk','shortCode'=>'funk','description'=>'','parent_id'=>$category),	
                 	array('name'=>'Gospel','shortCode'=>'Gospel','description'=>'','parent_id'=>$category),	
                 	array('name'=>'Hip Hop','shortCode'=>'hiphop','description'=>'','parent_id'=>$category),	
                 	array('name'=>'International','shortCode'=>'international','description'=>'','parent_id'=>
             		$category),	
                 	array('name'=>'Jazz','shortCode'=>'jazz','description'=>'','parent_id'=>$category),	
                 	array('name'=>'Latin','shortCode'=>'latin','description'=>'','parent_id'=>$category),	
                 	array('name'=>'Motown','shortCode'=>'motown','description'=>'','parent_id'=>$category),	
                 	array('name'=>'Oldies','shortCode'=>'oldies','description'=>'','parent_id'=>$category),	
                 	array('name'=>'Orchestra','shortCode'=>'orchestra','description'=>'','parent_id'=>$category),	
                 	array('name'=>'Pop','shortCode'=>'pop','description'=>'','parent_id'=>$category),	
                 	array('name'=>'Punk Rock','shortCode'=>'punkrock','description'=>'','parent_id'=>$category),	
                 	array('name'=>'R&B / Soul','shortCode'=>'soul','description'=>'','parent_id'=>$category),	
                 	array('name'=>'Reggae','shortCode'=>'reggae','description'=>'','parent_id'=>$category),	
                 	array('name'=>'Rock','shortCode'=>'rock','description'=>'','parent_id'=>$category),	
                 	array('name'=>'Strings','shortCode'=>'strings','description'=>'','parent_id'=>$category),	
                 	array('name'=>'Swing','shortCode'=>'swing','description'=>'','parent_id'=>$category)
             	));	
             	$category = DB::table('category')->insertGetId(array('name'=>'Cakes','shortCode'=>'cakes','description'=>'','parent_id'=>null));	
                $subCategory = DB::table('category')->insert(array(
                 	array('name'=>'Cheese','shortCode'=>'cheese','description'=>'','parent_id'=>$category),
                 	array('name'=>'Cookies','shortCode'=>'cookies','description'=>'','parent_id'=>$category),
                 	array('name'=>'Croquembouche','shortCode'=>'croquembouche','description'=>'','parent_id'=>$category),
                 	array('name'=>'Cupcakes','shortCode'=>'cupcakes','description'=>'','parent_id'=>$category),
                 	array('name'=>'Dessert Buffet','shortCode'=>'dessertbuffet','description'=>'','parent_id'=>$category),
                 	array('name'=>'Fruit','shortCode'=>'fruit','description'=>'','parent_id'=>$category),
                 	array('name'=>'Groom’s Cake','shortCode'=>'groom’scake','description'=>'','parent_id'=>$category),
                 	array('name'=>'Pastries','shortCode'=>'pastries','description'=>'','parent_id'=>$category)
                 ));
                $category = DB::table('category')->insertGetId(array('name'=>'Limos','shortCode'=>'limos','description'=>'','parent_id'=>null));	
                $subCategory = DB::table('category')->insert(array(
                 	array('name'=>'Classic Car','shortCode'=>'classiccar','description'=>'','parent_id'=>$category),
                 	array('name'=>'Exotic Car','shortCode'=>'exoticcar','description'=>'','parent_id'=>$category),
                 	array('name'=>'Horse and Carriage','shortCode'=>'horseandcarriage','description'=>'','parent_id'=>
                 	$category),
                 	array('name'=>'Stretch Limo','shortCode'=>'stretchlimo','description'=>'','parent_id'=>$category),
                 	array('name'=>'Motor Coach','shortCode'=>'motorcoach','description'=>'','parent_id'=>$category),
                 	array('name'=>'Motorcycle','shortCode'=>'motorcycle','description'=>'','parent_id'=>$category),
                 	array('name'=>'SUV Limo','shortCode'=>'suvlimo','description'=>'','parent_id'=>$category),
                 	array('name'=>'Sedan','shortCode'=>'sedan','description'=>'','parent_id'=>$category),
                 	array('name'=>'Shuttle','shortCode'=>'shuttle','description'=>'','parent_id'=>$category),
                 	array('name'=>'Standard Limo','shortCode'=>'standardlimo','description'=>'','parent_id'=>$category),
                 	array('name'=>'Trolley','shortCode'=>'trolley','description'=>'','parent_id'=>$category),
                 	array('name'=>'Van','shortCode'=>'van','description'=>'','parent_id'=>$category)
                 ));
                $category = DB::table('category')->insertGetId(array('name'=>'Invitations','shortCode'=>'invitations','description'=>'','parent_id'=>null));	
                $subCategory = DB::table('category')->insert(array(
                 	array('name'=>'Assembly','shortCode'=>'assembly','description'=>'','parent_id'=>$category),
                 	array('name'=>'Design','shortCode'=>'design','description'=>'','parent_id'=>$category),
                 	array('name'=>'Embossed','shortCode'=>'embossed','description'=>'','parent_id'=>$category),
                 	array('name'=>'Engraving','shortCode'=>'engraving','description'=>'','parent_id'=>$category),
                 	array('name'=>'letterpress','shortCode'=>'letterpress','description'=>'','parent_id'=>$category),
                 	array('name'=>'Printing','shortCode'=>'printing','description'=>'','parent_id'=>$category)
                 ));
                $category = DB::table('category')->insertGetId(array('name'=>'Musician','shortCode'=>'musician','description'=>'','parent_id'=>null));	
                $subCategory = DB::table('category')->insert(array(
                 	array('name'=>'Acapella','shortCode'=>'acapella','description'=>'','parent_id'=>$category),
                 	array('name'=>'Brass - Quartet','shortCode'=>'brass-quartet','description'=>'','parent_id'=>$category),
                 	array('name'=>'Brass - Quintet','shortCode'=>'brass-quintet','description'=>'','parent_id'=>$category),
                 	array('name'=>'Brass - Trio','shortCode'=>'brass-trio','description'=>'','parent_id'=>$category),
                 	array('name'=>'Choir','shortCode'=>'choir','description'=>'','parent_id'=>$category),
                 	array('name'=>'Instrumental - Duo','shortCode'=>'instrumental-duo','description'=>'','parent_id'=>
             		$category),
                 	array('name'=>'Instrumental - Solo','shortCode'=>'instrumental-solo','description'=>'','parent_id'=>$category),
                 	array('name'=>'Instrumental - Trio','shortCode'=>'instrumental-trio','description'=>'','parent_id'=>$category),
                 	array('name'=>'Orchestra','shortCode'=>'orchestra','description'=>'','parent_id'=>$category),
                 	array('name'=>'Soloist / Vocalist','shortCode'=>'soloist','description'=>'','parent_id'=>$category),
                 	array('name'=>'String - Quartet','shortCode'=>'string-quartet','description'=>'','parent_id'=>
             		$category),
                 	array('name'=>'String - Quintet','shortCode'=>'string-quintet','description'=>'','parent_id'=>
             		$category),
                 	array('name'=>'String - Trio','shortCode'=>'string-trio','description'=>'','parent_id'=>$category)
                 ));
             	$category = DB::table('category')->insertGetId(array('name'=>'Rehearsal Dinners','shortCode'=>'rehearsaldinners','description'=>'','parent_id'=>null));	
                $subCategory = DB::table('category')->insert(array(
                 	array('name'=>'Banquet Hall','shortCode'=>'banquethall','description'=>'','parent_id'=>$category),
                 	array('name'=>'Boat / Yacht','shortCode'=>'boat','description'=>'','parent_id'=>$category),
                 	array('name'=>'Country / Golf Club','shortCode'=>'golfclub','description'=>'','parent_id'=>
             		$category),
             		array('name'=>'Farm','shortCode'=>'farm','description'=>'','parent_id'=>$category),
					array('name'=>'Government Building','shortCode'=>'governmentbuilding','description'=>'','parent_id'=>$category),
					array('name'=>'Historic','shortCode'=>'historic','description'=>'','parent_id'=>$category),
					array('name'=>'Hotel','shortCode'=>'hotel','description'=>'','parent_id'=>$category),
					array('name'=>'Inn / B&B','shortCode'=>'inn','description'=>'','parent_id'=>$category),
					array('name'=>'Mansion','shortCode'=>'mansion','description'=>'','parent_id'=>$category),
					array('name'=>'Museum','shortCode'=>'museum','description'=>'','parent_id'=>$category),
					array('name'=>'Religious','shortCode'=>'religious','description'=>'','parent_id'=>$category),
					array('name'=>'Restaurant','shortCode'=>'restaurant','description'=>'','parent_id'=>$category),
					array('name'=>'Winery ','shortCode'=>'winery','description'=>'','parent_id'=>$category)
				));
				$category = DB::table('category')->insertGetId(array('name'=>'Decor & Lighting','shortCode'=>'
					decor&lighting','description'=>'','parent_id'=>null));	
                $subCategory = DB::table('category')->insert(array(
                 	array('name'=>'Aisle Decoration','shortCode'=>'aisledecoration','description'=>'','parent_id'=>$category),
                 	array('name'=>'BackDrops','shortCode'=>'backdrops','description'=>'','parent_id'=>$category),
					array('name'=>'Candles','shortCode'=>'candles','description'=>'','parent_id'=>$category),
					array('name'=>'Centerpiece','shortCode'=>'centerpiece','description'=>'','parent_id'=>$category),
					array('name'=>'Flower Girl Baskets','shortCode'=>'flowergirlbaskets','description'=>'','parent_id'=>$category),
					array('name'=>'Pedestals','shortCode'=>'pedestals','description'=>'','parent_id'=>$category),
					array('name'=>'Plants','shortCode'=>'plants','description'=>'','parent_id'=>$category),
					array('name'=>'Ring Pillows','shortCode'=>'ringpillows','description'=>'','parent_id'=>$category),
					array('name'=>'Signs','shortCode'=>'signs','description'=>'','parent_id'=>$category),
					array('name'=>'Theme Props','shortCode'=>'themeprops','description'=>'','parent_id'=>$category),
					array('name'=>'Unity Ceremony Supplies','shortCode'=>'unityceremonysupplies','description'=>'','parent_id'=>$category),
					array('name'=>'Vases','shortCode'=>'vases','description'=>'','parent_id'=>$category),
					array('name'=>'Wedding Arch','shortCode'=>'weddingarch','description'=>'','parent_id'=>$category),
					array('name'=>'Wedding Decoration Clean Up','shortCode'=>'weddingdecorationcleanup','description'=>'','parent_id'=>$category),
					array('name'=>'Wedding Decoration Delivery','shortCode'=>'weddingdecorationdelivery','description'=>'','parent_id'=>$category),
					array('name'=>'Wedding Decoration Set Up','shortCode'=>'weddingdecorationsetup','description'=>'','parent_id'=>$category),
					array('name'=>'Wedding Designers','shortCode'=>'weddingdesigners','description'=>'','parent_id'=>$category)
				));
                $category = DB::table('category')->insertGetId(array('name'=>'Travel Agents','shortCode'=>'travelagents','description'=>'','parent_id'=>null));	
                $subCategory = DB::table('category')->insert(array(
                 	array('name'=>'Africa','shortCode'=>'africa','description'=>'','parent_id'=>$category),
                 	array('name'=>'Alaska','shortCode'=>'alaska','description'=>'','parent_id'=>$category),
					array('name'=>'Asai','shortCode'=>'asai','description'=>'','parent_id'=>$category),
					array('name'=>'Australia / New Zealand','shortCode'=>'australia','description'=>'','parent_id'=>$category),
					array('name'=>'Caribbean','shortCode'=>'caribbean','description'=>'','parent_id'=>$category),
					array('name'=>'Central America','shortCode'=>'centralamerica','description'=>'','parent_id'=>$category),
					array('name'=>'Dominican Republic','shortCode'=>'dominicanrepublic','description'=>'','parent_id'=>$category),
					array('name'=>'East Europe','shortCode'=>'easteurope','description'=>'','parent_id'=>$category),
					array('name'=>'Hawaii','shortCode'=>'hawaii','description'=>'','parent_id'=>$category),
					array('name'=>'Mexico','shortCode'=>'mexico','description'=>'','parent_id'=>$category),
					array('name'=>'Middle East','shortCode'=>'middleeast','description'=>'','parent_id'=>$category),
					array('name'=>'North America','shortCode'=>'northamerica','description'=>'','parent_id'=>$category),
					array('name'=>'Pacific Islands','shortCode'=>'pacificisland','description'=>'','parent_id'=>$category),
					array('name'=>'Scandinavia','shortCode'=>'scandinavia','description'=>'','parent_id'=>$category),
					array('name'=>'South America','shortCode'=>'southamerica','description'=>'','parent_id'=>$category),
					array('name'=>'West Europe','shortCode'=>'westeurope','description'=>'','parent_id'=>$category)
				));
				$category = DB::table('category')->insertGetId(array('name'=>'Jewelers','shortCode'=>'jewelers','description'=>'','parent_id'=>null));	
                $subCategory = DB::table('category')->insert(array(
                 	array('name'=>'Antique Accessories','shortCode'=>'antiqueaccessories','description'=>'','parent_id'=>$category),
                 	array('name'=>'Antique Engagement Rings','shortCode'=>'antiqueengagementrings','description'=>'','parent_id'=>$category),
					array('name'=>'Antique Wedding Bands','shortCode'=>'antiqueweddingbands','description'=>'','parent_id'=>$category),
					array('name'=>'Custom Accessories','shortCode'=>'customaccessries','description'=>'','parent_id'=>$category),
					array('name'=>'Custom Engagement Rings','shortCode'=>'customengagementrings','description'=>'','parent_id'=>$category),
					array('name'=>'Custom Wedding Bands','shortCode'=>'customweddingbands','description'=>'','parent_id'=>$category),
					array('name'=>'Designer Accessories','shortCode'=>'designeraccessories','description'=>'','parent_id'=>$category),
					array('name'=>'Designer Engagement Rings','shortCode'=>'designerengagementrings','description'=>'','parent_id'=>$category),
					array('name'=>'Designer Wedding Rings','shortCode'=>'designerweddingrings','description'=>'','parent_id'=>$category)
				));
				$category = DB::table('category')->insertGetId(array('name'=>'Flavor & Gifts','shortCode'=>'flavor&gifts','description'=>'','parent_id'=>null));	
                $subCategory = DB::table('category')->insert(array(
                 	array('name'=>'Assembly','shortCode'=>'assembly','description'=>'','parent_id'=>$category),
                 	array('name'=>'Design','shortCode'=>'design','description'=>'','parent_id'=>$category),
                 	array('name'=>'Printing','shortCode'=>'printing','description'=>'','parent_id'=>$category)
                 ));
                $category = DB::table('category')->insertGetId(array('name'=>'Bar','shortCode'=>'bar','description'=>'','parent_id'=>null));	
                $subCategory = DB::table('category')->insert(array(
                 	array('name'=>'Bartenders','shortCode'=>'bartenders','description'=>'','parent_id'=>$category),
                 	array('name'=>'Beer','shortCode'=>'beer','description'=>'','parent_id'=>$category),
                 	array('name'=>'Champagne','shortCode'=>'champagne','description'=>'','parent_id'=>$category),
                 	array('name'=>'Liquor','shortCode'=>'liquor','description'=>'','parent_id'=>$category),
                 	array('name'=>'Non Alcoholic Drinks','shortCode'=>'nonalcoholicdrinks','description'=>'','parent_id'=>$category),
                 	array('name'=>'Signature Cocktails','shortCode'=>'signaturecocktails','description'=>'','parent_id'=>$category),
                 	array('name'=>'Wine','shortCode'=>'wine','description'=>'','parent_id'=>$category)
                 ));
                $category = DB::table('category')->insertGetId(array('name'=>'Cakes','shortCode'=>'cakes','description'=>'','parent_id'=>null));	
                $subCategory = DB::table('category')->insert(array(
                 	array('name'=>'Cake Accessories','shortCode'=>'cakeaccessories','description'=>'','parent_id'=>$category),
                 	array('name'=>'Cake Cutting + Serving','shortCode'=>'cakecuttingserving','description'=>'','parent_id'=>$category),
                 	array('name'=>'Cake Delivery + Setup','shortCode'=>'cakedeliverysetup','description'=>'','parent_id'=>$category),
                 	array('name'=>'Cake Tastings','shortCode'=>'caketestings','description'=>'','parent_id'=>$category),
                 	array('name'=>'Groom’s Cakes','shortCode'=>'groom’scakes','description'=>'','parent_id'=>$category)
                 	
                 ));
                 $category = DB::table('category')->insertGetId(array('name'=>'Accessories','shortCode'=>'accessories','description'=>'','parent_id'=>null));	
                $subCategory = DB::table('category')->insert(array(
                	array('name'=>'Beauty + Fragrance','shortCode'=>'beautyfragrance','description'=>'','parent_id'=>$category),
					array('name'=>'Clutches + Handbags','shortCode'=>'clutcheshandbags','description'=>'','parent_id'=>$category),
					array('name'=>'Cover Ups','shortCode'=>'coverups','description'=>'','parent_id'=>$category),
					array('name'=>'Hair Accessories','shortCode'=>'hairaccessories','description'=>'','parent_id'=>$category),
					array('name'=>'Lingerie','shortCode'=>'lingerie','description'=>'','parent_id'=>$category),
					array('name'=>'Sashes + Belts','shortCode'=>'sashesbelts','description'=>'','parent_id'=>$category),
					array('name'=>'Shoes','shortCode'=>'shoes','description'=>'','parent_id'=>$category),
					array('name'=>'Veils','shortCode'=>'veils','description'=>'','parent_id'=>$category)
				));
				$category = DB::table('category')->insertGetId(array('name'=>'Calligraphers','shortCode'=>'calligraphers','description'=>'','parent_id'=>null));	
                $subCategory = DB::table('category')->insert(array(
                	array('name'=>'All Inclusive','shortCode'=>'allinclusive','description'=>'','parent_id'=>$category),
					array('name'=>'Business Center','shortCode'=>'businesscenter','description'=>'','parent_id'=>$category),
					array('name'=>'Casino','shortCode'=>'casino','description'=>'','parent_id'=>$category),
					array('name'=>'Complimentary Breakfast','shortCode'=>'complimentarybreakfast','description'=>'','parent_id'=>$category),
					array('name'=>'Complimentary Parking','shortCode'=>'complimentaryparking','description'=>'','parent_id'=>$category),
					array('name'=>'Golf Course','shortCode'=>'golfcourse','description'=>'','parent_id'=>$category),
					array('name'=>'Group Activities + Tours','shortCode'=>'groupactivities','description'=>'','parent_id'=>$category),
					array('name'=>'Gym','shortCode'=>'gym','description'=>'','parent_id'=>$category),
					array('name'=>'Handicap Accessible','shortCode'=>'handicapaccessible','description'=>'','parent_id'=>$category),
					array('name'=>'Honeymoon Suite','shortCode'=>'honeymoonsuite','description'=>'','parent_id'=>$category),
					array('name'=>'Kid Friendly','shortCode'=>'kidfriendly','description'=>'','parent_id'=>$category),
					array('name'=>'On-Site Bar','shortCode'=>'onsitebar','description'=>'','parent_id'=>$category),
					array('name'=>'On-Site Restaurant','shortCode'=>'on-siterestaurant','description'=>'','parent_id'=>$category),
					array('name'=>'Pet Friendly','shortCode'=>'petfriendly','description'=>'','parent_id'=>$category),
					array('name'=>'Pool','shortCode'=>'pool','description'=>'','parent_id'=>$category),
					array('name'=>'Room Block Available','shortCode'=>'roomblockavailable','description'=>'','parent_id'=>$category),
					array('name'=>'Spa','shortCode'=>'spa','description'=>'','parent_id'=>$category),
					array('name'=>'WaterFront','shortCode'=>'waterfront','description'=>'','parent_id'=>$category)
				));
                $category = DB::table('category')->insertGetId(array('name'=>'Variety Acts','shortCode'=>'varietyacts','description'=>'','parent_id'=>null));	
                $subCategory = DB::table('category')->insert(array(
                	array('name'=>'Rehearsals + Parties','shortCode'=>'rehearsalsparties','description'=>'','parent_id'=>$category),
					array('name'=>'Wedding','shortCode'=>'wedding','description'=>'','parent_id'=>$category),
					array('name'=>'Birthday','shortCode'=>'birthday','description'=>'','parent_id'=>$category),
					array('name'=>'Ceremony','shortCode'=>'ceremony','description'=>'','parent_id'=>$category)
				));
				$category = DB::table('category')->insertGetId(array('name'=>'Alterations + Preservation','shortCode'=>'alterationspreservation','description'=>'','parent_id'=>null));	
                $subCategory = DB::table('category')->insert(array(
                	array('name'=>'Alterations','shortCode'=>'alterations','description'=>'','parent_id'=>$category),
					array('name'=>'Bridal Stylist','shortCode'=>'bridalstylist','description'=>'','parent_id'=>$category),
					array('name'=>'Custom Designs','shortCode'=>'customdesigns','description'=>'','parent_id'=>$category),
					array('name'=>'Dry Cleaning + Preservation','shortCode'=>'drycleaningpreservation','description'=>'','parent_id'=>$category)
				));
				$category = DB::table('category')->insertGetId(array('name'=>'Fitness','shortCode'=>'fitness','description'=>'','parent_id'=>null));	
                $subCategory = DB::table('category')->insert(array(
                	array('name'=>'BootCamps','shortCode'=>'bootcamps','description'=>'','parent_id'=>$category),
					array('name'=>'Diet + Nutrition','shortCode'=>'dietnutrition','description'=>'','parent_id'=>$category),
					array('name'=>'Fitness Videos','shortCode'=>'fitnessvideos','description'=>'','parent_id'=>$category),
					array('name'=>'Gyms','shortCode'=>'gyms','description'=>'','parent_id'=>$category),
					array('name'=>'Personal Trainer','shortCode'=>'personaltrainer','description'=>'','parent_id'=>$category)
				));
				$category = DB::table('category')->insertGetId(array('name'=>'Dance Lessons','shortCode'=>'dancelessons','description'=>'','parent_id'=>null));	
                $subCategory = DB::table('category')->insert(array(
                	array('name'=>'Classical','shortCode'=>'classical','description'=>'','parent_id'=>$category),
					array('name'=>'Country','shortCode'=>'country','description'=>'','parent_id'=>$category),
					array('name'=>'Dance','shortCode'=>'dance','description'=>'','parent_id'=>$category),
					array('name'=>'Disco','shortCode'=>'disco','description'=>'','parent_id'=>$category),
					array('name'=>'Folk','shortCode'=>'folk','description'=>'','parent_id'=>$category),
					array('name'=>'Hip Hop','shortCode'=>'hiphop','description'=>'','parent_id'=>$category),
					array('name'=>'Jazz','shortCode'=>'jazz','description'=>'','parent_id'=>$category),
					array('name'=>'Pop','shortCode'=>'pop','description'=>'','parent_id'=>$category),
					array('name'=>'Rock','shortCode'=>'rock','description'=>'','parent_id'=>$category),
					array('name'=>'World Music','shortCode'=>'worldmusic','description'=>'','parent_id'=>$category)
				));
                $category = DB::table('category')->insertGetId(array('name'=>'Menswear','shortCode'=>'menswear','description'=>'','parent_id'=>null));	
                $subCategory = DB::table('category')->insert(array(
                	array('name'=>'Alterations','shortCode'=>'alterations','description'=>'','parent_id'=>$category),
					array('name'=>'Custom Design','shortCode'=>'customdesign','description'=>'','parent_id'=>$category),
					array('name'=>'Group Discounts','shortCode'=>'groupdiscounts','description'=>'','parent_id'=>$category),
					array('name'=>'Online Ordering','shortCode'=>'onlineordering','description'=>'','parent_id'=>$category),
					array('name'=>'Private Appointment Available','shortCode'=>'privateappointmentavailable','description'=>'','parent_id'=>$category),
					array('name'=>'Rentals','shortCode'=>'rentals','description'=>'','parent_id'=>$category)
				));
				$category = DB::table('category')->insertGetId(array('name'=>'model','shortCode'=>'model','description'=>'','parent_id'=>null));
                $category = DB::table('category')->insertGetId(array('name'=>'guide','shortCode'=>'guide','description'=>'','parent_id'=>null));    
                 	
    }
}
