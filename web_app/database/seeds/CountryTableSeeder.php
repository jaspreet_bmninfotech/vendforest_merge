<?php

use Illuminate\Database\Seeder;


//use Faker\Factory as F;
//use use Faker\Generator as Faker;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::unprepared(file_get_contents(base_path('database/dumps/country.sql')));
    }
}
