<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     //Truncate table users;
     
    public function run()
    {
 
       $userData=array("firstName" =>"Vendor",
                            "lastName" =>"Vendor",
                            "username" =>"vendor",
       						"email" => "vendor@vendorforest.com",
                            "type" => "vn",
       						"password" => bcrypt('123456'),
                            "confirmpassword" => bcrypt('123456'),
                            "isconfirmed" => 1,
                            "dob" =>('2000-01-01'),
                            "gender" =>"m",
                            "phone" =>"9999999999",
                            "remember_token"=>'');

       $user1= new \App\User($userData);
       $user1->save();

        $userData=array("firstName" =>"Client",
                            "lastName" =>"Client",
                            "username" =>"client",
                            "email" => "client@vendorforest.com",
                            "type" => "cn",
                            "password" => bcrypt('123456'),
                            "confirmpassword" => bcrypt('123456'),
                            "isconfirmed" =>1,
                            "dob" =>('1996-08-09'),
                            "gender" =>'m',
                            "phone" =>"8888888888",
                            "remember_token"=>'');

       $user2= new \App\User($userData);
       $user2->save();
       
    }
}
