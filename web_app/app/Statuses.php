<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Statuses extends Model
{
    protected $table = 'statuses';
    public $timestamps = false;

    public static function getStatuses($content = false)
    {
        $res = \DB::table('statuses');
        if($content) {
            return $res->where('content', $content)->get();
        }else{
            return $res->get();
        }
    }
    public static function getStatusNumberByName($content, $name)
    {
        $result =  \DB::table('statuses')
            ->where('content', $content)
            ->where('name', $name)
            ->first();
        if($result == null) {
            return -1;
        }else {
            return $result->status_int;
        }
    }
}
