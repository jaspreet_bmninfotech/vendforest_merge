<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Team extends Model 
{
	 use SoftDeletes;
	const status_open = 1;
    const status_close = 0;
    const status_expired = 2;
    protected $dates = ['deleted_at'];
    protected $table = 'team';
     protected $fillable=['id','user_id','name','description'];
    public $timestamps = true;

     public static function new_team($data)
    {
    	$team = new Team();
    	$team->user_id = $data['user_id'];
    	$team->name = $data['name'];
    	$team->description = $data['description'];
      $team->status =Team::status_open;
    	$team->save();
    	return $team->id;
    }
     public function teammember()
    {
        return $this->hasMany('App\TeamMember');
    }
    public static function team_details($team_id)
  {
    $team_details = DB::table('team')->leftjoin('user','user.id','=','team.user_id')->select('user.*','team.*','user.id as uid','team.name as tname','team.id as tid')
                ->where(array('team.id' => $team_id))
                ->first();
    return sizeof($team_details) > 0 ? (array) $team_details : array();
  }
    public static function boot()
   {   
        parent::boot();    
           // cause a delete of a product to cascade to children so they are also deleted
           static::deleted(function($teamdel)
           {
           $teamdel->teammember()->delete();
           });
   }
}