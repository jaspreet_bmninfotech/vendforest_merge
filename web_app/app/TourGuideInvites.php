<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;

class TourGuideInvites extends Model
{
    const status_declined = 0;
    const status_accepted = 1;
    const status_pending = 2;

	protected $table = 'tourguideinvites';
    protected $fillable =['user_id','tourguide_job_id','inviteMessage','status'];
    public $timestamps = true;

    public static function send_invite($data)
    {
    	$invite = new TourGuideInvites();
    	$invite->user_id = $data['user_id'];
    	$invite->tourguide_job_id = $data['job_id'];
    	$invite->inviteMessage = $data['invite_message'];
        $invite->status= TourGuideInvites::status_pending;
    	$invite->save();

    	return $invite->id;
    }
    public static function is_invite_exist($job_id, $user_id)
    {
    	return DB::table('tourguideinvites')->where(array('tourguide_job_id' => $job_id, 'user_id' => $user_id))->first();
    }
    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
      public function tourguidejob()
    {
        return $this->belongsTo('App\TourGuideJob','tourguide_job_id','id');
    }
}
