<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class feedback extends Model
{
    protected $table = 'feedback';
   protected $fillable=['hire_id','client_id','vendor_id','review','rating'];
   public $timestamps = true;
}
