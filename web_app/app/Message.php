<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;
use Auth;
class Message extends Model 
{
    protected $table = 'message';
    public $timestamps = true;
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public static function get_all_messages()
    {
        $messages = DB::table('message as m')
                        ->selectRaw('m.id as message_id, m.type, m.job_id, m.invite_id, m.sender_id, s.firstName as sender_name, s.username as sender_username, m.receiver_id, j.name as job_name, (SELECT message FROM message WHERE parent_id = m.id OR id = m.id ORDER BY created_at ASC LIMIT 1) as latest_message')
                    ->leftjoin('user as s', 'm.sender_id', '=', 's.id')
    				->leftjoin('job as j', 'm.job_id', '=', 'j.id')
                    ->whereNull('m.parent_id')
    				->whereNull('m.deleted_at')
    				->where(function($query){
    					return $query
    						->orWhere(array('m.sender_id' => Auth::user()->id, 'm.receiver_id' => Auth::user()->id));
    				})
    				->get();

        return sizeof($messages) > 0 ? $messages->toArray() : array();
    }

    public static function get_message_thread($message_id)
    {
        $parent_message = DB::table('message as m')
                        ->selectRaw('m.id as message_id, m.message, m.type, DATE_FORMAT(m.created_at, "%W, %M %d, %Y") as message_date, DATE_FORMAT(m.created_at, "%H:%i") as message_time, s.firstName as sender_fname, s.lastName as sender_lname, s.username as sender_uname')
                    ->leftjoin('user as s', 'm.sender_id', '=', 's.id')
                    ->whereNull('m.deleted_at')
                    ->where(array('m.id' => $message_id))
                    ->first();

        if ($parent_message)
        {
            $thread = DB::table('message as m')
                        ->selectRaw('m.id as message_id, m.message, m.type, DATE_FORMAT(m.created_at, "%W, %M %d, %Y") as message_date, DATE_FORMAT(m.created_at, "%H:%i") as message_time, s.firstName as sender_fname, s.lastName as sender_lname, s.username as sender_uname')
                        ->leftjoin('user as s', 'm.sender_id', '=', 's.id')
                        ->whereNull('m.deleted_at')
                        ->where(array('m.parent_id' => $message_id))
                        ->get();

            if ($thread)
            {
                $parent_message->thread = $thread->toArray();
            }
        }

        return $parent_message;
    }

    public static function send_message($data)
    {
        $msg = new Message();
        $msg->parent_id = $data['parent_id'];
        $msg->sender_id = Auth::user()->id;
        $msg->receiver_id = $data['receiver_id'];
        $msg->message = $data['msg_txt'];
        if($msg->save())
        {
            return $msg;
        }

        return false;
    }
}