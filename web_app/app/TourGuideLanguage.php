<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourGuideLanguage extends Model
{
    protected $table = 'tour_guide_language';
    protected $fillable=['language','tourguideInfo_Id'];
    public $timestamps = false;
}
