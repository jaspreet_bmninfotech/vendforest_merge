<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Invites extends Model
{
    const status_declined = 0;
    const status_accepted = 1;
    const status_pending = 2;

    protected $table = 'invites';
    protected $fillable =['user_id','job_id','invite_message','status'];
    public $timestamps = true;

    public static function send_invite($data)
    {
    	$invite = new Invites();
    	$invite->user_id = $data['user_id'];
    	$invite->job_id = $data['job_id'];
    	$invite->invite_message = $data['invite_message'];
        $invite->status =Invites::status_pending;
    	$invite->save();

    	return $invite->id;
    }
    public static function is_invite_exist($job_id, $user_id)
    {
    	return DB::table('invites')->where(array('job_id' => $job_id, 'user_id' => $user_id))->first();
    }
    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
    public function job()
    {
        return $this->belongsTo('App\Job' , 'job_id','id');
    }
    public function team()
    {
        return $this->belongsTo('App\Team' , 'team_id','id');
    }
}
