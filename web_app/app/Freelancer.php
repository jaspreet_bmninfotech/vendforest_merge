<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Freelancer extends Model
{
	public static function all_records()
	{
		return DB::table('user as u')
					->selectRaw('u.id, u.firstName,u.profile_image, u.lastName, u.username, u.success_rate, bz.description as business_desc, bz.ratePerHour, bz.info, country.name as address_country')
					->leftjoin('bussiness as bz', 'bz.user_id', '=', 'u.id')
					->leftjoin('address as adr', 'adr.id', '=', 'u.address_id')
					->leftjoin('country', 'country.id', '=', 'adr.country_id')
					->where(array('u.type' => 'vn', 'isconfirmed' => 1))
					->whereRaw('u.profilePercent >= 60')
					->get();

	}
	public static function freelancer_details($user_id)
	{
		$freelancer_details = DB::table('user')
								->where(array('id' => $user_id))
								->first();
		return sizeof($freelancer_details) > 0 ? (array) $freelancer_details : array();
	}
	public static function search_freelancers($search_params)
	{
		
		$freelancers = DB::table('user as u')
					->selectRaw('u.id, u.firstName, u.lastName, u.username, u.success_rate,u.profile_image, bz.description as business_desc, bz.ratePerHour, bz.info, country.name as address_country')
					->leftjoin('bussiness as bz', 'bz.user_id', '=', 'u.id');
		$is_search = false;
		$where_str = "u.type = 'vn' AND u.isconfirmed = 1";
		if (isset($search_params['filter_name']) && $search_params['filter_name'] != '')
		{
			$is_search = true;
			$where_str .= " AND (";
			$exp_name = explode(" ", $search_params['filter_name']);
			for ($i=0; $i < sizeof($exp_name); $i++)
			{
				if ($i > 0)
				{
					$where_str .= " OR ";
				}
				// $where_str .= "u.firstName LIKE '%" . $exp_name[$i] . "%' OR u.lastName LIKE '%" . $exp_name[$i] . "%'";
				 $where_str .= "u.firstName LIKE '%" . $exp_name[$i] . "%'";
			}
			$where_str .= ")";
		}
		if (isset($search_params['filter_location']) && $search_params['filter_location'] != '')
		{
			$is_search = true;
			$where_str .= " AND country.name LIKE '%" . $search_params['filter_location'] . "%'";
		}
		if (isset($search_params['filter_category']) && $search_params['filter_category'] != '')
		{
			$is_search = true;
			$freelancers = $freelancers->join('category as cat', 'cat.id', '=', 'bz.category_id');
			$where_str .= " AND cat.id = " . $search_params['filter_category'];
		}
		if (isset($search_params['filter_hourly_rate']) && $search_params['filter_hourly_rate'] != '')
		{
			$is_search = true;
			if ($search_params['filter_hourly_rate'] == '-10')
			{
				$freelancers = $freelancers->whereBetween('ratePerHour', array(0, 10));
			}
			elseif ($search_params['filter_hourly_rate'] == '10-29')
			{
				$freelancers = $freelancers->whereBetween('ratePerHour', array(10, 30));
			}
			elseif ($search_params['filter_hourly_rate'] == '30-59')
			{
				$freelancers = $freelancers->whereBetween('ratePerHour', array(30, 60));
			}
			elseif ($search_params['filter_hourly_rate'] == '60+')
			{
				$freelancers = $freelancers->where('ratePerHour' >=60);
			}
		}
		if (isset($search_params['filter_job_success']) && $search_params['filter_job_success'] != '')
		{
			$is_search = true;
			if ($search_params['filter_job_success'] == '80')
			{
				$freelancers = $freelancers->whereBetween('success_rate', array(80, 100));
			}
			elseif ($search_params['filter_job_success'] == '90')
			{
				$freelancers = $freelancers->whereBetween('success_rate', array(90, 100));
			}
		}

		if ($is_search)
		{
			$freelancers = $freelancers->join('address as adr', 'adr.id', '=', 'u.address_id');
		}
		else
		{
			$freelancers = $freelancers->leftjoin('address as adr', 'adr.id', '=', 'u.address_id');
		}

		return $freelancers->leftjoin('country', 'country.id', '=', 'adr.country_id')
							->whereRaw($where_str)->get();
	}
}