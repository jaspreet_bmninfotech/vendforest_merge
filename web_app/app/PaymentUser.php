<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Cashier\Billable;

class PaymentUser extends Model
{
	 use Billable;
     protected $table = 'paymentuser';
     protected $fillable =['customer_id','card_brand','card_last_four','user_id'];
     public $timestamps = true;
}
