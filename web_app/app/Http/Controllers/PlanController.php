<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Plan;
use App\Subscription;
use Auth;
use Illuminate\Support\Facades\Log;
use DateTime;
use DateInterval;
use Illuminate\Support\Facades\DB;
use App\PaymentUser;
use App\Setting;
use App\Job;
use Session;
use Redirect;

class PlanController extends Controller
{
    public function index() {

        $result = Plan::where('type', 'vn')->get();
        $result1 = Plan::where('type', 'cn')->get();
        if (Auth::user()->type == 'vn') {
            $plans = $this->parsePlans($result);
            //Log::info(print_r($plans, true));
            return view('plan/index', ['plans' => $plans]);
        } else {
            $plans = $this->parsePlans($result1);
            return view('plan/index', ['plans' => $plans]);
        }
    }
    public function save($plan_id) {
        $user_id = Auth::id();
        $plan = User::select('subscription_id')->where('id', $user_id)->first();

        if ($plan) {
            $profilePrecentage = Auth::user()->profilePercent;
            if ($profilePrecentage == 50) {
                $user = User::where('id', $user_id)->update(['profilePercent' => 60]);
            }
        }
        $pay = Plan::where('id', $plan_id)->select('name', 'price', 'duration', 'frequency', 'type')->first();
        if ($plan_id != NULL) {
            if ($pay->price == 0.00) 
            {
                $date = new DateTime();
                $mapArray = ['m' => 'months',
                    'd' => 'day',
                    'y' => 'year'
                ];
                $modifier = $pay->duration.$mapArray[$pay->frequency];
                $date->modify($modifier);
                // $date->add(new \DateInterval($modifier));
                $subscription_id = Subscription::insertGetId([
                    'user_id' => $user_id,
                    'plan_id' => $plan_id,
                    'valid_until' => $date,
                    'price' => $pay->price
                ]);
            Log::info("id: $subscription_id plan_id: $plan_id, user_id: $user_id saved in Subscription.");    
                User::where('id', $user_id)->update(['subscription_id' => $subscription_id]);
            } 
            else {
                return redirect()->route('plan.buy', $plan_id);
            }
        }
        return redirect('/');
    }
    private function parsePlans($plans) 
    {
        $result = array();
        foreach($plans as $plan) {
            $row = array();
            $row["id"] = $plan->id;
            $row["name"] = $plan->name;
            $row["descriptions"] = explode(", ", $plan ->description);
            $row["price"] = $plan->price;
            $result[] = $row;
        }
        return $result;
    }
    public function buyplan(Request $request, $plan_id)
    {
        $checkuser = Auth::user()->type;
        if ($request->isMethod('post')) 
        {
            \Stripe\Stripe::setApiKey("sk_test_PHS0wV5HZJ41uaZDQsgqHKQp");
            $token = $request->Input('stripeToken');

            $Id = Auth::user()->id;
            $isCustomer = PaymentUser::where('user_id', $Id)->select('customer_id')->first();
            if (!$isCustomer) 
            {
                $customer = $this->createStripeCustomer($token, $plan_id); // CREATE STRIPE CUSTOMER
            }
            else {
                // ### IF ALREADY HAS CUSTOMER
                $customer = \Stripe\Customer::retrieve($isCustomer->customer_id);
                $cardId = $customer->sources->data[0]->id;
                if ($token)
                {
                    $customer->sources->retrieve($cardId)->delete();
                    $customer->source = $token; // obtained with Stripe.js
                    $customer->save();
                    $customerId = $customer->id;
                    $customer = \Stripe\Customer::retrieve($customerId);
                    $newcardId = $customer->sources->data[0]->id; // ### NEW CARD CREATED
                    $card = $customer->sources->retrieve($newcardId);
                    $card->save();
                }
            }
            // ### CREATE SUBSCRIPTION 
            $last4 = $customer->sources->data[0]->last4;
            $card_brand = $customer->sources->data[0]->brand;
            $subscription = $this->createSubscription($customer, $plan_id); // CREATE SUBSCRIPTION IN STRIKE
            $subscriptionId = $subscription->id;
            if ($subscription->status == "active") {
                $price = Plan::where('id', $plan_id)->select('price', 'duration', 'frequency')->first();
                $date = new DateTime();
                $mapArray = ['m' => 'months',
                    'd' => 'day',
                    'y' => 'year'
                ];
                $modifier = $price->duration.$mapArray[$price->frequency];
                $date->modify($modifier);
               
                $isSubscription = User::where('id', $Id)->select('subscription_id')->first();
                $subscriptioninfo = ['card_brand' => $card_brand,
                    'card_last_four' => $last4,
                    'subscription_id' => $subscriptionId,
                    'price' => $price->price,
                    'user_id' => $Id,
                    'plan_id' => $plan_id,
                    'valid_until' => $date
                ];
                $subsinfo = Subscription::create($subscriptioninfo); // SAVE SUBSCRIPTION INFO IN DB
                $subsinfo->save();
                if ($isSubscription['subscription_id'] == "") {
                    User::where('id', $Id)->update(['subscription_id' => $subsinfo->id]);
                } 
                else {
                    $modifier = "-3day";  // reset date to today's date
                    $date->modify($modifier);
                    // $date->sub(new DateInterval('P1D')); // VALID UNTIL UPDATE 1 DAY LESS
                    Subscription::where('id', $isSubscription->subscription_id)->update(['valid_until' => $date]);
                    $subId = Subscription::where('id', $isSubscription->subscription_id)->select('subscription_id')->first();
                    $sub = \Stripe\Subscription::retrieve($subId->subscription_id);
                    $sub->cancel();
                    User::where('id', $Id)->update(['subscription_id' => $subsinfo->id]);
                }
            }
            else {
                return redirect()-> route('plan');
            }
            Session::flash('success','Payment done successfully !');
            return Redirect::back();
        } else 
        {
            \Stripe\Stripe::setApiKey("sk_test_PHS0wV5HZJ41uaZDQsgqHKQp");
            $isCustomer = PaymentUser::where('user_id', Auth::user()->id)-> select('customer_id')->first();
            if ($isCustomer) {
                $customer = \Stripe\Customer::retrieve($isCustomer->customer_id);
                $user = Auth::user()->username;
                $plan = Plan::find($plan_id);
                return view('plan/buyPlan', ['plan' => $plan,
                    'user' => $user,
                    'customer' => $customer
                ]);
            } else {
                $user = Auth::user()->username;
                $plan = Plan::find($plan_id);
                return view('plan/buyPlan', ['plan' => $plan,
                                            'user' => $user
                ]);
            }
        }
    }
    public function createStripeCustomer($token, $plan_id)
    {
        \Stripe\Stripe::setApiKey("sk_test_PHS0wV5HZJ41uaZDQsgqHKQp");
        $customer = \Stripe\Customer::create(array(
            "plan" => $plan_id,
            "description" => "check daily payment",
            "source" => $token, // obtained with Stripe.js
        ));
        $customerId = $customer->id;
        $payment = new PaymentUser();
        $payment->user_id = Auth::user()->id;
        $payment->customer_id = $customerId;
        $payment->save();
        return $customer;
    }
    public function createSubscription($customer, $plan_id) 
    {
        \Stripe\Stripe::setApiKey("sk_test_PHS0wV5HZJ41uaZDQsgqHKQp");
        $subscription = \Stripe\Subscription::create(array(
            "customer" => $customer->id,
            "items" => array(
                array(
                    "plan" => $plan_id,
                ),
            )
        ));
        return $subscription;
    }
    public function jobpayment(Request $request,$settingId,$job_id)
    {
        if($request->isMethod('get'))
        {
             $user = Auth::user()->username;
              $setting=Setting::select('id','value')->first();
                $settingValue =$setting->value;
                //  $setting_id =base64_decode()
                //  $setting = Setting::find($settingId);
                //  $job_id  = Job::find($job_id);     
                return view('client/jobpayment/perJobpayment',['user' => $user,
                                                                'setting'=>$settingId,
                                                                'job'   =>$job_id,
                                                                'settingValue'=>$settingValue
                                                                ]);
        }
        if ($request->isMethod('post')) 
        {
            $setting=Setting::select('id','key','value')->first();
            $settingValue =$setting->value;
            \Stripe\Stripe::setApiKey("sk_test_PHS0wV5HZJ41uaZDQsgqHKQp");
            $token = $request->Input('stripeToken');
            try {
            \Stripe\Charge::create ( array (
                    "amount" => (float)$settingValue*100,
                    "currency" => "usd",
                    "source" => $token, // obtained with Stripe.js
                    "description" => "Test payment." 
            ));
            $jobId= base64_decode($job_id);
            Job::where('id',$jobId)->update(['hasPaid'=>1,'isFree'=>0]);
            Session::flash ('success','Payment done successfully!');
            return Redirect::back();
            } catch ( \Exception $e ) {       
            return Redirect::back ()->withErrors("Error! Please Try again.");
            }
        }
    }
}