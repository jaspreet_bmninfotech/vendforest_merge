<?php

namespace App\Http\Controllers;

use Auth;
use App\Category;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class HomeController extends Controller
{
    use AuthenticatesUsers;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $redirectTo = '/';
    public function __construct()
    {
        // $this->middleware('guest')->except('logout');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::User()){
            if(Auth::User()->type === 'vn'){
                return redirect()->route('vendor.dashboard');
            }
            elseif(Auth::User()->type === 'cn'){
                return redirect()->route('client.job');
            }
            elseif(Auth::User()->type === 'tg'){
                return redirect()->route('tourguide.profile');
            }
        }
        $category   = Category::select('id','name','shortCode','parent_id')
                        ->where('parent_id','=',null)->paginate(8);
        return view('index',['category' => $category ]);
    }
    public function category(){
        $category   = Category::select('id','name','shortCode','parent_id')
                        ->where('parent_id','=',null)->get();
                        echo json_encode($category, true); 
    }


}
