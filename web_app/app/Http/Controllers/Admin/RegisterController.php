<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Admin;


class RegisterController extends Controller
{
    
    use RegistersUsers;

	public function create_form()
	{
		return view('admin.register');
	} 

	public function create(Request $request)
	{
			return Admin::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);
		// dd($request->all());
	}

}
