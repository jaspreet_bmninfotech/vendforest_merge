<?php

namespace App\Http\Controllers\tourguide;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TourGuideHire;
use App\Reasons;
use App\User;
use Auth;
use App\Notifications\VendorOfferAcceptTourguide;
use App\Notifications\VendorOfferDeclineTourguide;

class TourGuideController extends Controller
{
	public function index()
	{
		return view('tourguide/index');
	}
    public function list() {

        $id = Auth::id();

        $hire_list = TourGuideHire::where([['user_id',$id], ['status', '=', '2']])->with('tourguidejob')->get();
        return view('tourguide.hire.list', [
            'tourguidehire' => $hire_list
        ]);
    }
    public function show($id)
    {
        $hire_proposal = TourGuideHire::with('tourguidejob')->find($id);
        $reasons       = Reasons::getReasons('declined');
        // $bid           = Bid::getJobBid($hire_proposal->user_id, $hire_proposal->job_id);
        if($hire_proposal == null) {
            abort(404);
        }
        return view('tourguide.hire.detail', [
            'tourguidehire'    => $hire_proposal,
            // 'bid'     => $bid,
            'reasons' => $reasons
        ]);
    }
    public function accept(Request $request, $hire_id)
    {
       if($request->get('check') == 'on') {
           $message = $request->get('message');
           if($message != null) {
               //save message
           }
           // $status_name = 'Accept';
           $status_name =TourGuideHire::status_opened;
           $operation = 'Accepted hire offers';
           $hire_proposal = TourGuideHire::with('tourguidejob')->find($hire_id);
           TourGuideHire::updateStatus($hire_id,$status_name,$message);
           $s_user = $hire_proposal->tourguidejob->user_id;
           $user = User::where('id',$s_user)->first();
           $senduser= Auth::user()->username;
           $user->notify(new VendorOfferAcceptTourguide($hire_proposal,$senduser,$operation));
           return redirect()->route('tourguide.hire.list')
               ->with('hire_flash_accept', 'Offer is accepted');
       }
    }
    public function decline(Request $request, $hire_id)
    {
        if($request->get('reason') != null) {
            // $status_name = 'Declined';
            $status_name=TourGuideHire::status_close;
            $message = $request->get('message');
            $reason = $request->get('reason');
            TourGuideHire::updateStatus($hire_id,$status_name,$message, $reason);
            $hire_proposal = TourGuideHire::with('tourguidejob')->find($hire_id);
            $operation = 'Declined hire offers';
           	$s_user = $hire_proposal->tourguidejob->user_id;
           	$user = User::where('id',$s_user)->first();
           	$senduser= Auth::user()->username;
            $user->notify(new VendorOfferDeclineTourguide($hire_proposal,$senduser, $operation));
            return back();
            return redirect()->route('tourguide.hire.list')
                ->with('hire_flash_decline', 'Hire offer declined');
        }
    }
}
