<?php

namespace App\Http\Controllers\tourGuide;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\TourGuideDeclineClientVendorJob;
use App\Notifications\TourGuideAcceptClientVendorJob;
use App\Job;
use App\Bid;
use App\Hires;
use App\TourGuideInvites;
use App\User;
use Response;
use Auth;
use Validator;
use Input;
use App\Category;
use App\Country;
use App\Attachment;
use App\Timezone;
use App\JobAttachment;
use App\Setting;
use Session;
use DB;

class JobsController extends Controller
{
 
    public function list(Request $request)
    {
        $id = Auth::user()->id;
        $tourguideinvite = DB::table('tourguideinvites')->leftjoin('tourguidejob','tourguidejob.id','=','tourguideinvites.tourguide_job_id')
            ->select('tourguideinvites.tourguide_job_id','tourguideinvites.inviteMessage','tourguidejob.status','tourguideinvites.user_id',
        'tourguideinvites.created_at','tourguidejob.title','tourguideinvites.status as clientStatus')->where('tourguideinvites.user_id','=',$id)->get();
            return view('tourguide.jobs.list', ['tourguideinvite' => $tourguideinvite ]);
    }
    public function detail($id){
        $user_id = Auth::user()->id;
        $hasdata = TourGuideInvites::select('tourguide_job_id')->where('tourguide_job_id','=',$id)->where('user_id','=',$user_id)->get();
        foreach ($hasdata as $key => $value) {
            $value['tourguide_job_id'];
        if($value['tourguide_job_id'] == $id) {
        $data = DB::table('tourguidejob')->leftjoin('tourguideinvites as tgi','tgi.tourguide_job_id','=','tourguidejob.id')->select('tourguidejob.id','tourguidejob.title','tourguidejob.created_at','tourguidejob.fromdatetime','tourguidejob.status','tgi.status as invitestatus','tourguidejob.todatetime','tourguidejob.totalpeople')->where('tourguidejob.id','=',$id)->first();
        $user =User::select('firstName','lastName','username','email')->where('id',$user_id)->first();
        return view('tourguide.jobs.detail',['data' => $data]);
        }else{
        return redirect()->route('tourguide.job');
        }
        }
    }
    public function declined(Request $request){
        $declined_id = $request->declined_id;
        $user_id = Auth::user()->id;
        $data = TourGuideInvites::where('user_id','=',$user_id)->where('tourguide_job_id','=',$declined_id)->update(['status'=>TourGuideInvites::status_declined]);
        $update =  TourGuideInvites::select('id')->where(['tourguide_job_id'=>$declined_id,'user_id'=>$user_id,'status'=>'0'])->first();
        if($declined_id!=NULL)
        {    
            if($update)
            {
                $operation = 'declined job';
                $tourguide_declinejob= TourGuideInvites::with('tourguidejob')->find($update->id); 
                $user=$tourguide_declinejob->tourguidejob->user_id;
                $senduser =User::where('id',$user)->first();
                $uname= Auth::user()->username;
                $senduser->notify(new TourGuideDeclineClientVendorJob($tourguide_declinejob,$uname, $operation));     
            }
        }
         return response(['success'=>true]);
    }
    public function accept(Request $request){
        $accept_id = $request->accept_id;
        $user_id = Auth::user()->id;
        $data = TourGuideInvites::where('user_id','=',$user_id)->where('tourguide_job_id','=',$accept_id)->update(['status'=>TourGuideInvites::status_accepted]);
         $update =  TourGuideInvites::select('id')->where(['tourguide_job_id'=>$accept_id,'user_id'=>$user_id,'status'=>'1'])->first();
        if($accept_id!=NULL)
        {    
            if($update)
            {
                $operation = 'Accepted job';
                $tourguide_acceptjob= TourGuideInvites::with('tourguidejob')->find($update->id); 
                $user=$tourguide_acceptjob->tourguidejob->user_id;
                $senduser =User::where('id',$user)->first();
                $uname= Auth::user()->username;
                $senduser->notify(new TourGuideAcceptClientVendorJob($tourguide_acceptjob,$uname,$operation));     
            }
        }
         return response(['success'=>true]);
    }
}


