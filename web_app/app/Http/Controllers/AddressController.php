<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Address;
use App\City;
use App\State;
use App\Country;
use Response;

class AddressController extends Controller
{
     public function state(Request $request,$id)
    {
    
 			$data 	=State::select('id','name')->where('country_id',$id)
 					->get();
			 return response()->json($data);
    }
     public function city(Request $request,$id)
    {
 		$data 	= City::select('id','name')->where('state_id',$id)
 				->get();
			 return response()->json($data);
    }

}
