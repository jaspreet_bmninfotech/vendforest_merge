<?php



namespace App\Http\Controllers\vendor;



use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Job;
use App\Bussiness;
use App\Category;

use App\User;

use Auth;

use App\Bid;
use DateTime;


class DashboardController extends Controller

{

    public function index(){
        $user_id =Auth::id();

        $jobApplied = Bid::select('job_id')->where('user_id','=',$user_id)->get();

        $jobAppliedValue = array();

        foreach ($jobApplied as $key => $job) {

            $jobAppliedValue[] = intval($job['job_id']);
        }
        return view ('vendor/dashboard/index', ['jobsApplied' => json_encode($jobAppliedValue)]);
    }

    public function getjobs(int $page){

        $paginate = \DB::table('job')->join('user','user.id','=','job.user_id')->select('job.id','job.name','job.maxRate','job.minRate','job.isOngoing','job.isHourly','job.user_id','job.budget','job.created_at','user.username')->where('hasPaid',1)->orWhere('isFree',1)->paginate(2,['*'], 'page',$page);
        $paginate->withPath(route('vendor.getjobs', ['page'=> intval($page + 1)]));
        echo json_encode($paginate, true);
    }

    public function quickdetails(int $jobId){

        $data = Job::select('id','name','description','maxRate','isHourly')->where('id',$jobId)->first();
       echo json_encode($data, true);  
    }
    public function detail(int $id){
        $job=Job::find($id);
        if($job)
        {
            $data = Job::select('id','name','description','maxRate','isHourly','budget','isOneTime','created_at')->where('id','=',$id)->first();

            $user_id =Auth::id();

            $hasdata = Bid::select('user_id','job_id')->where('user_id','=',$user_id)->where('job_id','=',$id)->first();

            $job_category = Job::select('category_id','subcategory_id','user_id')->where('id','=',$id)->first();
            $category = Category::select('name')->where('id','=',$job_category['category_id'])->first();

            $subcategory = Category::select('name')->where('parent_id','=',$job_category['category_id'])->where('id','=',$job_category['subcategory_id'])->first();
            $totalJobs_id = Job::select('id')->where('user_id','=',$job_category['user_id'])->get();

              $totalJobs = count($totalJobs_id);
           
        
            return view('vendor/jobs/detail',['data'=>$data,'hasdata'=>$hasdata,'category'=>$category,'subcategory'=>$subcategory,'user_id'=>$user_id,'totalJobs'=>$totalJobs]);
        }
        else
        {
            $mes = 'No Job Found';
            return view('vendor/jobs/detail',['mes'=>$mes]);
        }

    }
    public function detailJob(int $id,$tid){

        $job=Job::find($id);
        if($job)
        {
            $data = Job::select('id','name','description','maxRate','isHourly','budget','isOneTime','created_at')->where('id','=',$id)->first();
            $user_id =Auth::id();
            $hasdata = Bid::select('team_id','job_id')->where('team_id','=',$tid)->where('job_id','=',$id)->first();
            $job_category = Job::select('category_id','subcategory_id','user_id')->where('id','=',$id)->first();
            $category = Category::select('name')->where('id','=',$job_category['category_id'])->first();
            $subcategory = Category::select('name')->where('parent_id','=',$job_category['category_id'])->where('id','=',$job_category['subcategory_id'])->first();
            $totalJobs_id = Job::select('id')->where('user_id','=',$job_category['user_id'])->get();
            $totalJobs = count($totalJobs_id);
            return view('vendor/jobs/detail',['data'=>$data,'hasdata'=>$hasdata,'category'=>$category,'subcategory'=>$subcategory,'user_id'=>$user_id,'totalJobs'=>$totalJobs,'tid'=>$tid]);
        }
        else
        {
            $mes = 'No Job Found';
            return view('vendor/jobs/detail',['mes'=>$mes]);
        }

    }
    public function progressinfo(){
        $user_id = Auth::id();
      $data = Job::select('id','jobSuccess')->where('id',$user_id)->first();
        echo json_encode($data, true);   
    } 



    // public function checkJobApplied(){

        

    // }

}

