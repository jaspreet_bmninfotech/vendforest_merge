<?php

namespace App\Http\Controllers\vendor;

use App\Job;
use App\User;
use App\Bid;
use App\JobTransaction;
use App\Bussiness;
use App\Notifications\RequestedClientFund;
use App\feedback;
use App\TeamMember;
use App\Team;
use App\Hires;
use App\vendordetails;
use App\bankDetails;
use Auth;
use Carbon\Carbon;
use Validator;
use Session;
use Input;
use Response;
use Illuminate\Http\Request;
use App\Notifications\EndClientContracts;
use App\Http\Controllers\Controller;

class ContractController extends Controller
{
    public function list()
    {
        $user_id = Auth::id();
        $result= \DB::table('hires')->join('job','hires.job_id','=','job.id')->select('hires.id as hire_id','job.id','job.name','job.created_at','hires.rate','job.isHourly','job.budget')->where([['hires.user_id', $user_id]
                        ])->orWhere('hires.team_id','!=', NUll)->where('hires.status', '=', 1)->get();
       
        return view('vendor/contract/list',['data'=> $result]);
    }
    public function contractlist($id){

	   $user_id= Auth::user()->id;
        $Id= base64_decode($id);

	   	$result =\DB::table('hires')->join('job','job.id','=','hires.job_id')->select('hires.user_id','hires.rate','hires.id as hire_id','hires.job_id','hires.created_at',
            'job.name','job.isHourly','job.budget','job.maxRate')
            ->where(['hires.user_id'=> $user_id,'hires.job_id'=>$Id,'hires.team_id'=>NULL])->where('hires.status', '=', 1)->get();
           
            // dd($result);
        $team=Hires::join('job','job.id','=','hires.job_id')->join('team','team.id','=','hires.team_id')
        ->select('hires.user_id','hires.rate','hires.id as hire_id','hires.job_id','hires.created_at',
            'job.name','job.isHourly','job.budget','job.maxRate','team.id as tid','team.name as tname')->where([['hires.team_id', '!=', NULL],['hires.job_id',$Id],['team.user_id',$user_id]])->where('hires.status', '=', 1)->get();

    	 $members=[];
        if($team!="")
        {
           foreach ($team as $k => $v) {
            $members= TeamMember::where('team_id',$v['tid'])->get();
           }
        }
        return view('vendor/contract/teamplusvendors',['vendors'=>$result,'team'=>$team,'members'=>$members]);
    }
    public function detail($id,$tid=null){
          $id= base64_decode($id);
        $teamid= base64_decode($tid);

        \Stripe\Stripe::setApiKey("sk_test_WvXhIX4DW8oZLvHLsQM7DjIx");       
        $hire = Hires::select('created_at','updated_at','status', 'job_id', 'id', 'rate','team_id')->find($id);
       
        $data = Job::select('id','name','description','maxRate','user_id','isHourly','status','budget','isOneTime')->where('id','=',$hire->job_id)->first();
    	$vendor = Auth::user();
         if($hire->user_id!=Null)
        {  
        $bid = Bid::where( ['job_id' => $data->id , 'user_id' => $vendor->id] )->first();
        $serviceFee = $bid->serviceFee;
        }else{
             $bid = Bid::where( ['job_id' => $data->id , 'team_id' => $hire->team_id] )->first();
            $serviceFee = $bid->serviceFee;
        }
        $jobTransaction = JobTransaction::where(['from' => Auth::user()->id , 'hire_id' => $id])->get();

        $client_id = $data->user_id;
        $client = User::select('username','created_at', 'id')->where('id','=',$client_id)->first();
        $bussiness = Bussiness::select('name')->where('user_id','=',$client_id)->first();
        $Paid = JobTransaction::select('amt')->where([ 'hire_id' => $id ])->where('approve_release','!=',null)->get()->sum('amt');

        // $paidAmount = [];
        // foreach($Paid as $k => $v){
        //     $paidAmount []= (($hire->rate * $v->amount)/100);
        // }
        $pendingAmount = JobTransaction::where(['hire_id' => $id , 'approve_release' => null])->get()->sum('amt');

        $sendParams = [ 'data'    => $data,
                        'client'   => $client,
                        'vendor'   => $vendor,
                        'bussiness'=> $bussiness,
                        'hires'    => $hire,
                        'bid'    => $bid,
                        'jobTransaction'    => $jobTransaction,
                        'serviceFee' => $serviceFee,
                        'pendingAmount' => $pendingAmount,
                        'totalPaid' => $Paid
                    ];
                   
        return view('vendor/contract/detail',$sendParams);
    }
    public function endContract($id){
    	$user_id =Auth::id();

        $contract = Hires::select('created_at',
                            'updated_at',
                            'status',
                            'job_id',
                            'id')
                    ->find($id);

        $job = Job::select('id',
                            'name',
                            'description',
                            'maxRate',
                            'user_id',
                            'isHourly',
                            'status',
                            'budget',
                            'isOneTime')
                ->where('id','=',$contract->job_id)->first();

    	return view('vendor/contract/endContract',['contract'=>$contract,
    											   'job'=>$job
    												]);
    }
    public function contractUpdate(Request $request,$id){
    	$user =Auth::user();
    	if($request->isMethod('post'))
        {
            $request->validate([
                'review' => 'required'
                
            ]);
            $hire_id = $id;
            $status =Hires::status_close; //end hires (end contract) status
            $rating =['skill' =>Input::get('skill'),
            		'quality' =>Input::get('quality'),
            		'availability' =>Input::get('availability'),
            		'deadlines' =>Input::get('deadlines'),
            		'communication' =>Input::get('communication'),
            		'cooperation' =>Input::get('cooperation')];
            		$rating = serialize($rating);
            $review = Input::get('review');
            $user_type = $user->type;
            if ($user_type == 'cn') {
            	$client_id = $user->id;
            	$vendor_id = NULL;
            }else{
            	$vendor_id = $user->id;
            	$client_id = NULL;
            }

         //   $hire = Hires::find($hire_id);

            $hires = Hires::where('id','=',$hire_id)->update([
                'status'     => $status,
                'updated_at' => Carbon::now()
            ]);

    	    $feedback = feedback::create([
    	        'hire_id' => $hire_id,
                'client_id' => $client_id,
                'vendor_id' => $vendor_id,
                'review' => $review,
                'rating' => $rating]);
            $vendor_endcontract = Hires::with('job')->find($hire_id);
            $s_uid =$vendor_endcontract->user_id;
            $senduser =User::where('id',$s_uid)->select('username')->first();
            if($vendor_endcontract->status==0)
            {   
                $operation = 'end contracts';
                $vendor_endcontract->job->user->notify(new EndClientContracts($vendor_endcontract,$senduser,$operation));
            }

    	Session::flash('success');

            return redirect()->route('vendor.contract.detail',['id'=>$id]); 
         }
    }
    public function fundRequest(Request $request)
    {
        if($request->has('requestFund')){

            $requestedAmount = $request->requestFund;
            $vendor = $request->vendor;
            $client = $request->client;

            $hireid = $request->hire;
            $totalRate = Hires::where('id',$hireid)->first()->rate;

            $totalPaid = JobTransaction::select('amt')->where('hire_id',$hireid)->where('approve_release' , '!=','null')->get()->sum('amt');
            if( $totalPaid != 0 ){
                $restAmount = (($totalRate-$totalPaid)*$requestedAmount)/100;
            }else{
                $restAmount = ($totalRate*$requestedAmount)/100;
            }

            $data = [
                'amount' => $requestedAmount,
                'amt' => $restAmount,
                'from' => $vendor,
                'to' => $client,
                'request_fund' => now(),
                'hire_id' => $request->hire,
                'status' => 0  
            ];

            $model = JobTransaction::create($data);
            $model->save();
            
            $requestfund =JobTransaction::find($model->id);
            $senduser= User::where('id',$requestfund->to)->first();
            $operation = ' Request for fund ';
            $fromuser=Auth::user()->username;
            $senduser->notify(new RequestedClientFund($requestfund,$operation,$senduser,$fromuser));
        }else{
            

            if( $request->has('accountDetails') ){
                \Stripe\Stripe::setApiKey("sk_test_WvXhIX4DW8oZLvHLsQM7DjIx");
                $createCustomer = \Stripe\Customer::create(array(
                        "description" => Auth::user()->email
                    ));
                $customerId = $createCustomer->id;
                $creareAccount =  $acct = \Stripe\Account::create(array(
                          "type" => "custom",
                          "country" => "US",
                          "external_account" => array(
                            "object" => "bank_account",
                            "country" => "US",
                            "currency" => "usd",
                            "routing_number" => $request->accountDetails[0]["routing_number"],
                            "account_number" => $request->accountDetails[0]["account_number"],
                          ),
                          "tos_acceptance" => array(
                            "date" => strtotime("now"),
                            "ip" => "103.40.197.208"
                          )
                        ));
                $account_id = $acct->id;
                $externameAccount = $acct->external_accounts->data[0]->id;
                $saveBankDetails = bankDetails::insert(['user_id' => Auth::user()->id , 'account_id' => $account_id , 'status' => 1]);
                $bankDetails = bankDetails::where([ 'user_id' => Auth::user()->id , 'status' => 1 ])->first();
            }else{
                $bankDetails = bankDetails::where([ 'user_id' => Auth::user()->id , 'status' => 1 ])->first(); 
            }
            if( $bankDetails == null){
                return 'noBankAccount';
            }else{       
                $data = [
                    'request_release' => now(),
                ];
                $model = JobTransaction::where('id', $request->transactionId)->update($data);
                $requestfund = JobTransaction::find($request->transactionId);
                $senduser = User::where('id',$requestfund->to)->first();
                $operation = 'Request for Release fund';
                $fromuser=Auth::user()->username;
                $senduser->notify(new RequestedClientFund($requestfund,$operation,$senduser,$fromuser));
            }
        }
        if($model){
            return "true";
        }else{
            return "false";
        }
    }
    public  function showmultiple()
    {
         return view('vendor/contract/teamplusvendors');
    }
}