<?php

namespace App\Http\Controllers\vendor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\ClientBidNotify;
use App\Job;
use App\User;
use App\Bid;
use App\Invites;
use App\Setting;
use App\BidAttachments;
use App\Attachment;
use App\JobTransaction;
use Auth;
use Validator;
use Session;
use Response;
use Input;
class ProposalController extends Controller
{
    public function index($id,$tid=null)
    {  
        $job=Job::find($id); 
        if($job)
        {
    	$result =Job::join('user','user.id','=','job.user_id')
    	       ->select('job.id','job.description','job.isHourly','job.isOneTime','job.maxRate','job.budget','job.created_at','job.startDateTime','job.name','user.id')->where('job.id','=',$id)->first();
        $comission = Setting::select('key','value')->where('key','serviceCharge')->first();
        $data = json_decode( json_encode($result), true);

    	return view('vendor/proposal/add',['data'=>$data,'jobId'=>$id,'comission'=>$comission,'tid'=>$tid]);
        }
        else
        {
            $mes = 'No Job Found';
            return view('vendor/proposal/add',['mes'=>$mes]);
        }
    }
    public function create(request $request){
    	if($request->isMethod('post'))
        {
    	$user_id =Auth::id();
        $request->validate([
                            'serviceFee' => 'required',
                            'bidPrice' => 'required',
                            'payment'=>'required',
                            'cover' => 'required'
                            ]);
       $user_bid_check =  Bid::WhereNotNull('user_id')->where(['job_id'=>$request->jId, 'user_id'=>$user_id]);
        $team_bid_check =  Bid::whereNotNull('team_id')->where(['job_id'=>$request->jId, 'team_id'=>$request->tId]);

        if($user_bid_check->exists() && $team_bid_check->exists()){
            return array('message'=> "Both You & his team bid on this job.");
        }
       if($user_bid_check->exists()){
            return array('message'=>"You have already Applied bid on this job.");
       }
     if($team_bid_check->exists()){
        return array('message'=>" Team already bid on this job.");
     }
            $data = Bid::create(
                 ['serviceFee' => $request->serviceFee,
                  'bidPrice' => $request->bidPrice,
                  'payment' => $request->payment,  
                  'job_id' => $request->jId,
                  'status'  =>Bid::status_pending,
                  'cover' => $request->cover,
                ]);
            if($request->tId!=NULL)
            {
            $data['team_id'] =  $request->tId;
            }else
            {
                $data['user_id'] = $user_id;
            }
    	   $data->save();
        if($data['team_id']==NULL)
        {   
        $chechinvite= Invites::where('job_id',$request->jId)->where('user_id',$user_id)->select('id','status')->first();
        }
        else{
             $chechinvite= Invites::where('job_id',$request->jId)->where('team_id',$request->tId)->select('id','status')->first();
        }
        if($chechinvite!=NULL)
        {
            Invites::where('id',$chechinvite->id)->update(['status'=>Invites::status_accepted]);
            $operation = 'Accepted Invites and applied bid';
        }
        else{
        $operation = ' Applied Bid for Your Job';        
        }
        $bid_proposal = Bid::with('jobs')->find($data['id']);
        $clientuser = $bid_proposal->jobs->user_id;
        $uname = Auth::user()->username;
        $bid_proposal->jobs->user->notify(new ClientBidNotiFy($bid_proposal, $operation,$clientuser,$uname));
        $request->session()->put('BidId', $data['id']);
            Session::flash('success');
            if ($data) {
                    return response(['success'=>true,'id'=>$request->jId]);   
                }else{
                    return response(['success'=>false]);
                }
        }
            // return redirect()->route('vendor.proposal.{id}.detail', ['id' => $id]);
     } 
      public function coverfileuploads(Request $request)
   {
        $id = Auth::user()->id;
        $destinationPath = "uploads/vendor/Proposals".$id;   
        if ( !file_exists(public_path($destinationPath)) )
        {
            mkdir(public_path($destinationPath), 0777, true);
        }
        $files = Input::file('files');
        foreach ($files as $key => $file) {
            $ext = $file->getClientOriginalExtension();
            $fileName = substr_replace(str_slug($file->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
            $file = $file->move($destinationPath, $fileName);
            $data = Attachment::create(
                             ['name' => $fileName,
                              'path' => $file,
                              'mimeType' => $ext
                            ]
                        );
            $data->save();
            $bid_id = $request->session()->get('BidId');
            $attachment_id = $data->id;
            $datas = BidAttachments::create(
                         ['bid_id' => $bid_id,
                          'attachment_id' => $attachment_id
                        ]);
            $datas->save(); 
        }
        return response(['success'=>true]);  
    }
    public function detail(request $request,$id,$tid=null){
        $user_id =Auth::id();
        $result =\DB::table('job')
        ->leftjoin('user','user.id','=','job.user_id')
        ->leftjoin('address','address.id','=','user.address_id')
        ->leftjoin('country','country.id','=','address.country_id')
        ->leftjoin('state','state.id','=','address.state_id')
        ->leftjoin('bid','bid.job_id','=','job.id')
        ->leftjoin('bidattachment as ba','ba.bid_id','=','bid.id')
        ->leftjoin('attachment','attachment.id','=','ba.attachment_id')
        ->where('bid.user_id','=',$user_id)
        ->orWhere('bid.team_id','=',$tid)
        ->where('bid.job_id','=',$id)
        ->select('job.id','job.description','job.name','job.budget','job.maxRate','job.isHourly','user.id as uid','user.created_at as udate','bid.cover','attachment.path','ba.attachment_id', 'job.created_at','country.name as country_name','state.name as state_name')->first();
        $data = json_decode( json_encode($result), true);
      
        $totalJobs_id = Job::select('id')->where('user_id','=',$data['uid'])->get();
            $totalJobs = count($totalJobs_id);
            $user= User::with('openedJobs')->where(['id'=>$data['uid']])->first();
            $openedJobs=  $user['openedJobs'];
             $job = Job::with('hires')->where(['id'=>$id,'status'=>'1'])->first(); 
             $data['hires']= $job['hires'];

            $totalSpend = JobTransaction::select('amt')->where(['to'=> $data['uid']])->where('approve_release','!=',NULL)->get()->sum('amt');
            $activeuser=Job::with('hires')->where(['user_id'=>$data['uid'],'status'=>'1'])->get();
            $activeVendors = [];
            foreach ($activeuser as $key => $value) {
                if(!empty($value->hires->toArray())){
                    foreach ($value->hires as $k => $v) {
                        $activeVendors[] = $value->hires->toArray();
                    }

                }
            }       
    	return view('vendor/proposal/list',['data'=>$data,'totaljobs'=>$totalJobs,'openedjob'=>$openedJobs,'totalspend'=>$totalSpend,'activevendor'=>$activeVendors]);
    }
    // public function removeproposalimages(Request $request){
    //     $id = Auth::user()->id;
    //     $remove_id = $request->remove_id;
    //     $data = \DB::table('tourguideattachment')->where('tourguide_id','=',$id)->where('attachment_id','=',$remove_id)->delete();
    //     return response('success');
    // }
    
}
