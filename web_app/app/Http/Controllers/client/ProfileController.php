<?php
namespace App\Http\Controllers\client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Session;
use App\User;
use App\Country;
use App\Address;
use Input;
use Validator;
use App\Attachment;
use Auth;

class ProfileController extends Controller
{
    public function select(){
         $id =Auth::id();
        $data = User::select('firstName','lastName','username','email','countryDialcode','phone','dob','profile_image','gender')->where('id', $id)->first();
     
        // $date=Auth::user()->dob;
        // $dob= date("m-d-Y",strtotime($date));
        $attachment =\DB::table('attachment')->select('path')->where('id','=',$data['profile_image'])->first();
        $country= Country::all();                                     
        return view('client.info.profile',['data'       =>$data,
                                           'country'    => $country,'attachment'=>$attachment,
                                           // 'dob'        =>$dob
                                          ]);
    }
    public function getprofileaddress(Request $request)
    {
        $id =Auth::id();
        if($request->isMethod('get'))
        {
            $getaddress= \DB::table('user')
                            ->leftjoin('address','user.address_id','=','address.id')
                            ->select('user.id as userid','address.*')
                            ->where('user.id',$id)->get()->toArray();
                     
            $response   =   array("code" => "","success" => "","data"=>""); 
            if(!$getaddress) 
            {
                $response['code']   = 0;
                $response['success']= false;
                $response['data']   = '';
            } else 
            {   
                
                $response['code']   = 1;
                $response['success']= true;
                $response['data']   = $getaddress[0];
            }
            return response()->json($response);
        }
    }
    public function update(Request $request)
    {

        $id =Auth::id();
        if($request->isMethod('post'))
        {
            $data=Input::except(array('_token'));

                $rule=array(
                    'firstName'=>'Required_with:lastname|Alpha',
                    'lastName' =>'Required_with:firstName|Alpha',
                    'phone'     => 'required|min:10|max:12',
                    'dialCode'=>'required'
             );
            $validator=Validator::make($data, $rule);
            if($validator->fails()) {
                return redirect()->route('client.profile')->withErrors($validator)->with('profileRedirected','Please fill profile');
            }

        $id =Auth::id();
        $firstName=$request->firstName;
        $lastName=$request->lastName;
        $gender=$request->gender;
        $dialCode= $request->dialCode;
        $phone=$request->phone;
        $dob=$request->dob;
        
        $data = ['firstName' => $firstName,
                    'lastName' => $lastName,
                    'gender' => $gender,
                    'phone' => $phone,
                    'dob' => $dob,
                    'countryDialcode' =>$dialCode
                ];
           
        $profilePrecentage = Auth::user()->profilePercent;
        if($profilePrecentage == 40||$profilePrecentage==0){
            $data['profilePercent'] = 50;
        }
      
        $user = User::where('id',$id)->update($data);
         // $this->initiateEmailActivation($user);
        Session::flash('success','Successful Profile Updated ');
            if ($user) {
                    return response(['success'=>true]);   
                }else{
        return redirect()->route('client.profile'); 
        } 
    }
    }
     public function mailing(Request $request)
    {  
        if($request->isMethod('post'))
        {
            $data=Input::except(array('_token'));
                $rule=array(
                            'country'   =>  'Required_with:state|Required_with:city',
                            'state'     =>  'Required_with:country|Required_with:city',
                            'city'      =>  'Required_with:country|Required_with:state',
                            'postalCode'=>  'Required|integer|min:6',
                            'address'   =>  'Required',   
                           );
            $validator=Validator::make($data, $rule);
            if($validator->fails()) {
                return redirect()->route('client.profile')->withErrors($validator)->with('addressRedirected','Please fill address');
            }
            else
            {    
                $id =Auth::id();
                $data = User::select('address_id')->where('id', $id)->first();
                $address = [
                                            'country_id'    => $request->get('country'),
                                            'state_id'      => $request->get('state'),
                                            'city_id'       => $request->get('city'),
                                            'postalCode'    => $request->get('postalCode'),
                                            'address'       => $request->get('address')
                                            ];
                $profilePrecentage = Auth::user()->profilePercent;
                if($profilePrecentage == 50){
                    $data['profilePercent'] = 60;
                }
                if($data['address_id']==null)
                {
                    $address = Address::create($address);
                    $address->save();
                    $addressId      = $address->id;
                    $address =User::select('address_id')->where('id',$id)
                                    ->update(['address_id'=>$addressId,
                                            'profilePercent' => $data['profilePercent'],
                                            ]);
                }
            else{
                $address =Address::where('id',$data['address_id'])
                                    ->update($address);
                }
                Session::flash('success','Mailing address has Saved');
                return redirect()->route('client.profile');
            }
        }   
    }
    
    public function uploadcprofiles(Request $request)
   {
    
        $id = Auth::user()->id;
        $destinationPath = "uploads/client/profile/".$id;    
        if ( !file_exists(public_path($destinationPath)) )
        {
            mkdir(public_path($destinationPath), 0777, true);
        }
        $file = Input::file('file');
            $ext = $file->getClientOriginalExtension();
            $fileName = substr_replace(str_slug($file->getClientOriginalName()), "", -3) . '-' . rand(0, 1000) . '.' . $ext;
            $file = $file->move($destinationPath, $fileName);

        $hasattachment_id = User::select('profile_image')->where('id','=',$id)->first();
        if ($hasattachment_id['profile_image'] == "") {    
            $data = Attachment::create(
                             ['name' => $fileName,
                              'path' => $file,
                              'mimeType' => $ext
                            ]
                        );
            $data->save();
            $attachment_id = $data->id;
            $datas = user::where('id',$id)->update(
                         ['profile_image' => $attachment_id
                        ]);
        }else{
             
        $data = Attachment::where('id',$hasattachment_id['profile_image'])->update(
                             ['name' => $fileName,
                              'path' => $file,
                              'mimeType' => $ext
                            ]
                        );
        }
        return response(['success'=>true]);
    }
    
}
