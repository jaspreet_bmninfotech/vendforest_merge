<?php

namespace App\Http\Controllers\client;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\User;
use App\Plan;
use App\Subscription;
use Auth;
use DateTime;

class PlanController extends Controller
{
    //
    public function index(){
    	$result = Plan::where('type', 'cn')
        ->get();

    	$plans = $this->parsePlans($result);
        //Log::info(print_r($plans, true));
    	return view ('client/plan/index', ['plans'=>$plans]);
    }
    public function save($plan_id){
        $user_id = Auth::id();
        $date = new DateTime();
        $date->add(new \DateInterval('P1M'));
        $subscription_id = Subscription::insertGetId([
            'user_id'          => $user_id,
            'plan_id'             => $plan_id,
            'valid_until'          => $date,
        ]);
        Log::info("id: $subscription_id plan_id: $plan_id, user_id: $user_id saved in subscription.");
        User::where('id', $user_id)
            ->update(['subscription_id'=>$subscription_id]);
        $plan =User::select('subscription_id')->where('id',$user_id)->first();
        if($plan)
        {
            $profilePrecentage = Auth::user()->profilePercent;
            if($profilePrecentage == 50 ||$profilePrecentage==0){
                $user      = User::where('id',$user_id)->update(['profilePercent'=>60]);
            } 
        }
        return redirect()->route('client.job');
    }
    private function parsePlans($plans) {
        $result = array();
        foreach ($plans as $plan) {
            $row = array();
            $row["id"] = $plan->id;
            $row["name"] = $plan->name;
            $row["descriptions"] = explode(", ", $plan->description);
            $row["price"] = $plan->price;
            $result[] = $row;
        }
        return $result;
    }
}
