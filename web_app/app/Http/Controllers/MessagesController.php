<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Route;
use Auth;
class MessagesController extends Controller
{
    public function index()
    {
        $data['all_messages'] = \App\Message::get_all_messages();
        return view('messages.list')->with('data', $data);
    }

    public function get_message_thread()
    {
    	$params = Route::current()->parameters();
    	$data['messages'] = \App\Message::get_message_thread(base64_decode($params['parent_message_id']));
    	
    	echo json_encode($data);die;
    }

    public function send_message(Request $request)
    {
        $params = $request->all();
        $params['parent_id'] = base64_decode($params['parent_id']);
        
        $parent_detail = \App\Message::find($params['parent_id']);
        $params['receiver_id'] = $parent_detail->receiver_id;
        if ($parent_detail->receiver_id == Auth::user()->id)
        {
            $params['receiver_id'] = $parent_detail->sender_id;
        }

        $data['status'] = 0;
        $msg_insert = \App\Message::send_message($params);
        if ($msg_insert)
        {
            $data['status'] = 1;

            $msg_insert->sender_name = Auth::user()->firstName . " " . Auth::user()->lastName;
            if (!Auth::user()->firstName)
            {
                $msg_insert->sender_name = Auth::user()->username;
            }
            
            $msg_insert->message_time = date("H:i", strtotime($msg_insert->created_at));
            $data['message'] = $msg_insert->toArray();
        }
        
        echo json_encode($data);
        
    }
}