<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
        //get all notifications
    public function getAllNotification()
    {  
        $user  = Auth::user()->id;
        $model = DB::table('notifications')->where(['notifiable_id' => $user])->get();
        $data = [];
       foreach ($model as $key => $value) {
            $name = json_decode($value->data ,true)['senduser'];
            $message = json_decode($value->data , true)['notif']; 
            $date = json_decode($value->data , true)['date']; 
            $id = $value->id; 
            $notifiable_id = $value->notifiable_id; 
            $data[] = [ 'name'      =>$name,
                        'message'   =>$message,
                        'date'      =>$date,
                        'id'        =>$id,
                        'notifiable_id' =>$notifiable_id,

                    ];
        }
        if(Auth::User()){
            if(Auth::User()->type === 'cn'){     
                return view('notifications/allnotif/allnotification',['data'=> $data]);
            }
            elseif(Auth::User()->type === 'vn'){
                return view('notifications/allnotif/allnotification',['data'=> $data]);
            }
            elseif(Auth::User()->type === 'tg'){
                 return view('notifications/allnotif/allnotification',['data'=> $data]);
            }
        }
    }
}
