<?php

namespace App\Http\Controllers\Dispute;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Admin\Entities\Ticketit; 
use Modules\Admin\Entities\Ticket\TicketitCategory as Category;
use Auth;
use App\Job;
use App\Dispute;
use App\Hires;

use Modules\Admin\Entities\Ticket\TicketitCategoryUser as Category_user;

class DisputeController extends Controller
{

	public function index($type=null){

        $user = Auth::user();
		if($user->type=='vn'){
            if($type){
                $dispute = Dispute::with([ 'job','status'])->WhereIn('dispute_apply_by', ['cn','tg'])->where(['vendor_id'=>$user->id ])->get();
            }else{
    			$dispute = Dispute::with([ 'job','status'])->where(['dispute_apply_by'=>'vn','vendor_id'=>$user->id ])->get();
            }
		}elseif($user->type=='cn'){
            if($type){
                $dispute = Dispute::with([ 'job','status'])->WhereIn('dispute_apply_by', ['vn','tg'])->where(['vendor_id'=>$user->id ])->get();
            }else{
			     $dispute = Dispute::where(['dispute_apply_by'=>'cn','client_id'=>$user->id ])->get();
                }
		}elseif($user->type=='tg'){
             if($type){
                $dispute = Dispute::with([ 'job','status'])->WhereIn('dispute_apply_by', ['vn','cn'])->where(['vendor_id'=>$user->id ])->get();
            }else{
			     $dispute = Dispute::where(['dispute_apply_by'=>'tg','tour_guide_id'=>$user->id ])->get();
             }
		}

		return view('dispute.index', compact('dispute') );
	}
	
    public function create(){

        if(Auth::user()->type=="vn") {
           $job_ids =  Hires::where('user_id', Auth::id())->pluck('job_id')->toArray();
           if(!empty($job_ids)){
    	       $job = Job::whereIn('id', [$job_ids])->pluck('name','id');
           }
        }elseif (Auth::user()->type=="cn") {
            $job = Job::where('user_id', Auth::id() )->pluck('name','id');

        }

        if(empty($job)) {
            $job = ["dumy job 1" , "dumy job 2" ];
        }

        // Hires::
        $category = Category::pluck('name','id');
    	return view('dispute.create', compact('category', 'job'));
    }

    public function detail($id){
        $data = Dispute::with(['job', 'status' ,'category',  'dispute_verdicts', 'comment'])->whereId($id)->first();
        // dd($data, $id);
        return view('dispute.detail',compact('data'));
    }


    public function store(Request $request){

    	$users = Category_user::where('category_id',$request['category_id'])->get()->random(1);
        $request['agent_id'] = $users[0]->user_id;
    	$type ='vn';
    	$type = Auth::user()->type;
    	if($type =='vn'){
    		$request['vendor_id'] =  Auth::id();
    		$request['client_id'] = Job::select('user_id')->where('id', $request['job_id'])->first()->user_id;
    	}elseif($type=='cn'){
    		$request['client_id'] =  Auth::id();
    	}
    	$request['dispute_apply_by'] = $type;
    	$request['status_id'] = 1;
    	
    	$dispute = new Dispute();
    	$dispute->fill($request->all());
    	$dispute->save();

    	return redirect()->route('dispute.index');

    
    }
    
}
