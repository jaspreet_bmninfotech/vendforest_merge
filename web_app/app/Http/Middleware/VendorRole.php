<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;
use App\User;


class VendorRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // only access vendor
        if ($request->user()->type != 'vn') {
            return redirect()->route('/');
        }
        else{

            $session = $request->session();
            $isProfileRedirected =  $session->exists('profileRedirected');
            $isAddressRedirected =  $session->exists('addressRedirected');
            $isBussinessRedirected =  $session->exists('bussinessRedirected');
            $isPaymentRedirected =  $session->exists('paymentRedirected');

            $profilePercent = Auth::user()->profilePercent;
               
                if($profilePercent != 60 ||
                    $profilePercent < 60)
                {
                    if($profilePercent == 20 || $profilePercent == 0){
                        if(!$isProfileRedirected && 
                            $request->route()->getName() != "vendor.profile.update"){
                                return redirect()->route('vendor.profile')->with('profileRedirected','Please fill profile');
                        }
                    }
                    if($profilePercent == 30){
                        if(!$isAddressRedirected && 
                            $request->route()->getName() != "vendor.profile.mailing"){
                                return redirect()->route('vendor.profile')->with('addressRedirected','Please fill address');
                        }
                    }
                    if($profilePercent == 40){
                        if(!$isBussinessRedirected && 
                            $request->route()->getName() != "vendor.business.add"){
                                return redirect()->route('vendor.business')->with('bussinessRedirected','Please fill bussiness');
                        }
                    }
                    if($profilePercent == 50){
                        if(!$isPaymentRedirected && 
                            $request->route()->getName() != "vendor.payment.add"){
                                return redirect()->route('vendor.payment')->with('paymentRedirected','Please fill payment');
                        }
                    }
                }
        }
        return $next($request);
    }
}

