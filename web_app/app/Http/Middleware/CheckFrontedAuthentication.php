<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class CheckFrontedAuthentication
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $type=null)
    {
        dd($type);
        if(!Auth::check()){
            return redirect()->route('sign-in');
        }
        return $next($request);
    }
}
