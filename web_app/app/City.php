<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model 
{

    protected $table = 'city';
    protected $fillable =['name','state_id'];
    public $timestamps = true;

    public function state()
    {
        return $this->belongsTo('State', 'id');
    }

}