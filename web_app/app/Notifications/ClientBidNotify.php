<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ClientBidNotify extends Notification
{
    use Queueable;

    protected $user;
    protected $bids;
    protected $notif;
    protected $senduser;

   

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($bids,$notif,$user,$senduser)
    {
        $this->bids = $bids;
        $this->notif = $notif;
        $this->user = $user;
        $this->senduser = $senduser;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toDatabase($notifiable)
    {
        return [
            'date'    => Carbon::now()->format('Y-m-d H:i:s'),
            'content' => $this->bids,
            'user'    => $this->user,
            'notif'   => $this->notif,
            'senduser'=> $this->senduser
        ];
    }
    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
