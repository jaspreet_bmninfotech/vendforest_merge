<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class EndClientContracts extends Notification
{
    use Queueable;
    protected $vendorendcontract;
     protected $senduser;
    protected $notif;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($vendorendcontract,$senduser,$notif)
    {
         $this->vendorendcontract = $vendorendcontract;
        $this->senduser = $senduser;
        $this->notif = $notif;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
   public function toDatabase($notifiable)
    {
        return [
            'date'    => Carbon::now()->format('Y-m-d H:i:s'),
            'content' => $this->vendorendcontract,
            'user'    => $notifiable,
            'notif'   => $this->notif,
            'senduser'   => $this->senduser
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
