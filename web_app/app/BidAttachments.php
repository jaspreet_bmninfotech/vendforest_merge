<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BidAttachments extends Model
{
    protected $table = 'bidattachment';
   protected $fillable=['bid_id','attachment_id'];
    public $timestamps = true;


    public function bid()
    {
        return $this->belongsTo('bid');
    }

    public function Attachment()
    {
        return $this->hasOne('Attachment', 'attachment_id', 'id');
    }
}
