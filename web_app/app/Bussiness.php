<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bussiness extends Model 
{

    protected $table = 'bussiness';
    protected $fillable =['name','description','user_id','canTravel','category_id','sub_category_id','ratePerHour','ratePerProject','info','address_id'];
    public $timestamps = false;

    public function User()
    {
        return $this->belongsTo('User');
    }

    public function Category()
    {
        return $this->hasOne('Category', 'id', 'category_id');
    }

    public function SubCategory()
    {
        return $this->hasOne('Category', 'id', 'sub_category_id');
    }

    public function Attachment()
    {   
         

        return $this->hasMany('BusinessAttachment', 'id', 'business_id');
    }

    public function Address()
    {
        return $this->hasOne('Address', 'id', 'address_id');
    }

    public static $tourduration =array(
                                            '1'=>'one hour',
                                            '2'=>'two hour',
                                            '3'=>'three hour'
                                        );
}