<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
class Hires extends Model
{
	public $timestamps = true;

	const status_close = 0;
	const status_opened = 1;
	const status_pending = 2;
    
	protected $table = 'hires';
    protected $fillable=['id','job_id','user_id','status'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function job()
    {
        return $this->belongsTo('App\Job','job_id','id');
    }
	public static function save_hire($insert_arr, $user_id, $bid)
	{
		$hire = new Hires();
		$hire->user_id = $user_id;
		$hire->job_id = $insert_arr->id;
		$hire->rate = $bid->bidPrice;
		$hire->hire_description = $insert_arr->work_desc;
		$hire->created_at = date('Y-m-d H:i:s');
		$hire->status = Hires::status_pending;
		$hire->job_type = $insert_arr->isHourly;
		$hire->save();
		return $hire->id;
	}
	public static function updateStatus($hire_id, $status, $message, $reason = 0)
    {
        DB::table('hires')->where('id', $hire_id)
            ->update([
                'status' => $status,
                'message' => $message,
                'decline_reason' => $reason
            ]);

    }
}