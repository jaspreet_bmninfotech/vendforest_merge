<?php

namespace App\Logic\Activation;

use App\User;
use App\Notifications\SendActivationEmail;
use App\Traits\CaptureIpTrait;
use Carbon\Carbon;

class ActivationRepository
{
    public function createTokenAndSendEmail(User $user)
    {
        //if user changed activated email to new one
        if ($user->activated) {
            $user->update([
                'isconfirmed' => 0,
            ]);
        }

        // Send activation email notification
        self::sendNewActivationEmail($user);
    }

    public function sendNewActivationEmail(User $user)
    {
        $user->notify(new SendActivationEmail($user->id, $user->remember_token));
    }

    public function deleteExpiredActivations()
    {
        Activation::where('created_at', '<=', Carbon::now()->subHours(72))->delete();
    }
}
