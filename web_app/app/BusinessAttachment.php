<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BusinessAttachment extends Model 
{

    protected $table = 'businessattachment';
    protected $fillable=['business_id','attachment_id'];
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function Business()
    {
        return $this->belongsTo('Bussiness');
    }

    public function Attachment()
    {
        return $this->hasOne('Attachment', 'attachment_id', 'id');
    }
    

} 