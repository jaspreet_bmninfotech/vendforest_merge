<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Cashier\Billable;

class vendordetails extends Model
{
	 use Billable;
     protected $table = 'vendordetails';
     protected $fillable =['account_id','card_brand','card_last_four','user_id','token_id'];
     public $timestamps = true;
}
