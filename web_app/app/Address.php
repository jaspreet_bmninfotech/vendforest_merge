<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model 
{

    protected $table = 'address';
    protected $fillable=['city_id','country_id','state_id','postalCode','address'];
    public $timestamps = false;

    public function State()
    {
        return $this->hasOne('State', 'id', 'state_id');
    }

    public function Coutry()
    {
        return $this->hasOne('Country', 'id', 'country_id');
    }

    public function City()
    {
        return $this->hasOne('City', 'id', 'city_id');
    }

}