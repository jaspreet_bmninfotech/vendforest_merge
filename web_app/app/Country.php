<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model 
{

    protected $table = 'country';
    protected $fillable=['user_id','job_id'];
    public $timestamps = true;

    public function States()
    {
        return $this->hasMany('State', 'country_id', 'id');
    }

}