<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostAttachments extends Model
{
   protected $table = 'postattachments';
   protected $fillable=['post_id','attachment_id'];
    public $timestamps = true;


    public function post()
    {
        return $this->belongsTo('post');
    }

    public function Attachment()
    {
        return $this->hasOne('Attachment', 'attachment_id', 'id');
    }
}
