<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TourGuideAttachment extends Model
{
    protected $table = 'tourguideattachment';
   protected $fillable=['tourguide_id','attachment_id'];
    public $timestamps = true;
}
