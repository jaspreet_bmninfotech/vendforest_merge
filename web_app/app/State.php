<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model 
{

    protected $table = 'state';
    public $timestamps = false;

    public function hasCountry()
    {
        return $this->belongsTo('Country', 'id');
    }

    public function cities()
    {
        return $this->hasMany('City', 'state_id');
    }

}