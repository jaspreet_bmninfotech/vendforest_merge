<?php

namespace Modules\Admin\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Auth;
class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */


    public function showLoginForm(){

        return view('admin::auth.login');
    }

    public function login(Request $request){

        if(Auth::guard('admin')->attempt(['email'=>$request['email'] , 'password'=> $request['password'] ] )){

            return redirect()->route('admin.home.index');
        }
    }

    public function logout(){

        Auth::guard('admin')->logout();
        return back();
    }


    public function index()
    {
        return view('admin::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
