<?php
namespace Modules\Admin\Http\Controllers;
use Illuminate\Routing\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Category;
use App\Admin;
use Validator;
use Redirect;
use Auth;
use Input;
use Session;
class CategoryController extends Controller
{
    //<!--Category add--->
    public function insert()
    {
        $category=Category::all();
        return view('admin::category.insert',['category'=>$category]);

    }
     
     //<!--Category Edit-->
    public function edit($id)
    {
      
      $category =Category::find($id);         
      return view('admin::category.edit',compact('category')); 
       // $info=DB::table('category')->where('id',$id)->first();          
    }
    
    //<!--Category Update Function-->
    public function update(Request $request,$id)
    {
         if($request->isMethod('post'))
        {
          $data=Input::except(array('_token'));
          $rule=array(
              'name'=>'required',
              'description'=>'required',
          );
          $validator=Validator::make($data,$rule);
          $errors = $validator->errors();
          if($validator->fails())
          {
            return redirect()->route('category.edit')->withErrors($validator)->withInput();
          }
          $name=Input::get('name');
          $description=Input::get('description');
          $user = Category::where('id',$id)->update(['name' => $name,
                                                     'description' => $description
                                                  ]);
          return redirect()->route('category.list')->with('success','Successfully Category Update');
        }
        return redirect()->route('category.edit')->withInput();
    }
      
      //<!--Category Delete-->
    public function delete($id)
    {
       category::find($id)->delete();
       /*dd($id);
      die();*/
      return redirect()->route('category.list')->with('success','Successfully Category Delete');
    }

      //<!--Category View--->
    public function index()
    {
        $category=category::all();
        return view('admin::category.index', compact('category'));    
    }
   
     //<!--Category Add Insert Function-->
    public function addinsert(Request $request)
    {
        if($request->isMethod('post'))
        {

          $data=Input::except(array('_token'));
          $rule=array(
              'name'=>'required',
              'description'=>'required',
          );
          $validator=Validator::make($data,$rule);

         $errors = $validator->errors();
          if($validator->fails())
          {
            return redirect()->route('category.add')->withErrors($validator)->withInput()->with($errors);
          }
          else
          {
           $category = Category::create(array('name'=>Input::get("name"),'description' => Input::get("description")));
           $category->save(); 
            return redirect()->route('category.list')->with('success','Successfully Category Add');   
          }
        }
    }
}


 
     

        
