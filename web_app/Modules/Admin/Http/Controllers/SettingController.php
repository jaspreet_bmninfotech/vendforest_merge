<?php

namespace Modules\Admin\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use App\Setting;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Validator;
use Redirect;
use Auth;
use Input;
use session;

class SettingController extends Controller
{
	public function index()
	{
		$setting = Setting::get();
		return view('admin::setting.costperjob',['setting'=>$setting]);
	}
	// public function create(Request $request)
	// {
	// 	if($request->isMethod('get'))
	// 	{
	// 		return view('admin::setting.addcost');
	// 	}
		
	// 	$costvalue =$request->get('costvalue');
	// 	$costdesc =$request->get('description');
	// 	$data =Setting::create([
	// 							'value' =>$costvalue,
	// 							'description'=>$costdesc
	// 							]);
	// 	$data->save();
	// 	return redirect()->route('admin.setting.insert');

	// }
	public function update(Request $request){

		$id = $request->jobId;
		$value = $request->value;
		
		$setting =Setting::where('id',$id)->update(['value'=> $value
													
													]);
		return response('success');
	}
	
}