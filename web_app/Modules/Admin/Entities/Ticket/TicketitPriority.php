<?php

namespace Modules\Admin\Entities\Ticket;

use Illuminate\Database\Eloquent\Model;

class TicketitPriority extends Model
{
    protected $fillable = [ 'name', 'color'];
}
