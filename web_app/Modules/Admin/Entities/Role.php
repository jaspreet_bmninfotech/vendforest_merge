<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['name'];

    public function role_permisson(){
    	return $this->hasMany('Modules\Admin\Entities\RolePermisson', 'role_id');
    }
}
