<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;

class RolePermisson extends Model
{
    protected $fillable = ['role_id', 'module_id', 'status'];
}
