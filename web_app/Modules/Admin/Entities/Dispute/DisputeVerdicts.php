<?php

namespace Modules\Admin\Entities\Dispute;

use Illuminate\Database\Eloquent\Model;
// use Modules\Admin\Entities\Dispute\DisputeVerdicts;
class DisputeVerdicts extends Model
{
    protected $fillable = ['dispute_id', 'content', 'agent_id'];
}
