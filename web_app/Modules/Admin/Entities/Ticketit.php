<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;
class Ticketit extends Model
{
    protected $table;
    protected $fillable = ['subject', 'content', 'status_id', 'priority_id', 'user_id', 'agent_id', 'category_id'];

    public function __construct(){
    	$this->table = "ticketit";
    }

 //    protected $appends = ['priority'];

 //    public function getPriorityAttribute()
	// {
	//     return TicketitPriority::where('id',$this->priority_id)->first();
	// }

	public function priority() {
		return $this->belongsto('Modules\Admin\Entities\Ticket\TicketitPriority' , 'priority_id');
	}

	public function status_name() {
		return $this->belongsto('Modules\Admin\Entities\Ticket\TicketitStatus' , 'status_id');
	}

	public function category_name() {
		return $this->belongsto('Modules\Admin\Entities\Ticket\TicketitCategory' , 'category_id');
	}



	public function agent() {
		return $this->belongsto('App\User' , 'agent_id');
	}

	public static function complete(){
		if(admin_role_id() ==2){
			$user_id = admin_user_id();
			return self::where(['status_id'=> 2, 'agent_id'=>$user_id])->count();
		}
		return self::where('status_id', 2)->count();
	}

	public static function active(){

		if(admin_role_id() ==2){
			$user_id = admin_user_id();
			return self::whereNotIn('status_id', [2])->where(['agent_id'=>$user_id])->count();
		}
		return self::whereNotIn('status_id', [2])->count();
	}
}
