@extends ('admin::layouts/base') 
  @section('content')
        @if(Session::has('success'))
          <div class="alert alert-success">
            <p style="color:green;">{{Session::get('success')}}</p>
            @php Session::forget('success');
              
             @endphp
          </div>
        @endif
<div class="page-title">                    
   @include('admin::role.create')
    <h2><span class="fa fa-arrow-circle-o-left"></span> Role List</h2>
</div>                                         
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
        
            <div class="">
                <div class="panel panel-default" style="">
                    <div class="panel-body">
                        @if($data)
                        <table class="table datatable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>TITLE</th>
                                <th>Assign Permisson</th>
                            </tr> 
                        </thead>
                        <tbody>

                            @foreach($data as $key => $val)
                            <tr>
                                <td>{{$val->id}}</td>
                                <td>{{$val->name}}</td>
                                <td><a href="{{route('role.permisson',['role_id'=>$val->id ])}}"> Assign Permisson</a></td>
                            </tr>


                            @endforeach 
                       
                        </tbody>
                        </table>
                       @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
