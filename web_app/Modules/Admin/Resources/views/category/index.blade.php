@extends ('admin::layouts/base') 
  @section('content')
        @if(Session::has('success'))
          <div class="alert alert-success">
            <p style="color:green;">{{Session::get('success')}}</p>
            @php Session::forget('success'); @endphp
          </div>
        @endif
<div class="page-title">                    
    <h2><span class="fa fa-arrow-circle-o-left"></span> Categories List</h2>
    <a href="{{route('category.add')}}" class="btn btn-info pull-right" role="button">ADD CATEGORY</a>
</div>                                         
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="">
                <div class="panel panel-default" style="">
                    <div class="panel-body">
                        @if($category)
                        <table class="table datatable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>NAME</th>
                                <th>DESCRIPTION</th>
                                <th>CREATED DATE</th>
                                <th class="text-center">ACTION</th>
                            </tr> 
                        </thead>
                        <tbody>
                        @foreach($category as $category)
                            <tr>
                                <td>{{$category->id}}</td>
                                <td>{{$category->name}}</td>
                                <td>{{$category->description}}</td>
                                <td>{{$category->created_at ? $category->created_at->diffForHumans() : 'no date'}}</td>
                                <td class="text-center"><a class="btn btn-raised btn-primary btn-sm" href="{{route('category.edit',$category->id)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a> ||
                                <form method="post" id= "delete-form-{{$category->id}}" action="{{route('category.delete',$category->id)}}" style="display: none;">
                                    {{ csrf_field() }}
                                    {{ method_field('delete') }}
                                </form>
                               <button onclick="if(confirm('Are You Sure, You Went To Delete This?')){
                                        event.preventDefault();
                                        document.getElementById('delete-form-{{ $category->id}}').submit();
                                    }else{
                                        event.preventDefault();
                                        }" 
                                        class="btn btn-raised btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                                </td>
                            </tr>
                        @endforeach    
                        </tbody>
                        </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
