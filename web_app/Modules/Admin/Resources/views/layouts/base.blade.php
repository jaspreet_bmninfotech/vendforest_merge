<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Admin :: Vendor Forest</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
{{-- <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css"> --}}
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="{{asset('assets/admin/css/theme-default.css')}}"/>
        <link rel="stylesheet" type="text/css" id="theme" href="{{asset('assets/admin/css/index.css')}}"/>
       
         @section('addstyle')
         @show
        <!-- EOF CSS INCLUDE -->                                       
    </head>
    
  <!-- START PRELOADS -->
    <body>
        <div class="page-container">
            @include ('admin::layouts/admin-sidebar')
            <!-- PAGE CONTENT -->
            <div class="page-content">
                @section ('header')
                    @include ('admin::layouts/admin-header')
                @show 
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    
                </ul>
                <div class="page-content-wrap">
                
                    <div class="row">
                        <div class="col-md-12">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
       
        <script type="text/javascript" src="{{asset('assets/admin/js/plugins/jquery/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/admin/js/plugins/jquery/jquery-ui.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/admin/js/plugins/bootstrap/bootstrap.min.js')}}"></script>        
        <!-- END PLUGINS -->

        <!-- START THIS PAGE PLUGINS-->        
        <script type='text/javascript' src="{{asset('assets/admin/js/plugins/icheck/icheck.min.js')}}"></script>        
        <script type="text/javascript" src="{{asset('assets/admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/admin/js/plugins/scrolltotop/scrolltopcontrol.js')}}"></script>
        
        <script type="text/javascript" src="{{asset('assets/admin/js/plugins/morris/raphael-min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/admin/js/plugins/morris/morris.min.js')}}"></script>       
        <script type="text/javascript" src="{{asset('assets/admin/js/plugins/rickshaw/d3.v3.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/admin/js/plugins/rickshaw/rickshaw.min.js')}}"></script>
        <script type='text/javascript' src="{{asset('assets/admin/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
        <script type='text/javascript' src="{{asset('assets/admin/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>                
        <script type='text/javascript' src="{{asset('assets/admin/js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script>                
        <script type="text/javascript" src="{{asset('assets/admin/js/plugins/owl/owl.carousel.min.js')}}"></script>                 
        
        <script type="text/javascript" src="{{asset('assets/admin/js/plugins/moment.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/admin/js/plugins/daterangepicker/daterangepicker.js')}}"></script>
        <!-- END THIS PAGE PLUGINS-->        

        <!-- START TEMPLATE -->
        <script type="text/javascript" src="{{asset('assets/admin/js/plugins/datatables/jquery.dataTables.min.js')}}"></script> 
         <script type="text/javascript" src="{{asset('assets/admin/js/settings.js')}}"></script>
        
        <script type="text/javascript" src="{{asset('assets/admin/js/plugins.js')}}"></script>        
        <script type="text/javascript" src="{{asset('assets/admin/js/actions.js')}}"></script>
        
        <script type="text/javascript" src="{{asset('assets/admin/js/demo_dashboard.js')}}"></script> 
        @section('script')
        @show

    </body>
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->     
</html>

    