@extends ('admin::layouts/base') 
  @section('content')

<div class="page-title">                    
    <h2><span class="fa fa-arrow-circle-o-left"></span>Vendor List</h2> 
    <div class="row">
        <div class="col-md-6 col-lg-6">
            @if(session('success'))
            <div class="alert alert-success">
            {{session('success')}}
            </div>
            @endif
        </div>
    </div>
</div>                                         
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <div class="">
                <div class="panel panel-default" style="">
                    <div class="panel-body">
                        @if($user)
                        <table class="table datatable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>NAME</th>
                                <th>EMAIL</th>
                                <th>JOIN</th>
                                <th class="text-center">ACTION</th>
                            </tr> 
                        </thead>
                        <tbody>
                            @foreach($user as $user)
                            <tr>
                                <td>{{$user->id}}</td>  
                                <td>{{$user->username}}</td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->created_at ? $user->created_at->diffForHumans() : 'no date'}}</td>
                                <td class="text-center"><a class="btn btn-raised btn-primary btn-sm" href="{{route('user.vendor.edit',$user->id)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a> ||  <form method="post" id= "delete-form-{{$user->id}}" action="{{route('user.vendor.delete',$user->id)}}" style="display: none;">
                                    {{ csrf_field() }}
                                    {{ method_field('delete') }}
                                </form>
                               <button onclick="if(confirm('Are You Sure, You Went To Delete This?')){
                                        event.preventDefault();
                                        document.getElementById('delete-form-{{ $user->id}}').submit();
                                    }else{
                                        event.preventDefault();
                                        }" 
                                        class="btn btn-raised btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                                </td>
                               
                            </tr>
                            @endforeach    
                        </tbody>
                        </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
