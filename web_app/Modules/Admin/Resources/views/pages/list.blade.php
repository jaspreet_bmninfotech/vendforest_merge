@extends ('admin::layouts/base') 
  @section('content')
        @if(Session::has('success'))
          <div class="alert alert-success">
            <p style="color:green;">{{Session::get('success')}}</p>
            @php Session::forget('success'); @endphp
          </div>
        @endif
<div class="page-title">                    
    <h2><span class="fa fa-arrow-circle-o-left"></span> Pages List</h2>
    <a href="{{route('admin.pages.add')}}" class="btn btn-info pull-right" role="button">ADD PAGE</a>
</div>                                         
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
        
            <div class="">
                <div class="panel panel-default" style="">
                    <div class="panel-body">
                        @if($pages)
                        <table class="table datatable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th width="200px">TITLE</th>
                                <th width="200px">DESCRIPTION</th>
                                <th width="200px">SLUG</th>
                                <th>IMAGES</th>
                                <th>CREATED DATE</th>
                                <th class="text-center">ACTION</th>
                            </tr> 
                        </thead>
                        <tbody>
                        @foreach($pages as $page)
                            <tr>
                                <td>{{$page->id}}</td>
                                <td>{{$page->title}}</td>
                                <td>{{$page->description}}</td>
                                <td>{{$page->slug}}</td>
                                <?php if ($page->path == NULL) {
                                    ?>
                                <td>Image not Uploaded</td>
                                <?php
                                } else {
                                    ?>
                                <td>{{$page->name}}</td>
                                    <?php
                                } ?>
                                <td>{{$page->created_at}}</td>
                                <td class="text-center"><a class="btn btn-raised btn-primary btn-sm" href="{{route('pages.edit',$page->id)}}"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a> ||
                                <form method="post" id= "delete-form-{{$page->id}}" action="{{route('pages.delete',$page->id)}}" style="display: none;">
                                    {{ csrf_field() }}
                                    {{ method_field('delete') }}
                                </form>
                               <button onclick="if(confirm('Are You Sure, You Went To Delete This?')){
                                        event.preventDefault();
                                        document.getElementById('delete-form-{{ $page->id}}').submit();
                                    }else{
                                        event.preventDefault();
                                        }" 
                                        class="btn btn-raised btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                                </td>
                            </tr>
                        @endforeach    
                        </tbody>
                        </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
