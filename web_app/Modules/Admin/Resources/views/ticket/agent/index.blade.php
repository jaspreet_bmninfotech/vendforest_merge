@extends('admin::layouts.base')

@section('content')

<div class="panel panel-default">
   @include('admin::ticket.admin_ticket_header')

    <div class="panel-heading">
        <h2>Agent List
        </h2>

        <h2> <a href="{{route('ticket.agent.create')}}">Create Agent </a></h2>
    </div>

    <div class="panel-body">
        <div id="message"></div>

        <table class="table table-condensed table-stripe ddt-responsive" class="table datatabl">
    <thead>
        <tr>
            <td>id</td>
            <td>Name</td>
            <td>Join Category</td>
            <td>Remove from agent</td>
            
        </tr>
    </thead>
    <tbody>
    	 
    	@foreach($data as $key =>  $val)
    	<tr>
    		<td> {{$val->id}}</td>
    		<td> {{$val->username}} </td>
    		<td> 

            @php
            $cat = [];

            if(!empty($val['ticket_agent_category'])){
             $cat = $val->ticket_agent_category->pluck('category_id')->toArray();
             // dump($cat);
            }

            @endphp

                {!! Form::open(['route'=>'ticket.agentcat.store']) !!}

                {!! Form::hidden('agent_id', $val->id) !!}
                    @foreach($category as $cat_key => $cat_val)
                        @if(in_array($cat_key, $cat))
                            <span><input type="checkbox" checked="checked" name="category[]" value="{{$cat_key}}"> {{$cat_val}} </span>

                        @else
                            <span><input type="checkbox" name="category[]" value="{{$cat_key}}"> {{$cat_val}} </span>
                        @endif
                     @endforeach
                     {!! Form::submit('join') !!}
                {!! Form::close() !!}

              </td>
    		<td><a style="color:red;" href="{{route('ticket.agentcat.remove',['id'=> $val->id])}}">Remove</a></td>
    	</tr>
    	@endforeach
    </tbody>
</table>

        {{-- @include('ticketit::tickets.partials.datatable') --}}
    </div>

</div>
@endsection
