@extends('admin::layouts.base')

@section('content')

<div class="panel panel-default">

   @include('admin::ticket.admin_ticket_header')

    

    <div class="panel-body">
        <div id="message"></div>

        <a href="{{route('ticket.create')}}"> Create Ticket</a>

        <table class="table table-condensed table-stripe ddt-responsive" class="ticketit-table">
    <thead>
        <tr>
            <td>id</td>
            <td>Subject</td>
            <td>status</td>
            <td>agent</td>
            <td>priority</td>
            <td>owner</td>
            <td>category</td>
            
        </tr>
    </thead>
    <tbody>
    	{{-- {{dd($data)}} --}}

    	@foreach($data as $key =>  $val)
    	<tr>
    		<td> {{$val->id}}</td>
    		<td> <a href="{{route('ticket.detail',['id'=>$val->id])}}">{{$val->subject}} </a></td>
    		<td>{{$val['status_name']['name']}}</td>
            <td>{{agent_user_name($val->agent_id) }}</td>

             @if(!empty($val['priority']))
            <td>{{$val['priority']['name']}}</td>
              @else
              <td> </td>
            @endif

            <td> {{agent_user_name($val->user_id) }} </td>
            <td> {{@$val['category_name']['name']}} </td>
    		
    	</tr>
    	@endforeach
    </tbody>
</table>

        {{-- @include('ticketit::tickets.partials.datatable') --}}
    </div>

</div>
@endsection
