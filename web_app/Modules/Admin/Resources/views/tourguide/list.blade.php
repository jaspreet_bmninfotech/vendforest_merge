@extends ('admin::layouts/base') 
@section('addstyle')
<link rel="stylesheet" type="text/css" href=" https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"/>
	@endsection
  @section('content')
  <div class="page-content-wrap">
    @if(Session::has('success'))
          <div class="alert alert-success">
            <p style="color:green;">{{Session::get('success')}}</p>
            @php Session::forget('success'); @endphp
          </div>
      @endif
      <div class="row">
        <div class="col-md-12">
            <div class="">
                <div class="panel panel-default" style="">
                  <div class="panel-heading">
                      <h1 class="panel-title"><strong>List</strong> </h1>
                          <a href="{{route('admin.tourguide')}}" class="btn btn-info btn-md pull-right">Add Tourguide</a>              
                  </div>
                    <div class="panel-body">
                        @if($tourguide)
                        <table class="table datatable" id="example">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>USERNAME</th>
                                <th>nationalityStatus</th>
        
                               
                                <th class="text-center">ACTION</th>
                            </tr> 
                        </thead>
                        <tbody>
                            @foreach($tourguide as $tguide)
                            <tr>
                                <td>{{$tguide->id}}</td>  
                                <td>{{$tguide->username}}</td>
                                <td>{{$tguide->nationalityStatus}}</td> 
                                <td class="text-center">
                                  <a class="btn btn-raised btn-primary btn-sm" href="{{route('admin.tourguide.edit',$tguide->id)}}" id="edittour" data-id="{{$tguide->id}}">
                                       <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a> || <form method="post" id= "delete-form-{{$tguide->id}}" action="{{route('admin.tourguide.delete',$tguide->id)}}" style="display: none;">
                                    {{ csrf_field() }}
                                    {{ method_field('delete') }}
                                </form>
                               <button onclick="if(confirm('Are You Sure, You Went To Delete This?')){
                                        event.preventDefault();
                                        document.getElementById('delete-form-{{ $tguide->id}}').submit();
                                    }else{
                                        event.preventDefault();
                                        }" 
                                        class="btn btn-raised btn-danger btn-sm"><i class="fa fa-trash-o" aria-hidden="true"></i>
                                </button>
                                </td>
                            </tr>
                            @endforeach    
                        </tbody>
                        </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
  @endsection  
  @section('script')
  <script type="text/javascript" src="{{asset('assets/admin/js/address.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/admin/js/tourguide.js')}}"></script>

	<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function() {
    $('#example').DataTable( {
        
    } );
	} );
	</script>
  @endsection