@extends ('admin::layouts/base') 
  @section('content')
        @if(Session::has('success'))
          <div class="alert alert-success">
            <p style="color:green;">{{Session::get('success')}}</p>
            @php Session::forget('success');
              
             @endphp

          </div>
        @endif
             <?php
             // dd($data['role_permisson'] );

             ?>

             <style>
               .save-btn{
                position: absolute;
                top: 15px;
               left: 570px;;
               }
               
             </style>
<div class="page-title">                    
    <h2><span class="fa fa-arrow-circle-o-left"></span>Assign Permisson to <span style="color:green;">{{$data['permisson']['name']}} </span>  </h2>
</div>                                         
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
                      
                    
                        {!! Form::open(['route'=>'role-permisson.store' ])!!}
                        @foreach($data['module'] as $key => $val )
            <div class="col-md-5">
              
                <div class="panel panel-default" style="">
                    <div class="panel-body">
                      <table class="permissiontable">
                        <tr >
                            {!! Form::hidden('role_id',$data['permisson']['id'])!!}
                            <input type="hidden" name='moduleRoute[{{$val->id}}]' value="null" >
                            <td >
                                  @if(!empty($data['role_permisson'][$val->id]) && $data['role_permisson'][$val->id] ==1)
                                    <input checked="checked" name='moduleRoute[{{$val->id}}]' type="checkbox" class="filled-in checkAll aione-float-right" id="filled-in-box-module{{$loop->iteration}}" />
                                  @else
                                    <input name='moduleRoute[{{$val->id}}]' type="checkbox" class="filled-in checkAll aione-float-right" id="filled-in-box-module{{$loop->iteration}}" />
                                @endif
                                <label data-toggle="collapse" class="" style="width:400px; text-align: center;border-radius:5px;border: 1px solid #efeeee;padding:10px" data-target="#demo{{$val->id}}"> {{$val->name}}<span class="fa fa-angle-down"></span></label>
                                  
                            </td>
                        </tr>
                             <tr  id="demo{{$val->id}}" class="collapse">
                               
                                @if(!empty($val['sub_module']))
                                       @include('admin::role_permisson.sub_module_permisson', [ 'sub_module'=>$val['sub_module'] ])
                                @endif
                             </tr>  
                             
                              

                            <tr class="save-btn">
                            
                            <td>{!! Form::submit('Save',['class'=>'btn btn-info pull-right'])!!}</td> 
                          </tr>
                              



                      </table>
                    </div>
                </div>
            </div>
                        @endforeach
                        {!! Form::close() !!}
                       
        </div>
    </div>
</div>
@endsection
