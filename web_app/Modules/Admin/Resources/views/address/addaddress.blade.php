@extends ('admin::layouts/base') 
  @section('content')    

<div class="page-content-wrap">
   <div class="col-md-6 col-lg-6">
        @if(Session::has('success'))
          <div class="alert alert-success">
            <p style="color:green;">{{Session::get('success')}}</p>
            @php Session::forget('success'); @endphp
          </div>
        @endif
   </div>
   <div class="clearfix"></div>
   <div class="row">
    
        <div class="col-md-12"> 
              {!! csrf_field() !!}
          <div id="tabcountry">
            <div class="container">
              <!-- Modal -->
              <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Add Country</h4>
                    </div>
                    <div class="modal-body addaddress">
                        <div class="form-group row">
                            <label class="col-md-3 col-xs-12 control-label">Add Country<em>*</em></label>
                            <div class="col-md-6 col-xs-12">                                            
                                <input type="text" name="newcountry" id="newcountry" class="form-control"/>
                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-xs-12 control-label">Add sortName<em>*</em></label>
                            <div class="col-md-6 col-xs-12">                                            
                                <input type="text" name="newsortName" id="newsortName" class="form-control"/>
                            </div>
                            <span class="help-block">for ex. india as IN</span>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 col-xs-12 control-label">Add phoneCode<em>*</em></label>
                            <div class="col-md-6 col-xs-12">                                            
                                <input type="text" name="newphoneCode" id="newphoneCode" class="form-control"/>
                            </div>
                            <span class="help-block">for ex. Australia as 61</span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary  saveaddress">Submit</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
           </div>
        <form id="form">
            <div class="container">
              <!-- Modal -->
              <div class="modal fade" id="stateModal" role="dialog">
                <div class="modal-dialog">
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Add Country</h4>
                    </div>
                    <div class="modal-body addaddress">
                        <div id="tabstate">
                            <div class="form-group row">
                                <label class="col-md-3 col-xs-12 control-label">Add Country<em>*</em></label>
                                <div class="col-md-6 col-xs-12">                                            
                                    <select name="country" id="country" class="form-control" onchange="getstate(this)">
                                        <option value="">Select</option>
                                        @foreach($country as $key=>$val)
                                            <option value="{{$val->id}}" data-id="{{$val->id}}">{{$val->name}}</option>
                                                @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="form-group">
                                <label class="col-md-3 col-xs-12 control-label">Add State<em>*</em></label>
                                <div class="col-md-6 col-xs-12">                                            
                                   <input type="text" name="newstate" id="newstate" class="form-control">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary  saveaddress">Submit</button>
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>ADD Address</strong></h3>
                </div>
                <div class="panel-body "> 
                    <div class="form-group row ">
                        <ul class="nav nav-tabs addresstab">
                             <li class="active" "><a href="#" id="addcountry" data-toggle="modal" data-target="#myModal">Add Country</a></li>
                             <li><a href="#" id="addstate" data-toggle="modal" data-target="#stateModal">Add State</a></li>
                             <li><a href="#" id="addcity">Add City</a></li>
                        </ul> 
                    </div>
                    
                        
                    
                    
                    <div id="tabcity">
                        <div class="form-group row">
                            <label class="col-md-3 col-xs-12 control-label">Add Country<em>*</em></label>
                            <div class="col-md-6 col-xs-12">                                            
                                <select name="getcountry" id="getcountry" class="form-control" onchange="addstate(this)"/>
                               <option value="">Select</option>
                               @foreach($country as $key=>$val)
                                        <option value="{{$val->id}}" data-id="{{$val->id}}">{{$val->name}}</option>
                                            @endforeach
                                <option></option>
                            </select>
                            </div>
                        </div>
                        
                       
                        <div class="form-group row">
                            <label class="col-md-3 col-xs-12 control-label">Add State<em>*</em></label>
                            <div class="col-md-6 col-xs-12">                                            
                               <select name="state" id="state" class="form-control" onchange="getcity(this)"/>
                                <option value="">Select</option>
                                
                             </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-xs-12 control-label">Add City<em>*</em></label>
                            <div class="col-md-6 col-xs-12">                                            
                               <input type="text" name="newcity" class="form-control" id="newcity">
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="panel-footer">
                   
                    <a href="{{route('category.list')}}" class="btn btn-primary">Back</a>
                </div>
            </div>
            
        </form>   
            
        </div>
    </div>                    
</div>
@endsection
    @section('script')
        <script type="text/javascript" src="{{asset('assets/admin/js/address.js')}}"></script>
    @endsection