$(document).ready(function(){
    $('a[data-notif-id]').click(function (e) {
        e.preventDefault();
        var notif_id   = $(this).data('notifId');
        var targetHref = $(this).data('href');
        console.log(notif_id);      
        $.ajax({
            url: "/markread/"+notif_id,
            type:"GET",
            // data:{'notif_id': notif_id},
            success: function(data){
                window.location = targetHref;  
            }
        }); 
    });
});

$('#markall').click (function(e){
    e.preventDefault();        
     $.ajax({
            url: "/markall",
            type:"GET",
            success: function(data){
                 // $('#all-notif-empty').empty();
                var noNotifi = '<li class="dropdown" id="no-notif"> <div class="row"> <div class="text-center" > No Notification </div> <div class="clearfix"></div> </div> </li>';
                 $('.submenu  .notification').html('');
                $('.badge').html(0);
                $('.submenu .notification').html(noNotifi);
            }                  
        }); 
});
 