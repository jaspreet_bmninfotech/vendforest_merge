$(function () 
  {

    //  $.ajaxSetup({
    // headers: {
    //     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    // }
    // });
     $.ajax({
      url:'/gettourguideprofile',
      type: "GET",
      dataType: "json",      
          success: function(response) 
          {
             if (response.code == 1) 
             {

            console.log(response);
            var data = response.data;
            $('input[value='+data.gender+']').attr('checked','checked');
            // $('input[name=race]:checked').val(data.race);

            $('.race input[value='+data.race+']').attr('checked','checked');
            $('select[name="birth_country"]').val(data.birthCountry);
            $('input[name=address]').val(data.address);
            $('input[name=postalCode]').val(data.postalCode);
            $('#country').val(data.country_id);
            getstate($('#country')[0], function(){
              $('#state').val(data.state_id);
              getcity($('#state')[0], function(){
              $('#city').val(data.city_id);
            });
            });
            $('input[value='+data.nationalityStatus+']').attr('checked',true);
            $('input[value='+data.knowCountry+']').attr('checked',true);
            $('.touractivities input[name="tourguideactivities[]"]').val(data.tourguideActivities);
            $('#showyou').val(data.showYou);
            var isCar = data.haveCar == 1 ? 'yes' : 'no';
             $('.radbutn input[value="'+isCar+'"]').attr('checked','checked');
            havecar(isCar);
            var carforTour = data.carForTour ==1 ? 'yes' : 'no';
            $('.cartour input[value="'+carforTour+'"]').attr('checked','checked');
            if(carforTour=='yes')
            {
            $('select[name=make]').val(data.carMake);
            $('select[name=model]').val(data.carModel);
            $('select[name=year]').val(data.carYear);
            $('input[name=plateNo]').val(data.carPlate);
            $('input[name=licenceNo]').val(data.carLicenceno);
            $('#insurance_reason textarea[name=insurancereason]').val(data.noInsuranceReason);        
            }
            else{
               $('select[name=make]').val('');
                $('select[name=model]').val('');
                $('select[name=year]').val('');
                $('input[name=plateNo]').val('');
                $('input[name=licenceNo]').val('');
                $('#insurance_reason textarea[name=insurancereason]').val('');
            }
            cartour(carforTour);
            var iscarins  = data.carInsurance == 1 ? 'yes': 'no';
            $('.insurance input[value='+iscarins+']').attr('checked','checked');
            carinsurance(iscarins);
            
          }
         }
      }); 
  }); 
  $("#form").keypress(function(e){
     if (e.which == 13) {
      e.preventDefault();
     }
  }); 
$(document).ready(function() {

  var max_fields = 5; //maximum input boxes allowed
  var field = $("#extend"); //Fields wrapper
  var add_button = $(".add_field_button"); //Add button ID
  var x = 1; //initlal text box count
  $(add_button).click(function(e){
  //on add input button click
  e.preventDefault();
  if(x < max_fields){ //max input box allowed
  x++; //text box increment
  $(field).append('<div class="input-group text-field1"><input type="text" name="guidelanguage[]"/><span class="input-group-addon addtext"><a href="#" class="remove_field">X</a></span></div>'); //add input box
  }
  });
  $(field).on("click",".remove_field", function(e){ //user click on remove text
  e.preventDefault();
  $(this).parent('span').parent('div').remove(); x--;
  });

  
 
});
$(document).ready(function(){

 $('#carforTour').hide();
  $('#cardocuments').hide();
  // $('#cardocs').hide();
  // $('#licencecopy').hide();
  // $('#plate_licence_no').hide();
  // $('#insurance_docs').hide();
  // $('#insurance_reason').hide();
  // $('#isCarinsurance').hide();

  $('input[name="isCars"]').change(function(){
  var isCar=$(this).val();
  havecar(isCar);
   });
  $('input[name="carforTour"]').change(function(){
    var carforTour = $(this).val();
    cartour(carforTour);
    
  });
  $('input[name=carinsurance]').change(function(){
    var iscarins = $(this).val();
    carinsurance(iscarins);
     
  });
});
  function havecar(isCar)
  {
     if(isCar=='yes')
    {
      $('#carforTour').show();
    }
    else if(isCar=='no'){
      $('#carforTour').hide();
      $('#cardocuments').hide();
  //     $('#cardocs').hide();
  // $('#licencecopy').hide();
  // $('#plate_licence_no').hide();
  // $('#insurance_docs').hide();
  // $('#insurance_reason').hide();
  // $('#isCarinsurance').hide();
    }
  }
  function cartour(carforTour)
  {
    if(carforTour=='yes')
    {  
      $('#cardocuments').show();
  // $('#cardocs').show();
  // $('#licencecopy').show();
  // $('#plate_licence_no').show();
  $('#insurance_docs').hide();
  $('#insurance_reason').hide();
  $('#isCarinsurance').show();
    }
    else if(carforTour=='no'){
      $('#cardocuments').hide();
    $('select[name=make]').val('');
                $('select[name=model]').val('');
                $('select[name=year]').val('');
                $('input[name=plateNo]').val('');
                $('input[name=licenceNo]').val('');
                $('#insurancereason').val('');
  // $('#cardocs').hide();
  // $('#licencecopy').hide();
  // $('#plate_licence_no').hide();
  // $('#insurance_docs').hide();
  // $('#insurance_reason').hide();
  // $('#isCarinsurance').hide();
    }
  }
function carinsurance(iscarins)
{
  console.log(iscarins);
     if(iscarins=='yes')
    {
      $('#insurancereason').val('');
      $('#insurance_docs').show();
      $('#insurance_reason').hide();
    }
    else if(iscarins=='no')
    {
      $('#insuranceupload').val('');
       $('#insurance_docs').hide();
      $('#insurance_reason').show();
    }
}