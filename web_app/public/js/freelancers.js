$(document).ready(function(){
    $("#search_freelancers_btn").click(function(){
        $(".filters-div").toggleClass('hide');
    });

    $("#close_filters").click(function(){
        $(".filters-div").toggleClass('hide');
    });
});
var buttonId = '';
function initiate_invite(user_id,e)
{	
	buttonId = $(e).attr('id');
	$.ajax({
		url: "/client/freelancer-details/"+user_id,
		dataType: 'json',
		method: 'GET',
		success: function(retJson){
			var vimg = $(e).parents('.freelancer-details').find('.vendimg').attr('src');
			$('#invitation_modal').find('#vend-img').html('<img src="'+vimg+'" class="img-responsive img-circle">');
			$(".invitation_rcvr_name").html(retJson.data.firstName + ' ' + retJson.data.lastName);
			$(".invitation_rcvr_id").val(retJson.data.id);
		}
	});

	$('#invitation_modal').modal('show');
}

// $('.invitemessage').hide();
function send_invitation(es)
{
	if ($("textarea[name='invitation_message']").val() != '')
	{
		$.ajax({
			url: "/client/freelancer/invite-to-job",
			headers: {'X-CSRF-TOKEN': $('input[name="_token"]').val()},
			data: {'form_data': $("#send_invitation_form").serialize()},
			dataType: 'json',
			method: 'POST',
			success: function(retJson){
				
					$('#invitation_modal').modal('hide');
					var message = retJson.message;
					console.log(message);
				if (retJson.status==1)
				{
					$('#'+buttonId).parents('.freelancer-details').find('.success_msg').removeClass('hidden').html(message);
					setTimeout(function(){
						$('span.success_msg').addClass('hidden');
					}, 5000);
					$('.freelancer-details').find('.error_msg').addClass('hidden');
				}else
				{
					$('#'+buttonId).parents('.freelancer-details').find('.error_msg').removeClass('hidden').html(message);
					setTimeout(function(){
						$('span.error_msg').addClass('hidden');
					}, 5000);
					$('.freelancer-details').find('.success_msg').addClass('hidden');
				}
			}
		});
	}
	else
	{
		alert('Please add your custom message to post a job.');
	}
}